usage () {
    echo `basename "$0"` " [-h] [-help] bundleRefDir bundleDir"
    echo "    -h"
    echo "    -help Display this help."
}

case "$1" in
    '-' )
        shift;;
    '-v' )
        SVN_OPT=""
        shift;;
    '-h' | '-help' )
        usage
        shift
        exit;;
esac

curDir=`pwd`
cd $1
refDir=`pwd`
# cd ${curDir}
# cd $2
# bundleDir=`pwd`

# cd "${refDir}"
for refFile in *.properties
do
    if test -f "${refFile}" -a ! -L "${refFile}"
    then
	cd "${refDir}"
	grep -v '^#' ${refFile} | cut -d = -f 1 | sort -u | {
	    while read key
	    do
		#cd "${bundleDir}"
		for bundleFile in $2
		do
		    if test -f "${bundleFile}" -a ! -L "${bundleFile}"
		    then
			if grep -q "^${key}=" "${bundleFile}"
			then
			    echo "${bundleFile}: ${key} already in ${refFile}"
			fi
		    fi
		done
	    done
	}
    fi
done
