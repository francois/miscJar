#!/bin/bash 

# Title   : start_dodwand.sh
# Role    : script for launching the DoDWAN daemon

# Syntax  : ./start_dodwand.sh [-Dkey=value ...]

DODWAN_VERSION=1.0.1

DODWAN_ROOT=${HOME}/DoDWAN


if [ "${HOSTNAME}" == "" ] ; then
 echo "Variable HOSTNAME not defined."
 exit 1
fi

java  -Ddodwan.root=${DODWAN_ROOT} \
      -Ddodwan.host=${HOSTNAME} \
      $* -cp ./dodwan-${DODWAN_VERSION}.jar:Chat.jar -Ddodwan.announce_period=1 network.chat.LaunchChat
