package misc;

import java.util.ResourceBundle;
import java.util.Locale;
import java.io.FileOutputStream;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.JUnitCore;

public class LocaleTest extends Assert {

    static public junit.framework.Test suite () {
	return new junit.framework.JUnit4TestAdapter (LocaleTest.class);
    }

    static public void main (String args[]) {
	JUnitCore.main ("misc.LocaleTest");
    }

    // ========================================
    @Test public void launch () {
	Locale currentLocale = Locale.getDefault ();
	//Locale currentLocale = new Locale ("fr", "FR");
	ResourceBundle messages = ResourceBundle.getBundle ("Misc", currentLocale, Bundle.bundleControl);
	assertNotNull (messages.getString ("ActionQuit"));
    }

    // ========================================
    @Test public void save ()
	throws java.io.IOException {

	String arg = "/tmp/RemDumpProperties";
	Bundle.load ("Misc");
	System.getProperties ().storeToXML (new FileOutputStream (arg), Bundle.getString ("RemDumpProperties", null));
    }

    // ========================================
}
