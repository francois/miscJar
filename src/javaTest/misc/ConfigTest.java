package misc;

import java.awt.Container;
import java.awt.Point;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.LineNumberReader;
import java.io.PrintWriter;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.JUnitCore;

public class ConfigTest extends Assert {

    static public junit.framework.Test suite () {
	return new junit.framework.JUnit4TestAdapter (ConfigTest.class);
    }

    static public void main (String args[]) {
	JUnitCore.main ("misc.ConfigTest");
    }

    // ========================================
    static public final String testApplicationName = "Test";
    static public final String testConfigFileName =
	Config.configSystemDir+System.getProperty ("file.separator")+testApplicationName+Config.configExt;
    static public final String result = "result";
    static public final String booleanTrue = "booleanTrue";
    static public final String example = "example";
    static public final String value = "value";

    static public final String configContent =
	"<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>\n"+
	"<!DOCTYPE properties SYSTEM \"http://java.sun.com/dtd/properties.dtd\">\n"+
	"<properties>\n"+
	"  <entry key=\"FrameLocation\">[x=100,y=200]</entry>\n"+
	"  <entry key=\""+example+"\">"+value+"</entry>\n"+
	"  <entry key=\""+booleanTrue+"\">"+true+"</entry>\n"+
	"</properties>\n";

    public int getLines (String fileName)
	throws FileNotFoundException, IOException {
	long fileSize = (new File (fileName)).length ();
	LineNumberReader lineNumberReader = new LineNumberReader (new FileReader (fileName));
	lineNumberReader.skip (fileSize);
	return lineNumberReader.getLineNumber ();
    }

    @BeforeClass static public void createTestConfig ()
	throws FileNotFoundException {
	PrintWriter configFile = new PrintWriter (testConfigFileName);
	configFile.write (configContent);
	configFile.close ();
    }

    @AfterClass static public void removeTestConfig () {
	(new File (testConfigFileName)).delete ();
    }


    // ========================================
    @Test public void load () {
	boolean findFile = false;
	for (String fileName :
		 (new File (Config.configSystemDir)).list (new FilenameFilter () {
			 public boolean accept (File dir, String name) {
			     return name.endsWith (Config.configExt);
			 }
		     })) {
	    findFile = true;
	    Config.setPWD (ConfigTest.class);
	    Config.load (fileName.substring (0, fileName.length () - Config.configExt.length ()));
	    assertTrue (Config.isLoaded ());
	}
	assertTrue (findFile);
    }

    @Test public void getString () {
	Config.setPWD (ConfigTest.class);
	Config.load (testApplicationName);
	assertEquals (result, Config.getString (null, result));
	assertEquals (value, Config.getString (example));
    }

    @Test (expected=IllegalArgumentException.class) public void getStringNullKey () {
	Config.getString (null);
    }

    @Test public void getBoolean () {
	Config.setPWD (ConfigTest.class);
	Config.load (testApplicationName);
	assertTrue (Config.getBoolean (booleanTrue, false));
	assertFalse (Config.getBoolean (example, false));
    }

    @Test (expected=IllegalArgumentException.class) public void getBooleanNullKey () {
	Config.getBoolean (null);
    }

    @Test public void save ()
	throws FileNotFoundException, IOException {
	int lines = getLines (testConfigFileName);
	Config.setPWD (ConfigTest.class);
	Config.load (testApplicationName);
	Config.setString ("a", "b");
	Config.save (testApplicationName);
	assertTrue (lines < getLines (testConfigFileName));
	Config.load (testApplicationName);
	assertEquals ("b", Config.getString ("a"));
    }

    @Test public void setString () {
	Config.setPWD (ConfigTest.class);
	Config.load (testApplicationName);
	Config.setString ("a", "b");
	assertEquals ("b", Config.getString ("a"));
    }

    @Test public void setBoolean () {
	Config.setPWD (ConfigTest.class);				
	Config.load (testApplicationName);
	Config.setBoolean ("bool", true);
	assertTrue (Config.getBoolean ("bool"));
	Config.setBoolean ("bool", false);
	assertFalse (Config.getBoolean ("bool"));
    }

    @Test public void saveLocation () {
	Config.setPWD (ConfigTest.class);
	Config.load (testApplicationName);
	assertNull (Config.getString ("test"+Config.locationPostfix, null));
	Container container = new Container ();
	container.setLocation (10, 20);
	Config.saveLocation ("test", container);
	assertEquals ("[x=10,y=20]", Config.getString ("test"+Config.locationPostfix));
    }

    @Test public void loadLocation () {
	Config.setPWD (ConfigTest.class);
	Config.load (testApplicationName);
	Container container = new Container ();
	Config.loadLocation ("Frame", container, new Point (0, 0));
	assertEquals (new Point (100, 200), container.getLocation ());
    }

    // ========================================
}
