package misc;

import java.io.StringBufferInputStream;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import org.junit.Test;
import org.junit.Ignore;
import org.junit.runner.JUnitCore;

import misc.XML;

@SuppressWarnings("deprecation")
    public class XMLTest {

	@Ignore ("for exemple") @Test (expected=IndexOutOfBoundsException.class, timeout=100) public void empty () { 
	    new java.util.ArrayList<Object> ().get (0); 
	}

	static public junit.framework.Test suite () {
	    return new junit.framework.JUnit4TestAdapter (XMLTest.class);
	}

	static public void main (String args[]) {
	    JUnitCore.main ("misc.XMLTest");
	}

	// ========================================

	// XXX lecture de chaine, exriture dans une chaine différence

	String badXmlString = "Hello Word";
	String xmlString = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
	    "<request>\n"+
	    "  <application name=\"ping\"/>\n"+
	    "</request>\n";

	// ========================================
	@Test (expected=java.io.IOException.class) public void badRead ()
	    throws javax.xml.parsers.ParserConfigurationException, org.xml.sax.SAXException, java.io.IOException {
	    XML.readDocument (new StringBufferInputStream (badXmlString));
	}

	// ========================================
	@Test public void read ()
	    throws javax.xml.parsers.ParserConfigurationException, org.xml.sax.SAXException, java.io.IOException {
	    XML.readDocument (new StringBufferInputStream (xmlString));
	}

	// ========================================
	@Test public void write ()
	    throws javax.xml.parsers.ParserConfigurationException {
	    DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance ().newDocumentBuilder ();
	    Document doc = documentBuilder.newDocument ();
	    doc.setXmlStandalone (true);
	    Element elem = doc.createElement ("noeud1");
	    doc.appendChild (elem);
	    elem.setAttribute ("attr1", "val 1");
	    elem.appendChild (doc.createElement ("noeud2")).appendChild (doc.createElement ("noeud3"));
	    elem.appendChild (doc.createElement ("noeud4"));
	    Element elem3 = doc.createElement ("noeud5");
	    elem.appendChild (elem3);
	    elem3.setAttribute ("attr5", "val 5&");
	    XML.writeDocument (doc, System.out);
	}

	// ========================================
    }
