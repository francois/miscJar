package misc;

import java.util.Locale;
import javax.swing.JButton;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.JUnitCore;

public class BundleTest extends Assert {

    static public junit.framework.Test suite () {
	return new junit.framework.JUnit4TestAdapter (BundleTest.class);
    }

    static public void main (String args[]) {
	JUnitCore.main ("misc.BundleTest");
    }

    // ========================================
    static public int countObserver;
    static public int countActioner;

    @BeforeClass static public void setBundleObserver () {
	Bundle.addBundleObserver (new Object () {
		public void updateBundle () {
		    countObserver++;
		}
	    });
	Bundle.load ("BundleManager", Locale.US);
	String action = "Quit";
	JButton button = new JButton (Bundle.getAction (action)) {
		private static final long serialVersionUID = 1L;

		public void setText (String text) {
		    super.setText (text);
		    countActioner++;
		}
	    };
	Bundle.addLocalizedActioner (button);
	button.setActionCommand (action);
    }

    // ========================================
    @Test public void setLocale () {
	Bundle.load ("BundleManager", Locale.US);
	int oldValue = countObserver;
	Bundle.setLocale (Locale.FRANCE);
	assertTrue (oldValue+1 == countObserver);
	assertTrue (countActioner == countObserver);
    }

    @Test public void load () {
	int oldValue = countObserver;
	Bundle.load ("BundleManager");
	assertTrue (oldValue+1 == countObserver);
	assertTrue (countActioner == countObserver);
    }

    @Test public void getLocales () {
	Locale[] locales = Bundle.getApplicationsLocales ();
	assertTrue (locales.length > 0);
	Bundle.load ("BundleManager");
	for (Locale locale : locales)
	    Bundle.setLocale (locale);
    }

    @Test public void getString () {
	Bundle.load ("BundleManager", Locale.US);
	String s1 = Bundle.getTitle ("BundleEditor");
	Bundle.setLocale (Locale.FRANCE);
	String s2 = Bundle.getTitle ("BundleEditor");
	assertNotNull (s1);
	assertNotNull (s2);
	assertFalse (s1.equals (s2));
    }

    // ========================================
}
