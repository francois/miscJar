package misc;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.JUnitCore;

public class ColorCheckedLineTest extends Assert {

    static public junit.framework.Test suite () {
	return new junit.framework.JUnit4TestAdapter (ColorCheckedLineTest.class);
    }

    static public void main (String args[]) {
	JUnitCore.main ("misc.ColorCheckedLineTest");
    }

    // ========================================
    @Test public void ok () {
	try {
	    System.err.print ("Test ok : ");

	    // something right

	    System.err.println (ColorCheckedLine.MSG_OK);
	} catch (Exception e) {
	    System.err.println (ColorCheckedLine.MSG_OK);
	}
    }

    // ========================================
    @Test public void ko () {
	try {
	    System.err.print ("Test ko : ");

	    // something wrong
	    int i = 0;
	    i = 1/i;

	    System.err.println (ColorCheckedLine.MSG_OK);
	} catch (Exception e) {
	    System.err.println (ColorCheckedLine.MSG_KO);
	}
    }

    // ========================================
}
