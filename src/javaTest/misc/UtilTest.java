package misc;

import java.awt.Component;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.List;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JMenuItem;
import javax.swing.JRadioButton;
import javax.swing.JMenuBar;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.JUnitCore;

public class UtilTest extends Assert {

    static public junit.framework.Test suite () {
	return new junit.framework.JUnit4TestAdapter (UtilTest.class);
    }

    static public void main (String args[]) {
	JUnitCore.main ("misc.UtilTest");
    }

    // ========================================
    static public final void isCapital (String s) {
	assertTrue (Character.isUpperCase (s.charAt(0)));
	for (int i = 1; i < s.length (); i++)
	    assertTrue (Character.isLowerCase (s.charAt(i)));
    }

    static public final List<String> actions = Arrays.asList ("Open", "Save", "Quit");
    static public final ActionListener actionListener = new ActionListener () {
	    public void actionPerformed (ActionEvent e) {
	    }
	};

    @BeforeClass static public void createTestConfig () {
	Bundle.load ("BundleManager");
    }

    // ========================================
    @Test public void addRadioButton () {
 	Container container = new Container ();
	ButtonGroup group = new ButtonGroup ();
 	Util.addRadioButton (actions, actionListener, group, "Quit", container);
	assertTrue (actions.contains (group.getSelection ().getActionCommand ()));
	assertTrue (container.getComponentCount () == actions.size ());
	for (Component component : container.getComponents ()) {
	    JRadioButton button = (JRadioButton) component;
	    assertTrue (actions.contains (button.getActionCommand ()));
	    Arrays.asList (button.getActionListeners ()).contains (actionListener);
	    if ("Quit".equals (button.getActionCommand ())) {
		assertEquals (group.getSelection ().getActionCommand (), button.getActionCommand ());
		assertTrue (button.isSelected ());
	    } else
		assertFalse (button.isSelected ());
	}
    }

    @Test public void addButton () {
 	Container container = new Container ();
 	Util.addButton (actions, actionListener, container);
	assertTrue (container.getComponentCount () == actions.size ());
	for (Component component : container.getComponents ()) {
	    JButton button = (JButton) component;
	    assertTrue (actions.contains (button.getActionCommand ()));
	    Arrays.asList (button.getActionListeners ()).contains (actionListener);
	}
    }

    @Test public void addIconButton () {
 	Container container = new Container ();
 	Util.addIconButton (actions, actionListener, container);
	assertTrue (container.getComponentCount () == actions.size ());
	for (Component component : container.getComponents ()) {
	    JButton button = (JButton) component;
	    assertNotNull (button.getIcon ());
	    assertTrue (actions.contains (button.getActionCommand ()));
	    Arrays.asList (button.getActionListeners ()).contains (actionListener);
	}
    }

    @Test public void addMenuItem () {
	Container container = new Container ();
 	Util.addMenuItem (actions, actionListener, container);
	assertTrue (container.getComponentCount () == actions.size ());
	for (Component component : container.getComponents ()) {
	    JMenuItem item = (JMenuItem) component;
	    assertTrue (actions.contains (item.getActionCommand ()));
	    Arrays.asList (item.getActionListeners ()).contains (actionListener);
	}
    }

    @Test public void addCheckMenuItem () {
	Container container = new Container ();
 	Util.addCheckMenuItem (actions, actionListener, container);
	assertTrue (container.getComponentCount () == actions.size ());
	for (Component component : container.getComponents ()) {
	    JMenuItem item = (JMenuItem) component;
	    assertTrue (actions.contains (item.getActionCommand ()));
	    Arrays.asList (item.getActionListeners ()).contains (actionListener);
	}
    }

    @Test public void addJMenu () {
	JMenuBar jMenuBar = new JMenuBar ();
	Util.addJMenu (jMenuBar, "Help");
    }

    @Test public void setColumnLabels () {
	// YYY
    }


    @Test public void packWindow () {}
    @Test public void newJFrame () {}
    @Test public void WindowAdapter () {}
    @Test public void windowClosing () {}

    @Test public void loadActionIcon () {}
    @Test public void loadImageIcon () {}
    @Test public void getDataUrl () {}

    @Test public void collectMethod () {}
    @Test public void collectButtons () {}
    @Test public void actionPerformed () {}
    @Test public void merge () {}

    @Test public void toCapital () {
	for (String s : Arrays.asList ("abcd", "AbCd", "aBcD", "ABCD"))
	    isCapital (Util.toCapital (s));
    }

    @Test  (timeout=1500) public void sleep () {
	long before = System.currentTimeMillis ();
	Util.sleep (1);
	long after = System.currentTimeMillis ();
	long delta = after-before;
	assertTrue (delta > 900 && delta < 1100);
    }
}
