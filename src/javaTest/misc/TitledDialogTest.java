package misc;

import java.awt.Frame;
import java.util.Locale;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.JUnitCore;

public class TitledDialogTest extends Assert {

    static public junit.framework.Test suite () {
	return new junit.framework.JUnit4TestAdapter (TitledDialogTest.class);
    }

    static public void main (String args[]) {
	JUnitCore.main ("misc.TitledDialogTest");
    }

    // ========================================
    static public TitledDialog titledDialog;

    @BeforeClass static public void setBundleObserver () {
	Bundle.load ("BundleManager");
	titledDialog = new TitledDialog (new Frame (), "BundleEditor");
    }

    // ========================================
    @Test public void bundleReloaded () {
	Bundle.load ("BundleManager", Locale.US);
	String s1 = titledDialog.getTitle ();
	Bundle.setLocale (Locale.FRANCE);
	String s2 = titledDialog.getTitle ();
	assertNotNull (s1);
	assertNotNull (s2);
	assertFalse (s1.equals (s2));
    }

    @Test public void setVisible () {
	Config.setPWD (TitledDialogTest.class);
	Config.load ("BundleManager");
	titledDialog.setVisible (true);
	assertTrue (titledDialog.isVisible ());
	titledDialog.setVisible (false);
	assertFalse (titledDialog.isVisible ());
    }

    @Test public void saveLocation () {
	Config.setPWD (TitledDialogTest.class);
	Config.load ("BundleManager");
	assertNull (Config.getString ("BundleEditor"+Config.locationPostfix, null));
	titledDialog.setVisible (true);
	titledDialog.setVisible (false);
	titledDialog.saveLocation ();
	Object [] location = Config.coordonateFormat.parse (Config.getString ("BundleEditor"+Config.locationPostfix, null),
							    new java.text.ParsePosition (0));
	assertTrue (((Number) location [0]).intValue () > 0);
	assertTrue (((Number) location [1]).intValue () > 0);
    }

    // ========================================
}
