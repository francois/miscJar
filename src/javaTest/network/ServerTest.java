package network;

import org.junit.Test;
import org.junit.runner.JUnitCore;

import misc.Util;

public class ServerTest {

    static public junit.framework.Test suite () {
	return new junit.framework.JUnit4TestAdapter (ServerTest.class);
    }

    static public void main (String args[]) {
	JUnitCore.main ("network.ServerTest");
    }

    // ========================================
    @Test public void launch () {
	Server server = new Server (8080);
	Client client = new Client ("127.0.0.1", 8080);

	server.start ();

	server.send ("service avant ecoute client");

	Util.sleep (2);

	client.start ();

	Util.sleep (2);

	server.send ("service apres ecoute client");

	Util.sleep (2);

	client.send ("service immediat");
    }

    // ========================================
}
