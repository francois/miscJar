package network;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URL;
import java.net.URLConnection;

import org.junit.Test;
import org.junit.runner.JUnitCore;

import misc.XML;

public class HTTPInputStreamTest {

    static public junit.framework.Test suite () {
	return new junit.framework.JUnit4TestAdapter (HTTPInputStreamTest.class);
    }

    static public void main (String args[]) {
	JUnitCore.main ("network.HTTPInputStreamTest");
    }

    static {
	Protocol.setIpV4 ();
    }

    // ========================================
    @Test (timeout=3000) public void launch ()
	throws IOException {

	String xmlRequest =
	    "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
	    "<request>\n"+
	    "  <application name=\"ping\"/>\n"+
	    "</request>\n";

	final String xmlResponse =
	    "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
	    "<response>\n"+
	    "  <status value=\"ok\"/>\n"+
	    "</response>\n";

	System.err.println ("start test");
	System.err.flush ();
	final ServerSocket serverSocket = new ServerSocket (8080);
	serverSocket.setReuseAddress (true);
	(new Thread () {
		public void run () {
		    try {
			System.err.println ("run");
			System.err.flush ();
			Socket call = serverSocket.accept ();
			System.err.println ("Accept call "+call);
			HTTPInputStream httpInputStream = new HTTPInputStream (call.getInputStream ());
			System.err.println ("Server:main headers "+httpInputStream.headers+
					    " available:"+httpInputStream.available ());
			System.err.println ("Server:main server receiveXML : ");
			XML.writeDocument (XML.readDocument (httpInputStream), System.err);
			System.err.flush ();

			HTTPOutputStream httpPOutputStream = new HTTPOutputStream (call.getOutputStream ());
			httpPOutputStream.setHeader ("Content-type", "application/xml");
			httpPOutputStream.write (xmlResponse.getBytes ());
			httpPOutputStream.close ();
		    } catch (IOException e) {
			e.printStackTrace ();
		    }
		}
	    }).start ();

	System.err.println ("url");
	System.err.flush ();
	URLConnection urlConnection = XMLConnection.getXMLConnection (new URL ("http://localhost:8080/"));
	OutputStreamWriter out = new OutputStreamWriter (urlConnection.getOutputStream ());
	out.write (xmlRequest);
	out.close ();

	// attention l'ouverture de inputstream provoque la fermeture de outputstream
	InputStream in = urlConnection.getInputStream ();
	System.err.println ("Server:main client receiveXML : ");
	XML.writeDocument (XML.readDocument (in), System.err);
	System.err.flush ();
    }

    // ========================================
}
