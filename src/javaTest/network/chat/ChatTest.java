package network.chat;

import org.junit.Test;
import org.junit.runner.JUnitCore;

import network.Client;
import network.Server;

public class ChatTest {

    static public junit.framework.Test suite () {
	return new junit.framework.JUnit4TestAdapter (ChatTest.class);
    }

    static public void main (String args[]) {
	JUnitCore.main ("network.chat.ChatTest");
    }

    // ========================================
    @Test public void launch () {
	Server server = new Server (8080);
	Client client = new Client ("127.0.0.1", 8080);

	Chat chatServer = new Chat ("chatServer", server);
	Chat chatClient = new Chat ("chatClient", client);

	ChatObserver chatObserver = new ChatObserver () {
		public void talk (ChatQuote quote) {
		    System.err.println (Chat.dateFormat.format (quote.date)+" "+quote.speaker+" > "+quote.sentence);
		}
		public void renameSpeaker (String speaker) {}
		public void chatModifiedChange (boolean modified) {}
		public void clearChat () {}
	    };
	chatServer.addChatObserver (chatObserver);
	chatClient.addChatObserver (chatObserver);

	server.start ();
	misc.Util.sleep (1);
	client.start ();
	misc.Util.sleep (1);
	chatServer.say ("hello");
	misc.Util.sleep (1);
	chatClient.say ("world");
    }

    // ========================================
}
