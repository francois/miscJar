package network.chat;

import java.awt.Image;
import org.junit.Test;
import org.junit.runner.JUnitCore;

import misc.Bundle;
import misc.Config;
import misc.Controller;
import misc.Util;

import network.Client;
import network.ProtocolManager;
import network.Server;
import network.login.Login;
import network.login.LoginManager;

public class JChatTest {
    
    static public junit.framework.Test suite () {
	return new junit.framework.JUnit4TestAdapter (JChatTest.class);
    }

    static public void main (String args[]) {
	JUnitCore.main ("network.chat.JChatTest");
    }

    public class Controller2 extends Controller<String> {
	public Controller2 () { super (""); }
	protected void createModel (String s) {}
	public String getTitle () { return null; }
	public Image getIcon () { return null; }
    };

    // ========================================
    @Test public void launch () {
	Config.setPWD (JChatTest.class);
	Config.load ("Chat");
	Bundle.load ("Chat");

	Controller<String> controller = new Controller2 ();

	ProtocolManager serverProtocolManager = new ProtocolManager (controller);
	ProtocolManager clientProtocolManager = new ProtocolManager (controller);

	Server server = new Server (8080);
	Client client = new Client ("localhost", 8080);

	server.start ();
	client.start ();

	serverProtocolManager.setProtocol (server);
	clientProtocolManager.setProtocol (client);

	Login login1 = new Login ("");
	Login login2 = new Login ("");
	LoginManager loginManager1 = new LoginManager (controller, login1);
	LoginManager loginManager2 = new LoginManager (controller, login2);


	Util.newJFrame ("Serge", new JChat (serverProtocolManager, loginManager1,
					    new ChatManager (controller, new Chat ("Serge", server))), true);
	Util.newJFrame ("Clement", new JChat (clientProtocolManager, loginManager2,
					      new ChatManager (controller, new Chat ("Clement", client))), true);
    }

    // ========================================
}
