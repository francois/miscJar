package network;

import java.io.IOException;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.JUnitCore;

public class HTTPOutputStreamTest extends Assert {

    static public junit.framework.Test suite () {
	return new junit.framework.JUnit4TestAdapter (HTTPOutputStreamTest.class);
    }

    static public void main (String args[]) {
	JUnitCore.main ("network.HTTPOutputStreamTest");
    }

    static {
	Protocol.setIpV4 ();
    }

    // ========================================
    @Test (timeout=3000) public void launch ()
	throws IOException {
	final java.net.ServerSocket serverSocket = new java.net.ServerSocket (8080);
	serverSocket.setReuseAddress (true);
	(new Thread () {
		public void run () {
		    try {
			java.net.Socket call = serverSocket.accept ();
			HTTPOutputStream out = new HTTPOutputStream (call.getOutputStream ());
			out.setHeader ("Content-type", "application/xml");
			out.write ("coucou\n".getBytes ());
			out.close ();
		    } catch (Exception e) {
			fail ("Server side exception:"+e);
		    }
		}
	    }).start ();

	java.net.Socket client = new java.net.Socket ("localhost", 8080);
	java.io.InputStream in = client.getInputStream ();

	byte[] b = new byte [8196];
	int nb = in.read (b);
	System.err.println ("Client:in:"+new String (b, 0, nb));
	System.err.flush ();
    }

    // ========================================
}
