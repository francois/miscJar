package tool.model;

import java.io.File;

import org.junit.Test;
import org.junit.Ignore;
import org.junit.runner.JUnitCore;

import misc.Bundle;

public class BundleManagerModelTest {

    @Ignore ("for exemple") @Test (expected=IndexOutOfBoundsException.class, timeout=100) public void empty () { 
	new java.util.ArrayList<Object> ().get (0); 
    }

    static public junit.framework.Test suite () {
	return new junit.framework.JUnit4TestAdapter (BundleManagerModelTest.class);
    }

    static public void main (String args[]) {
	JUnitCore.main ("tool.BundleManagerModelTest");
    }

    // ========================================
    @Test public void copy () {
	Bundle.load ("BundleManager");
	BundleManagerModel bmm = new BundleManagerModel ();
	bmm.open (new File ("data/config"), "BundleManager", false);
 	bmm.saveAs (new File ("/tmp"), "BundleManager");
    }

    // ========================================
}
