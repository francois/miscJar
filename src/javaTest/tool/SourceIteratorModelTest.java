package tool;

import java.io.File;
import java.util.Vector;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.JUnitCore;

import tool.model.SourceIteratorModel;

public class SourceIteratorModelTest extends Assert {

    static public junit.framework.Test suite () {
	return new junit.framework.JUnit4TestAdapter (SourceIteratorModelTest.class);
    }

    static public void main (String args[]) {
	JUnitCore.main ("tool.SourceIteratorModelTest");
    }

    // ========================================
    @Test public void parse () {
	Vector<File> files = new Vector<File> ();
	for (SourceIteratorModel si = new SourceIteratorModel (new File ("../src")); si.hasMoreJava (); ) {
	    File file = si.nextJava ();
	    files.add (file);
	    assertTrue (file.isFile ());
	}
	for (SourceIteratorModel si = new SourceIteratorModel (new File ("../src")); si.hasMoreString (); ) {
	    String next = si.nextString ();
	    assertTrue (files.contains (si.getCurrentFile ()));
	    assertTrue (next.indexOf ("\"") >= 0);
	}
    }

    // ========================================
}
