package misc;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.TreeSet;
import java.util.Vector;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;

import static misc.Util.GBC;
import static misc.Util.GBCNL;

import static misc.RemoteUpdate.CheckPeriod;
import static misc.RemoteUpdate.FileDescriptor;
import static misc.RemoteUpdate.FileInfo;
import static misc.RemoteUpdate.TodoFile;

public class JRemoteUpdate implements SwingConstants {

    static public String
	actionDetails = "Details",
	labelFileName = "FileName", labelLocal= "Local", labelRemote = "Remote";


    // ========================================
    private OwnFrame controller;
    RemoteUpdate remoteUpdate;
    Runnable actionAfterDownLoad;

    JPanel jPanel = Util.getGridBagPanel ();
    ArrayList<MirrorInfo> allMirrorInfo;
    JFrame jFrame = new JFrame ();
    JButton startAction;
    
    // ========================================
    public JRemoteUpdate (OwnFrame controller, RemoteUpdate remoteUpdate, Runnable actionAfterDownLoad) {
	this.controller = controller;
	this.remoteUpdate = remoteUpdate;
	this.actionAfterDownLoad = actionAfterDownLoad;
	jFrame.setIconImage (controller.getIcon ());
    }

    public class MirrorInfo {
	RemoteUpdate.Mirror mirror;
	StateNotifier notifier = new StateNotifier ();
	public ProgressState progressState = new ProgressState (notifier, "Progress");
	TodoFile transfertAction, cleanAction;
	JCheckBox transfertBoutton, cleanBoutton;
	JLabel info;
	ProgressStatePanel progressStatePanel = new ProgressStatePanel (progressState);

	MirrorInfo (RemoteUpdate.Mirror mirror, TodoFile transfertAction) {
	    this.mirror = mirror;
	    // XXX check transfertAction in Download|Upload
	    this.transfertAction = transfertAction;
	    cleanAction = transfertAction == TodoFile.Download ? TodoFile.LocalRemove : TodoFile.RemoteRemove;
	    transfertBoutton = Util.addCheckIcon (""+transfertAction, null,
						  Config.getBoolean (transfertAction+mirror.token+Config.checkedPostfix, false),
						  jPanel, GBC);
	    cleanBoutton = Util.addCheckIcon (""+cleanAction, null,
					      Config.getBoolean (cleanAction+mirror.token+Config.checkedPostfix, false),
					      jPanel, GBC);
	    notifier.addUpdateObserver (progressStatePanel, "Progress");
	    info = new JLabel (mirror.getInfo (mirror.check (transfertAction), transfertAction));
	    Util.addComponent (info, jPanel, GBC);
	    Util.addIconButton (actionDetails, new ActionListener () {
		    public void actionPerformed (ActionEvent e) {
			JOptionPane.showMessageDialog (jFrame, getDetails (), Bundle.getTitle (""+actionDetails), JOptionPane.INFORMATION_MESSAGE);
		    }
		}, jPanel);
	    Util.unBoxButton (jPanel);
	    Util.addComponent (progressStatePanel, jPanel, GBCNL);
	}
	public void setConfig () {
	    Config.setBoolean (transfertAction+mirror.token+Config.checkedPostfix, transfertBoutton.isSelected ());
	    Config.setBoolean (cleanAction+mirror.token+Config.checkedPostfix, cleanBoutton.isSelected ());
	}
	public void update () {
	    (new Thread () {
	    	    public void run () {
			mirror.update ();
			info.setText (mirror.getInfo (mirror.check (transfertAction), transfertAction));
			//Util.packWindow (jPanel);
		    }
		}).start ();
	}
	public JScrollPane getDetails () {
	    @SuppressWarnings ("serial")
	    DefaultTableModel dataObjectModel = new DefaultTableModel () {
		    public Class<?> getColumnClass (int column) {
			if (column != 1)
			    return String.class;
			return javax.swing.ImageIcon.class;
		    }
		};
	    JTable jTableObject = new JTable (dataObjectModel);
	    jTableObject.getTableHeader ().setReorderingAllowed (true);
	    jTableObject.setShowVerticalLines (false);
	    JScrollPane scrollPane = Util.getJScrollPane (jTableObject, true, false);
	    jTableObject.setFillsViewportHeight (true);
	    dataObjectModel.setColumnCount (6);
	    Util.setColumnLabels (jTableObject, new String[] {"FileName", null, "LocalDate", "LocalSize", "RemoteDate", "RemoteSize"});

	    for (TodoFile action : new TodoFile []{transfertAction, cleanAction})
		for (String fileName : mirror.check (action)) {
		    FileDescriptor fd = mirror.getInfo (fileName);
		    FileInfo lfi = fd.local, rfi = fd.remote;
		    dataObjectModel.addRow (new Object [] {fileName, Util.loadActionIcon (""+fd.todo+Util.ON),
							   FileInfo.getDate (lfi), FileInfo.getSize (lfi),
							   FileInfo.getDate (rfi), FileInfo.getSize (rfi)});
		}
	    scrollPane.setPreferredSize (new java.awt.Dimension (800, 400));
	    return scrollPane;
	}
	public void startAction () {
	    (new Thread () {
		    public void run () {
			try {
			    String messagePrefix = transfertAction == TodoFile.Download ? "Download" : "Upload";
			    if (!(cleanBoutton.isSelected () || transfertBoutton.isSelected ()))
				return;
			    mirror.update ();
			    TreeSet<String> removed = new TreeSet<String> ();
			    if (cleanBoutton.isSelected ())
				removed = mirror.performe (cleanAction, null);
			    TreeSet<String> transfered = new TreeSet<String> ();
			    if (transfertBoutton.isSelected ()) {
				transfered = mirror.performe (transfertAction, progressState);
				mirror.update ();
				info.setText (mirror.getInfo (mirror.check (transfertAction), transfertAction));
			    }
			    if (removed.size () == 0 && transfered.size () == 0)
				return;
			    JPanel msgPanel = Util.getGridBagPanel ();
			    Util.addComponent (new JLabel (MessageFormat.format (Bundle.getMessage (messagePrefix+"Completed"),
										 transfered.size ())), msgPanel, GBCNL);
			    if (transfered.size () > 0) {
				Util.addLabel (""+transfertAction, LEFT, msgPanel, GBCNL);
				Util.addComponent (Util.getJScrollPane (new JList<String> (new Vector<String> (transfered))),
						   msgPanel, GBCNL);
			    }
			    if (removed.size () > 0) {
				Util.addLabel ("Remove", LEFT, msgPanel, GBCNL);
				Util.addComponent (Util.getJScrollPane (new JList<String> (new Vector<String> (removed))),
						   msgPanel, GBCNL);
			    }
			    JOptionPane.showMessageDialog (jFrame,
							   msgPanel,
							   Bundle.getTitle (""+transfertAction), JOptionPane.INFORMATION_MESSAGE);
			} finally {
			    decrThreads ();
			}
		    }
		}).start ();
	}
    };
    private int nbThreads;
    private boolean isDownload;
    private synchronized void resetThreads (int nbThreads, boolean isDownload) {
	if (nbThreads < 1)
	    return;
	while (this.nbThreads != 0)
	    try {
		wait ();
	    } catch (Exception InterruptedException) {
	    }
	this.isDownload = isDownload;
	startAction.setEnabled (false);
	this.nbThreads = nbThreads;
    }
    private synchronized void decrThreads () {
	nbThreads--;
	if (nbThreads != 0)
	    return;
	notifyAll ();
	startAction.setEnabled (true);
	if (isDownload && actionAfterDownLoad != null)
	    actionAfterDownLoad.run ();
    }

    // ========================================
    public void dialog (TodoFile action, boolean checkPeriod) {
	jPanel.removeAll ();
	allMirrorInfo = new ArrayList<MirrorInfo> ();
	for (RemoteUpdate.Mirror mirror : remoteUpdate.mirrors)
	    allMirrorInfo.add (new MirrorInfo (mirror, action));
	for (MirrorInfo mirrorInfo : allMirrorInfo)
	    mirrorInfo.update ();
	startAction = Util.addButton (""+action, null, jPanel, GBCNL);
	startAction.addActionListener (new ActionListener () {
		public void  actionPerformed (ActionEvent e) {
		    (new Thread () {
			    public void run () {
				resetThreads (allMirrorInfo.size (), action == TodoFile.Download);
				for (MirrorInfo mirrorInfo : allMirrorInfo)
				    mirrorInfo.startAction ();
			    }
			}).start ();
		}
	    });
	if (checkPeriod) {
	    JPanel line = Util.getGridBagPanel ();
	    Util.addLabel ("CheckingPeriod", LEFT, line, GBC);
	    JComboBox<String> periodChooser = Util.addEnum (CheckPeriod.class, remoteUpdate.currentPeriod (), line, GBCNL);
	    Util.addComponent (line, jPanel, GBCNL);
	    if (JOptionPane.showConfirmDialog (jFrame, jPanel, Bundle.getTitle (""+action),
					       JOptionPane.YES_NO_OPTION,
					       JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION)
		Config.setString ("CheckPeriod", ""+CheckPeriod.values () [periodChooser.getSelectedIndex ()]);
	} else
	    JOptionPane.showMessageDialog (jFrame, jPanel, Bundle.getTitle (""+action), JOptionPane. QUESTION_MESSAGE);
	for (MirrorInfo mirrorInfo : allMirrorInfo)
	    mirrorInfo.progressState.abort ();
	for (MirrorInfo mirrorInfo : allMirrorInfo)
	    mirrorInfo.setConfig ();
    }
    public void downloadDialog () {
	dialog (TodoFile.Download, true);
    }
    public void uploadDialog () {
	dialog (TodoFile.Upload, false);
    }

    // ========================================
}
