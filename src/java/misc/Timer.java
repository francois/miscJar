package misc;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.TimeZone;

public class Timer {
    // ========================================
    static public SimpleDateFormat sdf = new SimpleDateFormat ("H:mm:ss.S");
    static {
	sdf.setTimeZone (TimeZone.getTimeZone ("GMT"));
    }
    class Milestone {
	String name;
	long end;
	long last;
	Milestone (String name, long end, long last) {
	    this.name = name;
	    this.end = end;
	    this.last = last;
	}
    }
    // ========================================
    long start = System.currentTimeMillis ();
    long lastEnd = start;
		
    ArrayList<Milestone> milestones = new ArrayList<Milestone> ();

    public String getLap (String milestoneName) {
	long now = System.currentTimeMillis ();
	long end = now-start;
	long last = now - lastEnd;
	lastEnd = now;
	milestones.add (new Milestone (milestoneName, end, last));
	return milestoneName+": "+sdf.format (end)+" ("+end+")"+": "+sdf.format (last)+" ("+last+")";
    }

    public String getNames () {
	String result = "";
	for (Milestone milestone : milestones)
	    result += ":"+milestone.name;
	return result;
    }
    public String getEnds () {
	String result = "";
	for (Milestone milestone : milestones)
	    result += ":"+sdf.format (milestone.end)+" ("+milestone.end+")";
	return result;
    }
    public String getLasts () {
	String result = "";
	for (Milestone milestone : milestones)
	    result += ":"+sdf.format (milestone.last)+" ("+milestone.last+")";
	return result;
    }

    // ========================================
}
