package misc;

import java.awt.Image;
import javax.swing.JFrame;

public interface OwnFrame {
    public JFrame getJFrame ();
    public String getTitle ();
    public Image getIcon ();
    public void reborn ();
}
