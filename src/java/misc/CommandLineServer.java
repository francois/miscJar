package misc;

import java.io.IOException;
import java.lang.reflect.Method;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.MessageFormat;
import java.util.Collection;
import java.util.Hashtable;
import java.util.TreeSet;
import java.util.Vector;

public class CommandLineServer<Model> {

    // ========================================
    private Model model;
    private Class<? extends CommandLineWaiter<Model>> classWaiter;
    private String classWaiterName;
    /** List of commands. */
    private TreeSet<String> commands = new TreeSet<String> ();
    /** Help for each command. */
    private Hashtable<String, String> help = new Hashtable<String, String> ();
    private Hashtable<String, Method> actionsMethod = new Hashtable<String, Method> ();

    // ========================================
    public TreeSet<String> getCommands () { return commands; }
    public Hashtable<String, String> getHelp () { return help; }
    public Hashtable<String, Method> getActionsMethod () { return actionsMethod; }
    public String getClassWaiterName () { return classWaiterName; }

    // ========================================
    public void showMsg (String msg) {}
    public void showStatus (String msg) {}

    // ========================================
    public CommandLineServer (Model model, Class<? extends CommandLineWaiter<Model>> classWaiter, Collection<String> commands) {
	this.model = model;

	this.classWaiter = classWaiter;
	String[] path = classWaiter.getName ().split ("\\.");
	classWaiterName = path [path.length-1];

	for (String action : commands)
	    this.commands.add (action.toLowerCase ());
	for (String action : this.commands) {
	    try {
		String methodName = "action"+Util.toCapital (action);
		actionsMethod.put (action, classWaiter.getMethod (methodName));
	    } catch (NoSuchMethodException e) {
		Log.keepLastException ("CommandLineServer", e);
	    }
	}
	updateBundle ();
	Bundle.addBundleObserver (this);
    }

    // ========================================
    public void updateBundle () {
	help.clear ();
	for (String cmd : commands)
	    help.put (cmd, Bundle.getString ("Help"+classWaiterName+"_"+cmd, null).replaceAll ("\\\\n", "\n"));
    }

    /** In case of multiple started, this is the only thread wich have right to run. */
    private Thread thread;

    // ========================================
    public boolean isAliveServer () {
	return thread != null && thread.isAlive ();
    }

    // ========================================
    private int lastPort;
    /** Interrupt the server after a TIME_OUT. */
    public void stopServer () {
	thread = null;
	try {
	    // force le accept pour sortir du run
	    (new Socket ("localhost", lastPort)).close ();
	} catch (Exception e) {
	}
	stopCalls ();
    }

    Vector<Socket> calls = new Vector<Socket> ();
    public synchronized void addCall (Socket call) {
	    calls.add (call);
	}
    public synchronized void removeCall (Socket call) {
	    calls.remove (call);
	}
    public synchronized void stopCalls () {
	    for (Socket call : calls) {
		try {
		    call.close ();
		} catch (Exception e) {
		}
	    }
	    calls.clear ();
	}

    // ========================================
    /**
     * Start a server on a dedicated port.
     * @param port the internet port number.
     */
    public void startServer (final int port) {
	(new Thread () {
		public void run () {
		    ServerSocket serverSocket = null;
		    try {
			serverSocket = new ServerSocket (port);
			CommandLineServer.this.lastPort = port;
		    } catch (IOException e) {
			showMsg (Bundle.getMessage ("CanTCreateSocket"));
			Log.keepLastException ("CommandLineServer::startServer", e);
			return;
		    }
		    showMsg (MessageFormat.format (Bundle.getMessage (classWaiterName+"Starts"), port));
		    showStatus (MessageFormat.format (Bundle.getMessage (classWaiterName+"Starts"), port));
		    thread = Thread.currentThread ();
		    try {
			while (thread == Thread.currentThread ()) {
			    try {
				Socket call = serverSocket.accept ();
				addCall (call);
				classWaiter.getConstructor (CommandLineServer.class, model.getClass (), Socket.class).
				    newInstance (CommandLineServer.this, model, call); 
			    } catch (IOException e) {
				showMsg (Bundle.getMessage ("ClientAbort"));
				Log.keepLastException ("CommandLineServer::startServer", e);
			    }
			}
		    } catch (java.lang.reflect.InvocationTargetException e) {
			Log.keepLastException ("CommandLineServer::startServer", e);
		    } catch (NoSuchMethodException e) {
			Log.keepLastException ("CommandLineServer::startServer", e);
		    } catch (IllegalAccessException e) {
			Log.keepLastException ("CommandLineServer::startServer", e);
		    } catch (InstantiationException e) {
			Log.keepLastException ("CommandLineServer::startServer", e);
		    }
		    try {
			showMsg (Bundle.getMessage (classWaiterName+"Stopped"));
			showStatus (Bundle.getMessage (classWaiterName+"Stopped"));
			serverSocket.close ();
		    } catch (IOException e) {
		    }
		}
	    }).start();
    }

    // ========================================
}
