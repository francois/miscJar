package misc;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;
import javax.swing.AbstractButton;
import javax.swing.JMenu;

@SuppressWarnings ("serial")
public class RemoteUpdateManager implements ApplicationManager, ActionListener {

    // ========================================
    static public final String
	actionUpdate					= "Update",
	actionOnline					= "Online";
    static public final List<String> actionsNames	= Arrays.asList (actionUpdate, actionOnline);
    @SuppressWarnings("unchecked")
    static public final Hashtable<String, Method> actionsMethod =
	Util.collectMethod (RemoteUpdateManager.class, actionsNames);
    public void  actionPerformed (ActionEvent e) {
	Util.actionPerformed (actionsMethod, e, this);
    }

    // ========================================
    public void addMenuItem (JMenu... jMenu) {
	Util.addMenuItem (actionsNames, this, jMenu[0]);
    }
    public void addIconButtons (Container... containers) {
	Util.addIconButton (actionsNames, this, containers[0]);
    }
    private ArrayList<AbstractButton> enabledConnectedButtons = new ArrayList<AbstractButton> ();
    public void addActiveButtons (Hashtable<String, AbstractButton> buttons) {
	enabledConnectedButtons.add (buttons.get (actionOnline));
	updateActiveButtons ();
    }
    public void updateActiveButtons () {
	for (AbstractButton button : enabledConnectedButtons)
	    button.setEnabled (connected);
    }
    public void actionUpdate () {
	jRemoteUpdate.downloadDialog ();
    }
    public void actionOnline () {
	jRemoteUpdate.uploadDialog ();
    }

    // ========================================
    private boolean connected = false;
    private RemoteUpdate remoteUpdate;
    private JRemoteUpdate jRemoteUpdate;

    public void setConnected (boolean connected) { this.connected = connected; updateActiveButtons (); }
    public RemoteUpdate getRemoteUpdate () { return remoteUpdate; }
    public JRemoteUpdate getJRemoteUpdate () { return jRemoteUpdate; }

    public RemoteUpdateManager (OwnFrame controller, RemoteUpdate remoteUpdate, Runnable actionAfterUpdate) {
	this.remoteUpdate = remoteUpdate;
	jRemoteUpdate = new JRemoteUpdate (controller, remoteUpdate, actionAfterUpdate);
    }

    public void check () {
	(new Thread () { public void run () {
	    if (remoteUpdate.hasNewVersion ())
		jRemoteUpdate.downloadDialog ();
	} }).start ();
    }

    // ========================================
}
