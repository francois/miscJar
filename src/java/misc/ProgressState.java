package misc;

import java.util.HashSet;

public class ProgressState {

    // ========================================
    HashSet<Thread> workingThreads = new HashSet<Thread> ();
    StateNotifier observable;
    String valueName;

    static public enum State {Init, Value, End};
    public State state;
    public String domain;
    public int value;
    public int maxValue;

    // ========================================
    public ProgressState (StateNotifier observable, String valueName) {
	this.observable = observable;
	this.valueName = valueName;
    }
    // ========================================
    public synchronized void init (String domain, int maxValue) {
	this.domain = domain;
	this.value = 0;
	this.maxValue = maxValue;
	state = State.Init;
	workingThreads = new HashSet<Thread> ();
	workingThreads.add (Thread.currentThread ());
	observable.broadcastUpdate (valueName);
    }
    public synchronized void addThread (Thread thread) {
	// XXX test ! State.end
	workingThreads.add (thread);
    }
    // ========================================
    public synchronized void abort () {
	workingThreads.clear ();
    }
    // ========================================
    public synchronized boolean isInterrupted () {
	return !workingThreads.contains (Thread.currentThread ());
    }
    // ========================================
    public synchronized boolean setValue (int value) {
	this.value = value;
	state = State.Value;
	observable.broadcastUpdate (valueName);
	return workingThreads.contains (Thread.currentThread ());
    }
    // ========================================
    public synchronized boolean addValue (int delta) {
	if (delta > 0) {
	    value += delta;
	    state = State.Value;
	    observable.broadcastUpdate (valueName);
	}
	return workingThreads.contains (Thread.currentThread ());
    }
    // ========================================
    public synchronized void end () {
	state = State.End;
	observable.broadcastUpdate (valueName);
    }
    // ========================================
}
