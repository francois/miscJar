package misc;

import java.util.Comparator;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.Point;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ContainerEvent;
import java.awt.event.ContainerListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;
import java.util.TreeSet;
import java.util.Vector;
import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JToolBar;
import javax.swing.SwingConstants;
import javax.swing.border.Border;
import javax.swing.plaf.basic.BasicButtonUI;
import javax.swing.table.AbstractTableModel;

import misc.TitledDialog;
import static misc.Util.GBCNL;

/**
   ...
*/

public class ToolBarManager implements ApplicationManager, ActionListener, ContainerListener, SwingConstants {

    // ========================================
    static public BasicButtonUI buttonUI = new BasicButtonUI ();
    static public Border buttonBorder = BorderFactory.createEmptyBorder (2, 2, 2, 2);

    static public final String
	actionUp						= "Up",
	actionDown					= "Down",
	actionToolBarProfil = "ToolBarProfil";
    static public final List<String> actionsToolBar =
	Arrays.asList (actionToolBarProfil);
    @SuppressWarnings("unchecked")
    static public final Hashtable<String, Method> actionsMethod =
	Util.collectMethod (ToolBarManager.class, actionsToolBar);
    public void actionPerformed (ActionEvent e) {
	try {
	    AbstractButton button = ((AbstractButton) e.getSource ());
	    Component component = toolBarComponent.get (button.getActionCommand ());
	    if (component != null) {
		if (button.isSelected ())
		    show (component);
		else
		    hide (component);
		return;
	    }
	} catch (Exception e2) {
	    Log.keepLastException ("ToolBarManager::actionPerformed", e2);
	}
	Util.actionPerformed (actionsMethod, e, this);
    }
    public void addMenuItem (JMenu... jMenu) {
	Util.addMenuItem (actionsToolBar, this, jMenu[0]);
	((MultiToolBarBorderLayout) container.getLayout ()).setMenu (jMenu[0].getParent ());
    }
    public void addIconButtons (Container... containers) {
	Util.addIconButton (actionsToolBar, this, containers[0]);
    }
    public void addActiveButtons (Hashtable<String, AbstractButton> buttons) {
	// disable if no toolbar to manage (orderedComponents.size () < 1 => container listener)
    }
    // ========================================
    @SuppressWarnings ("serial") public class TableObjectModel extends AbstractTableModel {
	public int getColumnCount () { return 2; }
	public int getRowCount () { return orderedComponents.size (); }
	public Object getValueAt (int row, int column) {
	    if (row >= orderedComponents.size ())
		return null;
	    return column == 0 ?
		orderedComponents.get (row).getParent () != null :
		Bundle.getTitle (toolBarName.get (orderedComponents.get (row)));
	}
	public String getColumnName (int column) {
	    return column == 0 ? Bundle.getLabel ("ToolBarShowed") : Bundle.getLabel ("ToolBarNamed");
	}
	public Class<?> getColumnClass (int column) {
	    return column == 0 ? Boolean.class : String.class;
	}
	public boolean isCellEditable (int row, int column) {
	    return column == 0;
	}
	public void setValueAt (Object aValue, int row, int column) {
	    if (column > 0 || row >= orderedComponents.size ())
		return;
	    Component component = orderedComponents.get (row);
	    if ((Boolean) aValue)
		show (component);
	    else
		hide (component);
	}
	public void move (int delta) {
	    int idx = table.getSelectedRow ();
	    if (idx < 0)
		return;
	    int mod = orderedComponents.size ();
	    Component old =  orderedComponents.remove (idx);
	    idx = (idx+delta+mod) % mod;
	    if (idx == orderedComponents.size ())
		orderedComponents.add (old);
	    else
		orderedComponents.add (idx, old);
	    table.revalidate ();
	    table.getSelectionModel ().setLeadSelectionIndex (idx);
	    container.getLayout ().layoutContainer (container);
	}
    };
    TableObjectModel dataObjectModel = new TableObjectModel ();
    JTable table = new JTable (dataObjectModel);
    public void actionToolBarProfil () {
	JPanel panel = new JPanel (new BorderLayout ());
	panel.add (Util.getJScrollPane (table), BorderLayout.CENTER);
	JPanel leftPropCmdPanel = new JPanel ();
	panel.add (leftPropCmdPanel, BorderLayout.SOUTH);
	leftPropCmdPanel.add (Util.newIconButton (actionUp, new ActionListener () {
		public void  actionPerformed (ActionEvent e) { dataObjectModel.move (-1); }
	    }));
	leftPropCmdPanel.add (Util.newIconButton (actionDown, new ActionListener () {
		public void  actionPerformed (ActionEvent e) { dataObjectModel.move (+1); }
	    }));
	Hashtable<String, AbstractButton> buttons = Util.collectButtons (null, leftPropCmdPanel);
	Util.collectButtons (buttons, leftPropCmdPanel);
	for (AbstractButton button : buttons.values ()) {
	    button.setUI (buttonUI);
	    button.setBorder (buttonBorder);
	}
	//table.getTableHeader ().setReorderingAllowed (true);
	table.setShowHorizontalLines (false);
	table.setShowVerticalLines (false);
	table.setRowSelectionAllowed (true);
	table.setColumnSelectionAllowed (false);
	table.setSelectionMode (0);
	Dimension preferredSize = new Dimension (table.getPreferredSize ());
	preferredSize.height += 40;
	table.getParent ().getParent ().setPreferredSize (preferredSize);
	for (int row = 0; row < table.getRowCount (); row++)
	    // check bundle before grab dialog
	    table.getValueAt (row, 1);
	JOptionPane.showMessageDialog (frame, panel);
	int i = 0;
	for (Component component : orderedComponents)
	    Config.setInt (toolBarName.get (component)+"Order", i++);
    }

    // ========================================
    /** Les points cardinaux sur lesquels l'orientation des boîtes doit être horizontal. */
    static final List<Integer> horizontalCardinal = Arrays.asList (NORTH, SOUTH);

    // ========================================
    /** receptacle qui accueille les boîtes (doit avoir une mise en forme de type bordure "BorderLayout"). */
    private Container container;
    /**
     * Nom donné à chaque boîte. Ce nom est utilisé comme racine pour trouver des valeur dans la configuration ou l'internationnalisation.
     * Exemple si le nom est  "Horloge" on trouvera dans la configuration :
     *  "HorlogeChecked" (true indiquant que la boîte est visible) et
     *  "HorlogePlace" (East indiquant que la boîte s'attachera à droite)
     * On trouvera dans le fichier des 
     *  "ActionHorloge" associant le texte à la case à cocher ("Montre l'horloge" pour le texte en français)
     * Enfin un évenement de type "Horloge" pourra être traité par "actionPerformed".
     */
    private Hashtable<Component, String> toolBarName = new Hashtable<Component, String>();
    private Hashtable<Component, Integer> toolBarOrder = new Hashtable<Component, Integer>();
    private ArrayList<Component> orderedComponents = new ArrayList<Component> ();
    /** Assoication permettant de trouver un composant d'après son nom. */
    private Hashtable<String, Component> toolBarComponent = new Hashtable<String, Component>();
    /** mémorise la position relative (points cardinaux) de la boîte lorsqu'elle est attachée. */
    private Hashtable<Component, String> toolBarPlace = new Hashtable<Component, String>();
    /** Liste des cases à cocher associé à la visibilité d'une boîte. */
    private Hashtable<String, Vector<AbstractButton>> nameCheckBox = new Hashtable<String, Vector<AbstractButton>>();

    JFrame frame = new JFrame ();
    // ========================================
    /**
     * créer un gestionnaire de boîte à outil détachable gravitant autour d'un receptacle.
     * Le receptacle doit avoir une mise en forme de type bordure "BorderLayout".
     * Le gestionnaire se mets automatiquement à l'écoute de l'ajout et de la suppression de composant qui peuvent résulter de l'attachement ou du détachment de boîte.
     */
    public ToolBarManager (Image icon, Container container) {
	this.container = container;
	((MultiToolBarBorderLayout) container.getLayout ()).setLayoutOrderedComponents (orderedComponents);
	frame.setIconImage (icon);
	container.addContainerListener (this);
    }

    // ========================================
    public JToolBar newJToolBar (String toolBarId, String defaultCardinalPoint) {
	JToolBar toolBar = new JToolBar (toolBarId);
	// XXX
	//Bundle.addLocalizedActioner (toolBar);
	add (toolBar, defaultCardinalPoint);
	return toolBar;
    }

    /**
     * Enregistre un composant détachable.
     * Il ne devrait pas y avoir plus de 4 boîtes visible en même temps (au maximum un par point cardinal).
     */
    public void add (Component component, String defaultCardinalPoint) {
	String name = component.getName (); //+toolBarPostfix;
	//Util.getClassName (component.getClass ());
	if (toolBarName.contains (component))
	    throw new IllegalArgumentException (MessageFormat.format (Bundle.getException ("AlreadyRegistredTool"),
								      name, defaultCardinalPoint));
	toolBarName.put (component, name);
	toolBarComponent.put (name, component);
	nameCheckBox.put (name, new Vector<AbstractButton>());
	toolBarOrder.put (component, Config.getInt (name+"Order", toolBarOrder.size ()));
	orderedComponents.add (component);
	orderedComponents.sort (componentsComparator);
	String place = Config.getString (name+"Place", defaultCardinalPoint);
	Integer cardinal = MultiToolBarBorderLayout.positionStringToInt.get (place);
	if (cardinal == null || cardinal == CENTER)
	    throw new IllegalArgumentException (MessageFormat.format (Bundle.getException ("NotCardinalPoint"), place));
	put (component, place);
	showIfVisible (component);
    }

    // ========================================
    /**
     * Ajoute un case à cocher concernée par la visibilité d'une boîte détachable.
     * La case sera mise à jour suivant le changement de visibilité du composant.
     * Le gestionaire de boîte se mets automatiquement à l'écoute du changement d'état de la case (que cela soit un JButton ou un JCheckBoxMenuItem).
     */
    public void addCheckBox (AbstractButton checkbox) {
	String name = checkbox.getActionCommand ();
	Vector<AbstractButton> buttons = nameCheckBox.get (name);
	if (buttons == null)
	    return;
	buttons.add (checkbox);
	checkbox.addActionListener (this);
    }

    // ========================================
    /**
     * les seuls évènement pris en charge provienne du changement d'état de case à cocher.
     * L'action doit avoir le même nom que celui utilisé pour désigner la boîte détachable.
     */

    // ========================================
    /** Mets à jour les boîtes à cocher refletant la visibilité d'une boîte à outils. */
    public void checkBoxComponent (Component component) {
	boolean showed = component.getParent () != null;
	for (AbstractButton checkbox : nameCheckBox.get (toolBarName.get (component))) {
	    try {
		checkbox.getClass ().getMethod ("setState", boolean.class).invoke (checkbox, showed);
	    } catch (NoSuchMethodException e) {
		Log.keepLastException ("ToolBarManager::checkBoxComponent", e);
	    } catch (InvocationTargetException e) {
		Log.keepLastException ("ToolBarManager::checkBoxComponent", e);
	    } catch (IllegalAccessException e) {
		Log.keepLastException ("ToolBarManager::checkBoxComponent", e);
	    }
	}
    }

    // ========================================
    /** Creer une boîte de dialogue pour une boîte detache au lancement de l'application. */
    private void newUndocked (final Component component) {
	final String name = toolBarName.get (component);
	final TitledDialog jdialog = new TitledDialog (frame, name);
	jdialog.add (component);
	((JToolBar) component).setFloatable (false);
	String place = get (component);
	try {
	    int cardinal = MultiToolBarBorderLayout.positionStringToInt.get (place);
	    ((JToolBar) component).setOrientation (horizontalCardinal.contains (cardinal) ?
						   SwingConstants.HORIZONTAL : SwingConstants.VERTICAL);
	} catch (Exception e) {
	}
	((JToolBar) component).addContainerListener (new ContainerListener () {
		public void componentAdded (ContainerEvent e) { Util.packWindow (component); }
		public void componentRemoved (ContainerEvent e) { Util.packWindow  (component); }
	    });

	jdialog.addWindowListener (new WindowAdapter () {
		public void windowClosing (WindowEvent e) {
		    jdialog.remove (component);
		    ((JToolBar) component).setFloatable (true);
		    show (component);
		}
	    });
	jdialog.setLocationRelativeTo (null);
	Config.loadLocation (name, jdialog, jdialog.getLocation ());
	jdialog.setVisible (true);
	jdialog.pack ();
    }

    public void saveLocation () {
	for (Component component : toolBarName.keySet ()) {
	    if (component.getParent () == null || container.isAncestorOf (component))
		continue;
	    Config.saveLocation (toolBarName.get (component), getUndocked (component));
	}
    }

    // ========================================
    /** Montre le composant au lancement de l'application s'il est marqué comme visible dans le fichier de configuration. */
    public void showIfVisible (final Component component) {
	if (Config.getBoolean (toolBarName.get (component)+Config.checkedPostfix, true))
	    if (Config.getBoolean (toolBarName.get (component)+Config.undockedPostfix, false))
		newUndocked (component);
	    else {
								
		show (component);
	    }
    }

    // ========================================
    /** Montre le composant, adapte l'orientation (horizontal ou non), mémorise l'état dans le fichier de configuration et mets à jour les cases à cocher. */
    public void show (Component component) {
	String place = get (component);
	try {
	    int cardinal = MultiToolBarBorderLayout.positionStringToInt.get (place);
	    ((JToolBar) component).setOrientation (horizontalCardinal.contains (cardinal) ?
						   SwingConstants.HORIZONTAL : SwingConstants.VERTICAL);
	} catch (Exception e){
	}
	Container parent = component.getParent ();
	if (parent!= null)
	    return;
	((JToolBar) component).setFloatable (true); 
	container.invalidate ();
	container.add (component, place);
	container.validate ();
	Config.setBoolean (toolBarName.get (component)+Config.checkedPostfix, true);
	checkBoxComponent (component);
    }

    // ========================================
    private Container getUndocked (Component component) {
	Container frame = component.getParent ();
	for (;;) {
	    if (frame instanceof Window)
		return frame;
	    frame = frame.getParent ();
	}
    }

    // ========================================
    /** Cache le composant, mémorise l'état dans le fichier de configuration et mets à jour les cases à cocher. */
    public void hide (Component component) {
	Container parent = component.getParent ();
	if (parent == null)
	    return;
	if (container.isAncestorOf (component)) {
	    container.invalidate ();
	    container.remove (component);
	    container.validate ();
	} else {
	    try {
		getUndocked (component).setVisible (false);
	    } catch (Exception e) {
	    }
	    parent.remove (component);
	}
	Config.setBoolean (toolBarName.get (component)+Config.checkedPostfix, false);
	checkBoxComponent (component);
    }

    // ========================================
    /** Change l'état de visibilité de la boîte détachable. */
    public void showHide (Component component) {
	if (component.getParent () == null)
	    show (component);
	else
	    hide (component);
    }

    // ========================================
    /** Recherche le point cardinal réel d'une boîte detachable dans le receptacle (exemple : cas d'une boîte vient d'être réttachée). */
    private String find (Component component) {
	return (String) ((MultiToolBarBorderLayout) container.getLayout ()).getConstraints (component);
    }

    // ========================================
    /** Recherche un point cardinal pour une boîte. */
    private String get (Component component) {
	String place = toolBarPlace.get (component);
	if (place != null)
	    return place;
	place = BorderLayout.NORTH;
	put (component, place);
	return place;
    }

    // ========================================
    /** Mémorise, sans controle, le point cardinal pour une boîte (y compris dans le fichier de configuration). */
    private void put (Component component, String place) {
	toolBarPlace.put (component, place);
	Config.setString (toolBarName.get (component)+"Place", place);
    }

    // ========================================
    /**
     * Traite l'ajout d'une boîte détachable par insertion (coche d'une case ou fermeture de sa boîte de dialogue quand elle est détachée).
     * Vérifie si elle attérie sur une place déjà prise.
     */
    public void componentAdded (ContainerEvent e) {
	Component component = e.getChild ();
	if (! toolBarName.keySet ().contains (component))
	    // boîte non géré par ce gestionnaire
	    return;
	String place = find (component);
	put (component, place);
	Util.packWindow (container);
	Config.setBoolean (toolBarName.get (component)+Config.undockedPostfix, false);
    }

    // ========================================
    /**
     * Traite le retrait d'une boîte détachable par suppression (décoche d'une case ou détachement vers une doite de dialogue).
     */
    public void componentRemoved (ContainerEvent e) {
	Util.packWindow (container);
	Component component = e.getChild ();
	if (! toolBarName.keySet ().contains (component))
	    // boîte non géré par ce gestionnaire
	    return;
	Config.setBoolean (toolBarName.get (component)+Config.undockedPostfix, true);
    }

    // ========================================
    public Comparator<Component> componentsComparator =
	new Comparator<Component> () {
	public int compare (Component o1, Component o2) {
	    Integer i1 = toolBarOrder.get (o1);
	    Integer i2 = toolBarOrder.get (o2);
	    if (i1 == i2)
		return o1.hashCode ()-o2.hashCode ();
	    if (i1 == null || i2 == null)
		return i2 == null ? 1 : -1;
	    return i1-i2;
	}
    };

    // ========================================
}
