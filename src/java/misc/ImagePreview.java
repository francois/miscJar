package misc;

import javax.swing.*;
import java.beans.*;
import java.awt.*;
import java.io.File;

/* ImagePreview.java by FileChooserDemo2.java. */
@SuppressWarnings("serial")
public class ImagePreview extends JComponent
    implements PropertyChangeListener {

    static public int maxInSide = 90;
    static public int border = 5;				
    static public int maxOutSide = maxInSide+2*border;

    protected ImageIcon thumbnail = null;
    protected File file = null;
    protected long maxSize;

    public ImagePreview (JFileChooser fc, long maxSize) {
	this.maxSize = maxSize;
	setPreferredSize (new Dimension (maxOutSide, maxOutSide));
	fc.addPropertyChangeListener (this);
    }

    public void loadImage () {
	if (file == null || (maxSize > 0 && file.length () > maxSize)) {
	    thumbnail = null;
	    return;
	}
	//Don't use createImageIcon (which is a wrapper for getResource)
	//because the image we're trying to load is probably not one
	//of this program's own resources.
	ImageIcon tmpIcon = new ImageIcon (file.getPath ());
	if (tmpIcon != null) {
	    thumbnail = tmpIcon;
	    if (tmpIcon.getIconWidth () > tmpIcon.getIconHeight ()) {
		if (tmpIcon.getIconWidth () > maxInSide)
		    thumbnail =
			new ImageIcon (tmpIcon.getImage ().getScaledInstance (maxInSide, -1, Image.SCALE_DEFAULT));
	    } else {
		if (tmpIcon.getIconHeight () > maxInSide)
		    thumbnail =
			new ImageIcon (tmpIcon.getImage ().getScaledInstance (-1, maxInSide, Image.SCALE_DEFAULT));

	    }
	}
    }

    public void propertyChange (PropertyChangeEvent e) {
	boolean update = false;
	String prop = e.getPropertyName ();

	//If the directory changed, don't show an image.
	if (JFileChooser.DIRECTORY_CHANGED_PROPERTY.equals (prop)) {
	    file = null;
	    update = true;

	    //If a file became selected, find out which one.
	} else if (JFileChooser.SELECTED_FILE_CHANGED_PROPERTY.equals (prop)) {
	    file = (File) e.getNewValue ();
	    update = true;
	}

	//Update the preview accordingly.
	if (update) {
	    thumbnail = null;
	    if (isShowing ()) {
		loadImage ();
		repaint ();
	    }
	}
    }

    protected void paintComponent (Graphics g) {
	if (thumbnail == null)
	    loadImage ();
	if (thumbnail != null) {
	    int x = getWidth ()/2 - thumbnail.getIconWidth ()/2;
	    int y = getHeight ()/2 - thumbnail.getIconHeight ()/2;
	    if (y < border)
		y = border;
	    if (x < border)
		x = border;
	    thumbnail.paintIcon (this, g, x, y);
	}
    }
}
