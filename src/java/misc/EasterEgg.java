package misc;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class EasterEgg {
  public static final SimpleDateFormat birthFormat = new SimpleDateFormat("MMdd");
  
  String defaultValue;
  
  class Periode {
    String start;
    
    String stop;
    
    String value;
    
    Periode(String start, String stop, String value) {
      this.start = start;
      this.stop = stop;
      this.value = value;
    }
  }
  
  ArrayList<Periode> periodes = new ArrayList<>();
  
  public EasterEgg(String defaultValue) {
    this.defaultValue = defaultValue;
  }
  
  public void addPeriode(String start, String stop, String value) {
    this.periodes.add(new Periode(start, stop, value));
  }
  
  public String getValue() {
    return get(new Date());
  }
  
  public String get(Date date) {
    String day = birthFormat.format(date);
    for (Periode periode : this.periodes) {
      if (periode.start.compareTo(periode.stop) <= 0) {
        if (day.compareTo(periode.start) >= 0 && day.compareTo(periode.stop) <= 0)
          return periode.value; 
        continue;
      } 
      if (day.compareTo(periode.start) <= 0 || day.compareTo(periode.stop) >= 0)
        return periode.value; 
    } 
    return this.defaultValue;
  }
}
