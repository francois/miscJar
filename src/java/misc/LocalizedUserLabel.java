// ================================================================================
// François MERCIOL 2015
// Name		: LocalizedUserLabel.java
// Language	: Java
// Author	: François Merciol
// CopyLeft	: Cecil B
// Creation	: 2015
// Version	: 0.1 (xx/xx/xx)
// ================================================================================
package misc;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Locale;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;

import static misc.Util.GBC;
import static misc.Util.GBCNL;

public class LocalizedUserLabel {

    // ========================================
    protected boolean isCustomized;
    protected String labelName;
    protected String currentLabel;
    protected String newLabel;
    protected JLabel jLabel;

    public String getLabel () { return labelName; }
    public String getNewLabel () { return isCustomized ? newLabel : null; }
    public String getCurrentLabel () { return currentLabel; }
    public JLabel getJLabel () { return jLabel; }

    // ========================================
    public LocalizedUserLabel (String labelName, boolean isLocalized, boolean admin) {
	isCustomized = isLocalized && admin;
	this.labelName = labelName;
	currentLabel = isLocalized ? Bundle.getUser (labelName) : labelName;
	jLabel = new JLabel (currentLabel, SwingConstants.RIGHT);
	if (!isCustomized)
	    return;
	jLabel.addMouseListener (new MouseAdapter () {
		public void mousePressed (MouseEvent e) {
		    LocalizedUserLabel.this.mousePressed (e);
		}
	    });
    }

    // ========================================
    public void mousePressed (MouseEvent e) {
	if (!isCustomized || !SwingUtilities.isRightMouseButton (e))
	    return;
	Locale locale = Bundle.getLocale ();
	ImageIcon flag = Util.loadImageIcon (Config.dataDirname, Config.iconsDirname, Config.flagsDirname,
					     locale.getCountry ().toLowerCase () + Config.iconsExt);
	if (flag != null)
	    flag.setDescription (locale.toString ());
	JLabel jLocal = new JLabel (locale.toString (), flag, SwingConstants.LEFT);
	JTextField jValue = new JTextField (newLabel == null ? Bundle.getUser (labelName) : newLabel, 9);
	JPanel content = Util.getGridBagPanel ();
	Util.addComponent (new JLabel (), content, GBC);
	Util.addComponent (jLocal, content, GBCNL);
	Util.addComponent (new JLabel (labelName+" : ", SwingConstants.RIGHT), content, GBC);
	Util.addComponent (jValue, content, GBCNL);
	if (JOptionPane.showConfirmDialog (e.getComponent (), content, Bundle.getTitle ("Localized"),
					   JOptionPane.YES_NO_OPTION) != JOptionPane.YES_OPTION)
	    return;
	currentLabel = newLabel = jValue.getText ();
	jLabel.setText (currentLabel);
    }

    // ========================================
}
