package misc;

import java.io.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

@SuppressWarnings ("serial") public class JConsole extends TitledDialog {

    // ========================================
    private JTextArea textArea = new JTextArea (5, 20);
    public static boolean copy = true;
    
    // ========================================
    public JConsole (Frame frame) {
	super (frame, "JConsole");
	textArea.setEditable (false);
	setPreferredSize (new Dimension (550, 300));
	getContentPane ().add (new JScrollPane (textArea), BorderLayout.CENTER);
	getContentPane ().add (Util.newButton ("Clear", new ActionListener () {
		public void actionPerformed (ActionEvent evt) {
		    synchronized (textArea) {
			textArea.setText ("");
		    }
		}
	    }), BorderLayout.SOUTH);
	if (copy) {
	    copyOut (true);
	    copyOut (false);
	}
	pack ();
    }


    // ========================================
    public void copyOut (boolean err) {
	Thread thread = new Thread () {
		public void run () {
		    for (;;) {
			try {
			    PipedInputStream pin = new PipedInputStream ();
			    PrintStream out = new PrintStream (new PipedOutputStream (pin), true, "UTF-8");
			    if (err)
				System.setErr (out);
			    else
				System.setOut (out);
			    BufferedReader in = new BufferedReader (new InputStreamReader (pin));
			    for (;;) {
				String line = in.readLine ();
				synchronized (textArea) {
				    textArea.append (line+"\n");
				}
			    }
			} catch (Exception e) {
			    textArea.append (e.toString());
			    ByteArrayOutputStream buf = new ByteArrayOutputStream ();
			    e.printStackTrace (new PrintStream (buf));
			    textArea.append (buf.toString ());
			    try {
				Thread.sleep (1000);
			    } catch (InterruptedException e2) {
			    }
			}
		    }
		}
	    };
	thread.setDaemon (true);
	thread.start ();
    }

    // ========================================
    public static void main (String[] arg) {
	new JConsole (null);
    }

    // ========================================
}
