package misc;

public class Timestamp {
    // ========================================
    private long changeTimestamp, updateTimestamp;
    public long[] getValues () { return new long[]{changeTimestamp, updateTimestamp}; }

    // ========================================
    public void reset () {
	changeTimestamp = updateTimestamp = System.currentTimeMillis ();
    }

    // ========================================
    public void change () {
	changeTimestamp = System.currentTimeMillis ();
    }

    // ========================================
    public void update () {
	updateTimestamp = System.currentTimeMillis ();
    }

    // ========================================
    public boolean isUpdated (Timestamp... others) {
	if (changeTimestamp > updateTimestamp)
	    return false;
	for (Timestamp other : others)
	    if (other.updateTimestamp > updateTimestamp)
		return false;
	return true;
    }

    // ========================================
}
