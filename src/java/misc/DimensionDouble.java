// ================================================================================
// François MERCIOL 2015
// Name		: Dimension2D.java
// Language	: Java
// Author	: François Merciol
// CopyLeft	: Cecil B
// Creation	: 2015
// Version	: 0.1 (xx/xx/xx)
// ================================================================================
package misc;

import java.awt.geom.Dimension2D;

@SuppressWarnings ("overrides")
public class DimensionDouble extends Dimension2D {

    // ========================================
    public double width, height;

    public DimensionDouble () {
    }

    public DimensionDouble (double width, double height) {
	this.width = width;
	this.height = height;
    }

    public boolean equals (Object obj) {
	try {
	    DimensionDouble dim = (DimensionDouble) obj;
	    return width == dim.width && height == dim.height;
	} catch (Exception e) {
	    return false;
	}
    }

    // ========================================
    public double getHeight () {
	return height;
    }

    public double getWidth () {
	return width;
    }

    public void setSize (Dimension2D d) {
	width = d.getWidth ();
	height = d.getHeight ();
    }

    public void setSize (double width, double height) {
	this.width = width;
	this.height = height;
    }

    // ========================================
    public String toString () {
	return DimensionDouble.class.getSimpleName ()+"[width="+width+",height="+height+"]";
    }

    // ========================================
}
