// ================================================================================
// François MERCIOL 2015
// Name		: ScaledImage.java
// Language	: Java
// Author	: François Merciol
// CopyLeft	: Cecil B
// Creation	: 2015
// Version	: 0.1 (xx/xx/xx)
// ================================================================================
package misc;

import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;
import java.awt.geom.Dimension2D;
import java.awt.geom.Point2D;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.util.Hashtable;
import javax.swing.ImageIcon;

public class ScaledImage {

    static public class ImageInfo {
	public AffineTransform at;
	public double [] bounds;
	public double x, y;
	Hashtable<Dimension, ImageIcon> tiles = new Hashtable<Dimension, ImageIcon> ();
	public ImageInfo (AffineTransform at, double [] bounds, double x, double y) {
	    this.at = at;
	    this.bounds = bounds;
	    this.x = x;
	    this.y = y;
	}
    }
    // ========================================
    static public final RenderingHints renderingHints;
    static {
	renderingHints = new RenderingHints (RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
	// renderingHints.put (RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
	// renderingHints.put (RenderingHints.KEY_ALPHA_INTERPOLATION, RenderingHints.VALUE_ALPHA_INTERPOLATION_QUALITY);
	// renderingHints.put (RenderingHints.KEY_DITHERING, RenderingHints.VALUE_DITHER_ENABLE);
	// renderingHints.put (RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC);
    }

    static public final int defaultAdjust = 8;
    public BufferedImage reference;
    public boolean horizontalSpin, verticalSpin;

    Hashtable<Integer, Hashtable<Dimension, ImageInfo>> scalesRotations = new Hashtable<Integer, Hashtable<Dimension, ImageInfo>> ();

    public ScaledImage (BufferedImage image) {
	reference = image;
    }
    public ScaledImage (BufferedImage image, boolean horizontalSpin, boolean verticalSpin) {
	reference = image;
	this.horizontalSpin = horizontalSpin;
	this.verticalSpin = verticalSpin;
    }

    public ScaledImage getNewSpin (boolean horizontalSpin, boolean verticalSpin) {
	if (this.horizontalSpin == horizontalSpin && this.verticalSpin == verticalSpin)
	    return this;
	return new ScaledImage (reference, this.horizontalSpin ^ horizontalSpin, this.verticalSpin ^ verticalSpin);
    }

    public void changeReference (BufferedImage image) {
	reference = image;
	clearCache ();
    }

    public void clearCache () {
	scalesRotations.clear ();
    }

    // ========================================
    public AffineTransform getAffineTransform (Dimension2D size, double theta) {
	int width = reference.getWidth ();
	int height = reference.getHeight ();
	double scaleX = size.getWidth () / width;
	double scaleY = size.getHeight () / height;
	if (scaleX == 0 || scaleY == 0)
	    return null;
	if (horizontalSpin)
	    scaleX = -scaleX;
	if (verticalSpin)
	    scaleY = -scaleY;
	AffineTransform at = AffineTransform.getRotateInstance (Math.toRadians ((int) Math.toDegrees (theta)));
	at.scale (scaleX, scaleY);
	at.translate (-width/2., -height/2.);
	return at;
    }

    static public final Point2D ORIGINE = new Point2D.Double (0, 0);
    static public final Dimension ONE_TILE = new Dimension (1, 1);
    public ImageIcon get (Dimension size, double theta) {
	return get (size, theta, ONE_TILE);
    }
    public ImageIcon get (Dimension size, double theta, Dimension tile) {
	if (tile.width <= 0 || tile.height <= 0)
	    return null;
	ImageInfo imageInfo = getInfo (size, theta);
	if (imageInfo == null)
	    return null;
	ImageIcon imageIcon = imageInfo.tiles.get (tile);
	if (imageIcon != null)
	    return imageIcon;
	AffineTransformOp op = new AffineTransformOp (imageInfo.at, renderingHints);
	BufferedImage image = op.createCompatibleDestImage (reference, null);
	Graphics2D printGraphics = (Graphics2D) image.getGraphics ();
	printGraphics.setRenderingHint (RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
	print (printGraphics, null, imageInfo, tile);
	imageIcon = new ImageIcon (image);
	imageInfo.tiles.put (tile, imageIcon);
	return imageIcon;
    }
    public void print (Graphics2D printGraphics, Point2D pos, ImageInfo imageInfo, Dimension tile) {
	if (pos != null)
	    printGraphics.translate (pos.getX ()+imageInfo.x, pos.getY ()+imageInfo.y);
	if (ONE_TILE.equals (tile))
	    printGraphics.drawImage (reference, imageInfo.at, null);
	else {
	    double width = reference.getWidth (), height = reference.getHeight ();
	    double scaleX = 1.0/tile.width, scaleY = 1.0/tile.height;
	    double stepX = width/tile.width, stepY = height/tile.height;
	    for (int i = 0; i < tile.width; i++)
		for (int j = 0; j < tile.height; j++) {
		    AffineTransform at = new AffineTransform (imageInfo.at);
		    at.translate (i*stepX, j*stepY);
		    at.scale (scaleX, scaleY);
		    printGraphics.drawImage (reference, at, null);
		}
	}
	if (pos != null)
	    printGraphics.translate (-pos.getX ()-imageInfo.x, -pos.getY ()-imageInfo.y);
    }
    public void print (Graphics2D printGraphics, Point2D pos, DimensionDouble size, double theta, Dimension tile) {
	print (printGraphics, pos, newInfo (size, theta), tile);
    }

    public double[] getBounds (Dimension size, double theta) {
    	ImageInfo imageInfo = getInfo (size, theta);
    	if (imageInfo == null)
    	    return null;
    	return imageInfo.bounds;
    }
    public ImageInfo getInfo (Dimension size, double theta) {
	int angle = (int) Math.toDegrees (theta);
	Hashtable<Dimension, ImageInfo> rotated = scalesRotations.get (angle);
	if (rotated == null) {
	    rotated = new Hashtable<Dimension, ImageInfo> ();
	    scalesRotations.put (angle, rotated);
	}
	ImageInfo imageInfo = rotated.get (size);
	if (imageInfo != null)
	    return imageInfo;
	imageInfo = newInfo (new DimensionDouble (size.width, size.height), theta);
	if (imageInfo == null)
	    return null;
	rotated.put (size, imageInfo);
	return imageInfo;
    }
    public ImageInfo newInfo (DimensionDouble size, double theta) {
	int width = reference.getWidth ();
	int height = reference.getHeight ();
	double scaleX = size.getWidth () / width;
	double scaleY = size.getHeight () / height;
	if (scaleX == 0 || scaleY == 0)
	    return null;
	if (horizontalSpin)
	    scaleX = -scaleX;
	if (verticalSpin)
	    scaleY = -scaleY;
	AffineTransform at = getAffineTransform (size, theta);
	double [] bounds = new double [] {0, 0, 0, height, width, height, width, 0};
	at.transform (bounds, 0, bounds, 0, 4);
	double dx = bounds[0];
	double dy = bounds[1];
	for (int i = 1; i < 4; i++) {
	    dx = Math.min (dx, bounds [i*2]);
	    dy = Math.min (dy, bounds [i*2+1]);
	}
	at.preConcatenate (AffineTransform.getTranslateInstance (-dx, -dy));
	return new ImageInfo (at, bounds, dx, dy);
    }

    public ImageInfo newInfo2 (DimensionDouble size, double theta) {
	int width = reference.getWidth ();
	int height = reference.getHeight ();
	double scaleX = size.getWidth () / width;
	double scaleY = size.getHeight () / height;
	if (scaleX == 0 || scaleY == 0)
	    return null;
	if (horizontalSpin)
	    scaleX = -scaleX;
	if (verticalSpin)
	    scaleY = -scaleY;
	AffineTransform at = AffineTransform.getRotateInstance (theta);
	at.scale (scaleX, scaleY);
	at.translate (-width/2., -height/2.);
	double [] bounds = new double [] {0, 0, 0, height, width, height, width, 0};
	at.transform (bounds, 0, bounds, 0, 4);
	double dx = bounds[0];
	double dy = bounds[1];
	for (int i = 1; i < 4; i++) {
	    dx = Math.min (dx, bounds [i*2]);
	    dy = Math.min (dy, bounds [i*2+1]);
	}
	at.preConcatenate (AffineTransform.getTranslateInstance (-dx, -dy));
	return new ImageInfo (at, bounds, dx, dy);
    }

    public ImageIcon getSide (int max) {
	int width = reference.getWidth ();
	int height = reference.getHeight ();
	return get (width>height ? new Dimension (max, (height*max)/width) : new Dimension ((width*max)/height, max), 0);
    }

    public ImageIcon getInsideFit (int maxWidth, int maxHeight, int adjust) {
	return getInside (Math.min (reference.getWidth (), (maxWidth/adjust)*adjust),
			  Math.min (reference.getHeight (), (maxHeight/adjust)*adjust));
    }

    public ImageIcon getInside (int maxWidth, int maxHeight) {
	int width = reference.getWidth ();
	int height = reference.getHeight ();
	return get ((width*maxHeight)/height > maxWidth ?
		    new Dimension (maxWidth, (height*maxWidth)/width) :
		    new Dimension ((width*maxHeight)/height, maxHeight), 0);
    }

    public ImageIcon get (DimensionDouble real, double scale, double theta) {
	return get (new Dimension ((int)(real.getWidth ()*scale), (int)(real.getHeight ()*scale)), 0);
    }

    // ========================================
}
