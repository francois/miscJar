// ================================================================================
// Copyright (c) Francois Merciol 2002
// Project	: Perso
// Name		: HourPanel.java
// Language	: Java
// Type		: Source file for HourPanel class
// Author	: Francois Merciol
// Creation	: 02/09/2002
// ================================================================================
// History
// --------------------------------------------------------------------------------
// Author	: 
// Date		: 
// Reason	: 
// ================================================================================
// To be improved :
// ================================================================================
package misc;

import java.awt.Dimension;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JTextField;

/**
   Champ de saisie d'une heure au format HH:mm.
   Autres fonctions :<ul>
   <li> (espace) : éfface le champs
   <li> (retoure chariot) ou (perte de "focus") : remet le champ au format.
   <li> Le premier caractère frappé éfface le champs. Des caractères peuvent être concervé si le prmier caractère
   concerne un déplacement ou un éffacement.
   </ul>
*/
@SuppressWarnings ("serial") public class HourPanel extends JTextField implements KeyListener, FocusListener {

    // ========================================
    /** Format d'une heure. */
    static public final SimpleDateFormat hourFormat = new SimpleDateFormat ("HH:mm");
    /** Ensemble des caractères ayant une action sur ce champs.
	Il faudrais ajouter le retoure chariot et le caractère d'effacement. */
    static public final String acceptChar = "0123456789: ";

    // ========================================
    /** mémorise si le caractère frappé et le premier dans le champs. */
    private boolean justFocus = false;

    // ========================================
    /**
       Création d'une date en reservant une taille maximal puis mis à la date actuelle.
    */
    public HourPanel () {
	super ("23:59");
	addKeyListener (this);
	addFocusListener (this);
	set (new Date ());
    }

    // ========================================
    // api
    // ========================================
    /**
       Remet en forme le champs.
    */
    public void enter () {
	tuneHour ();
    }

    // ========================================
    /**
       Fixe une heure.
       @param date la nouvelle valeur.
    */
    public void set (Date date) {
	setText (hourFormat.format (date));
    }

    // ========================================
    // Listener
    // ========================================
    /** Dernière possition du curseur. */
    int lastJTextPos = -1;

    // ========================================
    /** Interface KeyListener. */
    public void keyPressed (KeyEvent e) {
	lastJTextPos = getCaretPosition ();
    }

    // ========================================
    /** Interface KeyListener. */
    public void keyReleased (KeyEvent e) {
    }

    // ========================================
    /** Interface KeyListener. */
    public void keyTyped (KeyEvent e) {
	char c = e.getKeyChar ();

	switch (c) {
	case KeyEvent.VK_ENTER:
	    enter ();
	    return;
	case ';':
	    // le ; sera s�parateur
	    e.consume ();
	    return;
	case KeyEvent.VK_BACK_SPACE:
	    // on laisse java faire
	    justFocus = false;
	    return;
	case KeyEvent.VK_SPACE:
	    setText ("");
	    e.consume ();
	    return;
	}

	if (acceptChar.indexOf (c) < 0) {
	    e.consume ();
	    return;
	}

	if (getSelectedText () == null) {
	    if (justFocus)
		setText ("");
	    if (c != ':' && getText ().length () >= 5) {
		e.consume ();
		return;
	    }
	} else {
	    String text = getText ();
	    lastJTextPos = getSelectionStart ();
	    setText (text.substring (0, getSelectionStart ())+ text.substring (getSelectionEnd ()));
	}
	justFocus = false;
	processHour (e);
    }

    // ========================================
    /** Interface FocusListener. */
    public void focusGained (FocusEvent e) {
	justFocus = true;
    }

    // ========================================
    /** Interface FocusListener. */
    public void focusLost (FocusEvent e) {
	justFocus = false;
	tuneHour ();
    }

    // ========================================
    // Listener spécial
    // ========================================
    /**
       Traite l'insertion d'un caractère dur champ (chiffre ou ':').
       @param e touche frappé au clavier.
    */
    public void processHour (KeyEvent e) {
	char c = e.getKeyChar ();
	String text = getText ();
	int dpPos = text.indexOf (":");
	if (dpPos >= 0) {
	    if (c == ':') {
		if (lastJTextPos <= dpPos)
		    setText (((lastJTextPos == 0) ? "0" : text.substring (0, lastJTextPos))+":"+
			     text.substring (dpPos+1));
		else
		    setText (text.substring (0, dpPos)+":"+text.substring (lastJTextPos));
		setCaretPosition (text.indexOf (":")+1);
		e.consume ();
		return;
	    }
	} else {
	    if (c == ':')
		return;
	    // traiter l'apparition du :
	    if (text.length () < 1)
		return;
	    if (text.charAt (0) > '2') {
		// XXX pb si lastJTextPos = 0
		setText (text.charAt (0)+":"+text.substring (1));
		setCaretPosition (lastJTextPos+1);
		return;
	    }
	    if (text.length () > 1) {
		// XXX pb si lastJTextPos < 2
		setText (text.substring (0, 2)+":"+text.substring (2));
		setCaretPosition (lastJTextPos+1);
		return;
	    }
	}
    }

    // ========================================
    /**
       Remise en forme de l'heure.
    */
    public void tuneHour () {
	try {
	    String text = getText ().trim ();
	    if (text.isEmpty ()) {
		setText ("00:00");
		return;
	    }
	    switch (text.indexOf (":")) {
	    case -1:
		text += ":0";
		break;
	    case 0:
		text = "0"+text;
	    }
	    if (text.indexOf (":") == (text.length ()-1))
		text += "0";
	    setText (hourFormat.format (hourFormat.parse (text)));
	    getSize (tmpSize);
	    if (tmpSize.width != 0)
		size = tmpSize;
	} catch (Exception ex) {
	}
    }

    // ========================================
    /** Taille actuelle du champ. */
    private Dimension tmpSize = new Dimension ();
    /** Derni�re taille maximum utilisée. */
    private Dimension size;

    // ========================================
    /**
       @return la taille maximum déjà utilisé.
    */
    public Dimension getPreferredSize () {
	if (size != null)
	    return size;
	return super.getPreferredSize ();
    }

    // ========================================
    /**
       @return la taille maximum déjà utilisé.
    */
    public Dimension getMinimumSize () {
	if (size != null)
	    return size;
	return super.getPreferredSize ();
    }

    // ========================================
}
