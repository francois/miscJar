package misc;

import java.awt.Container;
import java.util.Hashtable;
import javax.swing.AbstractButton;
import javax.swing.JMenu;

public interface ApplicationManager {
    public void addMenuItem (JMenu... jMenu);
    public void addIconButtons (Container... containers);
    public void addActiveButtons (Hashtable<String, AbstractButton> buttons);
}
