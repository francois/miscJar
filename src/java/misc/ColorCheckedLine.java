package misc;

public class ColorCheckedLine {

    // ============================================================
    static public final int NBV = 8;
    static public final int NBT = 60;

    /** voir http://www.termsys.demon.co.uk/vtansi.htm */
    static public final String
	MOVE_TO_COL_START = "\033[300C\033[",
	MOVE_TO_COL_END = "D",
	SETCOLOR_SUCCESS = "\033[1;32m",
	SETCOLOR_FAILURE ="\033[1;31m",
	SETCOLOR_WARNING ="\033[1;33m",
	SETCOLOR_NORMAL ="\033[0;39m";

    static public final String
	MOVE_TO_60 = MOVE_TO_COL_START+(80-60)+MOVE_TO_COL_END,
	MSG_OK = MOVE_TO_60+"["+SETCOLOR_SUCCESS+"OK"+SETCOLOR_NORMAL+"]",
	MSG_KO = MOVE_TO_60+"["+SETCOLOR_FAILURE+"KO"+SETCOLOR_NORMAL+"]";

    // ========================================
}
