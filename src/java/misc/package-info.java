/**
 * Ordre de lecture des classes<ul>
 *  <li>Log</li>
 *  <li>Config</li>
 *  <li>Bundle</li>
 *  <li>Util</li>
 *  <li>StateNotifier</li>
 *  <li>ProgressState</li>

 *  <li>SpinnerSlider</li>
 *  <li>DatePanel</li>
 *  <li>HourPanel</li>
 *  <li>ImagePreview</li>
 *  <li>TitledDialog</li>
 *  <li>HtmlDialog</li>

 *  <li>ApplicationManager</li>
 *  <li>OwnFrame</li>
 *  <li>Controller</li>
 *  <li>HelpManager</li>
 *  <li>ToolBarManager</li>

 *  <li>Guide</li>

 *  <li>XML</li>

 *  <li>ColorCheckedLine</li>
 *  <li>CommandLineServer</li>
 *  <li>CommandLineWaiter</li>

 * </ul>
 * @author F. Merciol
 */
package misc;
