// ================================================================================
// Copyright (c) Francois Merciol 2002
// Project	: Classe
// Name		: DatePanel.java
// Language	: Java
// Type		: Source file for DatePanel class
// Author	: Francois Merciol
// Creation	: 02/09/2002
// ================================================================================
// History
// --------------------------------------------------------------------------------
// Author	: 
// Date		: 
// Reason	: 
// ================================================================================
// To be improved :
// ================================================================================
package misc;

import java.awt.Dimension;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import javax.swing.JTextField;

/**
   Champ de saisie d'une date au format dd/MM/yyyy.
   Autres fonctions :<ul>
   <li> (espace) : éfface le champs
   <li> '+' ou (fleche haut) : incrémente d'un jour
   <li> '-' ou (fleche bas) : décrémente d'un jour
   <li> (retoure chariot) ou (perte de "focus") : transforme une date sur deux chiffres en relatif à la date actuelle.
   </ul>
*/
@SuppressWarnings ("serial") public class DatePanel extends JTextField implements KeyListener, FocusListener {

    // ========================================
    /** Format d'une date en francais. */
    static public final SimpleDateFormat dateFormat = new SimpleDateFormat ("dd/MM/yyyy");
    /** Ensemble des caractères ayant une action sur ce champs.
	Il faudrais ajouter le retoure chariot et le caractère d'effacement. */
    static public final String acceptChar =  "0123456789/+- "+KeyEvent.VK_UP+KeyEvent.VK_DOWN;

    // ========================================
    /**
       Création d'une date en reservant une taille maximal puis mis à la date actuelle.
    */
    public DatePanel () {
	super ("31/12/9999");
	addKeyListener (this);
	addFocusListener (this);
	set (new Date ());
    }

    // ========================================
    // api
    // ========================================
    /**
       Interprétation d'une année sur 2 chiffres.
    */
    public void enter () {
	tune2ky ();
    }

    // ========================================
    /**
       Fixe une date.
       @param date la nouvelle valeur.
    */
    public void set (Date date) {
	setText (dateFormat.format (date));
    }

    // ========================================
    // Listener
    // ========================================
    /** Interface KeyListener. */
    public void keyPressed (KeyEvent e) {
    }

    // ========================================
    /** Interface KeyListener. */
    public void keyReleased (KeyEvent e) {
	int c = e.getKeyCode ();
	switch (c) {
	case KeyEvent.VK_UP:
	    incr (1);
	    e.consume ();
	    return;
	case KeyEvent.VK_DOWN:
	    incr (-1);
	    e.consume ();
	    return;
	}
    }

    // ========================================
    /** Interface KeyListener. */
    public void keyTyped (KeyEvent e) {
	char c = e.getKeyChar ();

	switch (c) {
	case KeyEvent.VK_ENTER:
	    enter ();
	    return;
	case ';':
	    // le ; sera s�parateur
	    e.consume ();
	    return;
	case KeyEvent.VK_BACK_SPACE:
	    // on laisse java faire
	    return;
	case KeyEvent.VK_SPACE:
	    setText ("");
	    e.consume ();
	    return;
	case '+':
	    incr (1);
	    e.consume ();
	    return;
	case '-':
	    incr (-1);
	    e.consume ();
	    return;
	}

	if (acceptChar.indexOf (c) < 0) {
	    e.consume ();
	    return;
	}

	if (getSelectedText () == null && getText ().length () >= 12) {
	    e.consume ();
	    return;
	}
    }

    // ========================================
    /** Interface FocusListener. */
    public void focusGained (FocusEvent e) {
    }

    // ========================================
    /** Interface FocusListener. */
    public void focusLost (FocusEvent e) {
	tune2ky ();
    }

    // ========================================
    // Listener spécial
    // ========================================
    /**
       Interprétation d'une année sur 2 chiffre.
    */
    public void tune2ky () {
	try {
	    Date origDate = dateFormat.parse (getText ().trim ());
	    GregorianCalendar cal = new GregorianCalendar ();
	    cal.setTime (origDate);
	    int y = cal.get (Calendar.YEAR);
	    if (y < 100) {
		cal.add (Calendar.YEAR, cur2ky);
		if (y < min2ky)
		    cal.add (Calendar.YEAR, 100);
		else if (y > max2ky)
		    cal.add (Calendar.YEAR, -100);
	    }
	    setText (dateFormat.format (cal.getTime ()));
	    getSize (tmpSize);
	    if (tmpSize.width != 0)
		size = tmpSize;
	} catch (Exception ex) {
	}
    }

    // ========================================
    /** Taille actuelle du champ. */
    private Dimension tmpSize = new Dimension ();
    /** Derniére taille maximum utilisée. */
    private Dimension size;

    // ========================================
    /**
       @return la taille maximum déjà utilisé.
    */
    public Dimension getPreferredSize () {
	if (size != null)
	    return size;
	return super.getPreferredSize ();
    }

    // ========================================
    /**
       @return la taille maximum déjà utilisé.
    */
    public Dimension getMinimumSize () {
	if (size != null)
	    return size;
	return super.getPreferredSize ();
    }

    // ========================================
    /**
       Change la date d'une certain nombre de jours.
       @param incr le nombre de jour à avancer (diminuer si négatif).
    */
    public void incr (int incr) {
	try {
	    tune2ky ();
	    Date origDate = dateFormat.parse (getText ().trim ());
	    GregorianCalendar cal = new GregorianCalendar ();
	    cal.setTime (origDate);
	    cal.add (Calendar.DATE, incr);
	    setText (dateFormat.format (cal.getTime ()));
	} catch (Exception ex) {
	}
    }

    // ========================================
    // valeurs d'une année sur 2 chiffres
    // ========================================
    /** XXX commentaire sur l'optimisation d'une année sur 2 chiffre. */
    static public int min2ky, max2ky, cur2ky;

    /** Initialisation des champs. */
    static {
	GregorianCalendar cal = new GregorianCalendar ();
	cal.setTime (new Date ());
	int y = cal.get(Calendar.YEAR);
	int yMod1ky = y % 100;
	min2ky = yMod1ky - 50;
	max2ky = yMod1ky + 50;
	cur2ky = y - yMod1ky;
    }

    // ========================================
}
