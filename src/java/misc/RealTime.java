package misc;

@SuppressWarnings ("serial")
class NewChalenger extends RuntimeException {
    public NewChalenger () {
	super ("NewChalenger");
    }
}

public class RealTime {
    // ========================================
    private int nbChalengers;
    private Thread leadership;

    public void start (Runnable task) {
	(new Thread () {
		public void run () {
		    if (!getLeadership ())
			return;
		    try {
			task.run ();
		    } catch (NewChalenger e) {
		    } finally {
			loseLeaderShip ();
		    }
		}
	    }).start ();
    }

    // ========================================
    public synchronized void waitTerminaison () {
	if (leadership == Thread.currentThread ())
	    return;
	while (leadership != null && nbChalengers > 0)
	    try {
		wait ();
	    } catch (InterruptedException e) {
	    }
    }

    // ========================================
    public synchronized boolean getLeadership () {
	if (leadership != null) {
	    nbChalengers++;
	    try {
		wait ();
	    } catch (InterruptedException e) {
	    }
	    nbChalengers--;
	    if (nbChalengers != 0) {
		notifyAll ();
		return false;
	    }
	}
	leadership = Thread.currentThread ();
	return true;
    }
    public synchronized void loseLeaderShip () {
	leadership = null;
	notifyAll ();
    }
    public synchronized void checkChalengers () {
	if (nbChalengers != 0)
	    throw new NewChalenger ();
    }

    // ========================================
    static public void checkChalengers (RealTime realTime) {
	if (realTime != null)
	    realTime.checkChalengers ();
    }
    // ========================================
}
