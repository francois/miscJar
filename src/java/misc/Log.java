package misc;

import java.awt.Component;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;
import java.util.Vector;
import javax.swing.AbstractButton;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

public class Log {

    // ========================================
    static private final SimpleDateFormat dateFormat = new SimpleDateFormat ("[yyyy/MM/dd HH:mm:ss] ");
    // static private final String separator = System.getProperty ("file.separator");
    // static public final String dumpExtension = "log";
    static public final boolean debug = true;

    // ========================================
    static public void writeLog (String serviceName, String message) {
	try {
	    BufferedWriter out = new BufferedWriter
		(new FileWriter (Config.getString ("logPath", Config.logSystemDir)+Config.FS+serviceName+Config.logExt, true));
	    out.write (dateFormat.format (new Date ())+message+"\n");
	    out.flush ();
	    out.close ();
	} catch (IOException e) {
	}
    }

    // ========================================
    static public Hashtable<String, String> lastExceptions = new Hashtable<String, String> ();
    static public boolean keepLastException;

    // ========================================
    static public void keepLastException (String where, Throwable t) {
	try {
	    if (debug)
		t.printStackTrace ();
	    ByteArrayOutputStream baos = new ByteArrayOutputStream ();
	    PrintWriter printWriter = new PrintWriter (baos);
	    printWriter.println ("Exception:\nDate: "+dateFormat.format (new Date ())+"\n");
	    t.printStackTrace (printWriter);
	    printWriter.flush ();
	    printWriter.close ();
	    lastExceptions.put (where, baos.toString ("ISO-8859-1"));
	    System.err.println ("Exception "+t+" find in "+where+"\nSee log file.");
	    keepLastException = true;
	    for (AbstractButton dumpCommand : dumpCommands)
		dumpCommand.setEnabled (keepLastException);
	} catch (Exception e2) {
	}
    }

    // ========================================
    static public void dumpLastException (File file) {
	PrintWriter printWriter = null;
	try {
	    printWriter = new PrintWriter (new FileWriter (file));
	    for (String where : lastExceptions.keySet ())
		printWriter.println ("\n\nContext: "+where+"\n"+lastExceptions.get (where));
	    printWriter.flush ();
	} catch (Exception e) {
	} finally {
	    try {
		printWriter.close ();
	    } catch (Exception e) {
	    }
	}
    }

    // ========================================
    static public void dumpSaveDialog (Component component) {
	JFileChooser dumpChooser =
	    new JFileChooser (Config.getString ("dumpDir", Config.logSystemDir));
	dumpChooser.setFileFilter (new FileNameExtensionFilter (Bundle.getLabel ("DumpFilter"), Config.logExt));
	dumpChooser.setDialogTitle (Bundle.getTitle ("Dump"));
	if (dumpChooser.showSaveDialog (component) != JFileChooser.APPROVE_OPTION)
	    return;
	dumpLastException (dumpChooser.getSelectedFile ());
    }

    // ========================================
    static private Vector<AbstractButton> dumpCommands = new Vector<AbstractButton> ();
    static public void addDumpCommand (AbstractButton dumpCommand) {
	if (dumpCommand == null)
	    return;
	dumpCommands.add (dumpCommand);
	dumpCommand.setEnabled (keepLastException);
    }
    static public void removeDumpCommand (AbstractButton dumpCommand) {
	dumpCommands.remove (dumpCommand);
    }
    // ========================================
}
