package misc;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;
import javax.swing.AbstractButton;
import javax.swing.ButtonGroup;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import static misc.Util.GBCNL;

@SuppressWarnings ("serial")
public class ProxyManager implements ApplicationManager, ActionListener {
    static public final String
	actionSetProxy					= "SetProxy",
	actionNoProxy					= "NoProxy",
	actionSystemConfigProxy				= "SystemConfigProxy",
	actionManualConfigProxy				= "ManualConfigProxy";

    static public final List<String> radioButtonNames	= Arrays.asList (actionNoProxy, actionSystemConfigProxy, actionManualConfigProxy);
    static public final List<String> actionsNames	= Arrays.asList (actionSetProxy);
    @SuppressWarnings("unchecked")
    static public final Hashtable<String, Method> actionsMethod =
	Util.collectMethod (ProxyManager.class, actionsNames, radioButtonNames);
    public void  actionPerformed (ActionEvent e) {
	Util.actionPerformed (actionsMethod, e, this);
    }
    // ========================================
    public void addMenuItem (JMenu... jMenu) {
	Util.addMenuItem (actionsNames, this, jMenu[0]);
    }
    public void addIconButtons (Container... containers) {
	Util.addIconButton (actionsNames, this, containers[0]);
    }
    public void addActiveButtons (Hashtable<String, AbstractButton> buttons) {
	// disable if no network ?
    }
    JPanel manualPanel;
    private String getConfig () {
	if ("true".equals (System.getProperty ("java.net.useSystemProxies")))
	    return actionSystemConfigProxy;
	String host = System.getProperty ("http.proxyHost");
	String port = System.getProperty ("http.proxyPort");
	if (host == null || port == null || host.isEmpty () || port.isEmpty ())
	    return actionNoProxy;
	return actionManualConfigProxy;
    }
    public void actionSetProxy () {
	JFrame jFrame = new JFrame ();
	jFrame.setIconImage (controller.getIcon ());
	JPanel msgPanel = Util.getGridBagPanel ();
	String proxyConfig = getConfig ();
	ButtonGroup group = new ButtonGroup ();
	Util.addRadioButton (actionNoProxy, this, group, proxyConfig, msgPanel, GBCNL);
	Util.addRadioButton (actionSystemConfigProxy, this, group, proxyConfig, msgPanel, GBCNL);
	Util.addRadioButton (actionManualConfigProxy, this, group, proxyConfig, msgPanel, GBCNL);
	manualPanel = Util.getGridBagPanel ();
	JTextField hostTF = new JTextField (Config.getString ("ProxyHostName", "squid"), 18);
	JTextField portTF = new JTextField (Config.getString ("ProxyPort", "3128"), 6);
	// System.setProperty("http.proxyUser", "user");
	// System.setProperty("http.proxyPassword", "password");
	Util.addLabelFields (manualPanel, "Host", hostTF);
	Util.addLabelFields (manualPanel, "Port", portTF);
	Util.addComponent (manualPanel, msgPanel, GBCNL);
	Util.setEnabled (manualPanel, proxyConfig.equals (actionManualConfigProxy));
	if (JOptionPane.showConfirmDialog (jFrame, msgPanel, misc.Bundle.getTitle ("Proxy"), JOptionPane.YES_NO_OPTION)
	    != JOptionPane.YES_OPTION)
	    return;
	Config.setString ("ProxyHostName", hostTF.getText ());
	Config.setString ("ProxyPort", portTF.getText ());
	proxyConfig = group.getSelection ().getActionCommand ();
	if (actionSystemConfigProxy.equals (proxyConfig)) {
	    System.setProperty ("java.net.useSystemProxies", "true");
	    System.setProperty ("http.proxyHost", "");
	} else {
	    System.setProperty ("java.net.useSystemProxies", "false");
	    if (actionNoProxy.equals (proxyConfig))
		System.setProperty ("http.proxyHost", "");
	    else if (actionManualConfigProxy.equals (proxyConfig)) {
		System.setProperty ("http.proxyHost", hostTF.getText ());
		System.setProperty ("http.proxyPort", portTF.getText ());
	    }
	}
	java.net.ProxySelector.setDefault (java.net.ProxySelector.getDefault ());
    }
    public void actionNoProxy () {
	Util.setEnabled (manualPanel, false);
    }
    public void actionSystemConfigProxy () {
	Util.setEnabled (manualPanel, false);
    }
    public void actionManualConfigProxy () {
	Util.setEnabled (manualPanel, true);
    }

    // ========================================
    private OwnFrame controller;
    public ProxyManager (OwnFrame controller) {
	this.controller = controller;
    }
    // ========================================
}
