package misc;

import java.awt.Frame;
import java.awt.Point;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.SwingConstants;

@SuppressWarnings ("serial") public class TitledDialog extends JDialog implements SwingConstants {
    // ========================================
    protected String titleId;

    // ========================================
    public TitledDialog (Frame frame, final String titleId) {
	super (frame, Bundle.getTitle (titleId), false);
	this.titleId = titleId;
	Bundle.addLocalizedDialog (this, titleId);
	addWindowListener (new WindowAdapter () {
		public void windowClosing (WindowEvent e) {
		    Util.updateCheckBox (titleId, false);
		}
	    });
	boolean visible = Config.getBoolean (titleId+Config.checkedPostfix, false);
	setVisible (visible);
	//Util.updateCheckBox (titleId, visible);
    }

    // ========================================
    public void setVisible (boolean visible) {
	// 	if (visible == isVisible ())
	// 	    return;
	super.setVisible (visible);
	Util.updateCheckBox (titleId, visible);
	if (visible) {
	    pack ();
	    setLocationRelativeTo (null);
	    Config.loadLocation (titleId, this, getLocation ());
	    toFront ();
	} else {
	    saveLocation ();
	}
    }

    // ========================================
    public void setJFrame (JFrame jFrame) {
	setIconImage (jFrame.getIconImage ());
    }

    // ========================================
    public void saveLocation () {
	Config.saveLocation (titleId, this);
    }

    // ========================================
}
