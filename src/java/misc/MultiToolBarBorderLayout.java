package misc;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.LayoutManager2;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;
import java.util.TreeSet;
import javax.swing.SwingConstants;

public class MultiToolBarBorderLayout implements LayoutManager2, SwingConstants {
    static public final Hashtable<String, Integer> positionStringToInt;
    static public final Hashtable<Integer, String> positionIntToString;

    static {
	positionIntToString = new Hashtable<Integer, String> ();
	positionIntToString.put (CENTER, BorderLayout.CENTER);
	positionIntToString.put (NORTH, BorderLayout.NORTH);
	positionIntToString.put (EAST, BorderLayout.EAST);
	positionIntToString.put (SOUTH, BorderLayout.SOUTH);
	positionIntToString.put (WEST, BorderLayout.WEST);

	positionStringToInt = new Hashtable<String, Integer> ();
	for (int key : positionIntToString.keySet ())
	    positionStringToInt.put (positionIntToString.get (key), key);
    }

    // ========================================
    int hgap;
    int vgap;
    Container menu;
    Hashtable<Component, Integer> positions = new Hashtable<Component, Integer> ();
    Component center;

    public MultiToolBarBorderLayout () {
	this (0, 0);
    }
    public MultiToolBarBorderLayout (int hgap, int vgap) {
	this.hgap = hgap;
	this.vgap = vgap;
    }
    public int getHgap () {
	return hgap;
    }
    public void setHgap (int hgap) {
	this.hgap = hgap;
    }
    public int getVgap () {
	return vgap;
    }
    public void setVgap (int vgap) {
	this.vgap = vgap;
    }
    public void setMenu (Container menu) {
	this.menu = menu;
    }
    private ArrayList<Component> orderedComponents = new ArrayList<Component> ();
    public void setLayoutOrderedComponents (ArrayList<Component> orderedComponents) {
	if (orderedComponents== null)
	    return;
	this.orderedComponents = orderedComponents;
    }
    // ========================================
    public void addLayoutComponent (Component comp, Object constraints) {
	synchronized (comp.getTreeLock ()) {
	    if ((constraints == null) || (constraints instanceof String))
		addLayoutComponent ((String)constraints, comp);
	    else
		throw new IllegalArgumentException ("cannot add to layout: constraint must be a string (or null)");
	}
    }
    @Deprecated
    public void addLayoutComponent (String name, Component comp) {
	synchronized (comp.getTreeLock ()) {
	    if (name == null)
		name = BorderLayout.CENTER;
	    Integer position = positionStringToInt.get (name);
	    if (position == CENTER) {
		if (center != null)
		    // XXX ou erreur
		    positions.remove (comp);
		center = comp;
	    }
	    positions.put (comp, position);
	}
    }
    public void removeLayoutComponent (Component comp) {
	synchronized (comp.getTreeLock ()) {
	    positions.remove (comp);
	    if (center == comp)
		center = null;
	}
    }
    public Object getConstraints (Component comp) {
	try {
	    return positionIntToString.get (positions.get (comp));
	} catch (Exception e) {
	    return null;
	}
    }
    // ========================================
    public Dimension minimumLayoutSize (Container target) {
	return layoutSize (target, true);
    }
    public Dimension preferredLayoutSize (Container target) {
	return layoutSize (target, false);
    }
    public Dimension maximumLayoutSize (Container target) {
	return new Dimension (Integer.MAX_VALUE, Integer.MAX_VALUE);
    }
    Hashtable<Integer, ArrayList<Component>> getSortedComponents (Container target) {
	ArrayList<Component> allComponents = new ArrayList<Component> (orderedComponents);
	for (Component c : target.getComponents ())
	    if (!allComponents.contains (c))
		allComponents.add (c);
	Hashtable<Integer, ArrayList<Component>> sortedComponents = new Hashtable<Integer, ArrayList<Component>> ();
	for (int pos : positionIntToString.keySet ())
	    sortedComponents.put (pos, new ArrayList<Component> ());
	for (Component c : allComponents) {
	    Integer position = positions.get (c);
	    if (!c.isVisible () || position == null)
		continue;
	    sortedComponents.get (position).add (c);
	}
	return sortedComponents;
    }
    int maxSize (ArrayList<Component> components, boolean isMinimum, boolean isHorizontal) {
	int max = 0;
	int componentCount = components.size ();
	for (int i = 0; i < componentCount; i++) {
	    Component c = components.get (i);
	    Dimension d = isMinimum ? c.getMinimumSize () : c.getPreferredSize ();
	    max = isHorizontal ? Math.max (max, d.width) : Math.max (max, d.height);
	}
	return max;
    }
    TreeSet<Integer> getIntermediate (ArrayList<Component> components, boolean isMinimum, boolean isHorizontal, int minSize) {
	ArrayList<Integer> sizes = new ArrayList<Integer> ();
	int componentCount = components.size ();
	for (int i = 0; i < componentCount; i++) {
	    Component c = components.get (i);
	    Dimension d = isMinimum ? c.getMinimumSize () : c.getPreferredSize ();
	    sizes.add (isHorizontal ? d.width : d.height);
	}
	TreeSet<Integer> result = new TreeSet<Integer> ();
	int maxComp = componentCount-1;
	for (int i = 0; i < componentCount; i++) {
	    int size = 0;
	    for (int j = i; j < maxComp; j++) {
		size += sizes.get (j);
		if (size > minSize)
		    result.add (size);
	    }
	    maxComp = componentCount;
	}
	return result;
    }
    void swapAxes (Dimension d) {
	int tmp = d.width;
	d.width = d.height;
	d.height = tmp;
    }
    Dimension fill (ArrayList<Component> components, boolean isMinimum, boolean isHorizontal, int maxWidth) {
	int componentCount = components.size ();
	int x = 0, y = 0, maxX = 0, rowHeight = 0;
	for (int i = 0; i < componentCount; i++) {
	    Component c = components.get (i);
	    Dimension d = isMinimum ? c.getMinimumSize () : c.getPreferredSize ();
	    if (!isHorizontal)
		swapAxes (d);
	    if (x == 0 || x + d.width <= maxWidth) {
		if (x > 0)
		    x += isHorizontal ? hgap : vgap;
		x += d.width;
		rowHeight = Math.max (rowHeight, d.height);
		continue;
	    }
	    maxX = Math.max (maxX, x);
	    x = d.width;
	    y += (isHorizontal ? vgap : hgap) + rowHeight;
	    rowHeight = d.height;
	}
	Dimension result = new Dimension (Math.max (maxX, x), y+rowHeight);
	// !!! if !isHorizontal : width => main axes
	return result;
    }
    Dimension getExtra (Hashtable<Integer, ArrayList<Component>> sortedComponents,
			int northPos, int southPos, boolean isMinimum, boolean isHorizontal,
			int menuWidth, int extraCenter) {
	int minWidth = Util.max (maxSize (sortedComponents.get (CENTER), isMinimum, isHorizontal)+extraCenter,
				 menuWidth,
				 maxSize (sortedComponents.get (northPos), isMinimum, isHorizontal),
				 maxSize (sortedComponents.get (southPos), isMinimum, isHorizontal));
	TreeSet<Integer> sizes = new TreeSet<Integer> ();
	sizes.add (minWidth);
	sizes.addAll (getIntermediate (sortedComponents.get (northPos), isMinimum, isHorizontal, minWidth));
	sizes.addAll (getIntermediate (sortedComponents.get (southPos), isMinimum, isHorizontal, minWidth));
	int minSurface = Integer.MAX_VALUE;
	int extraHeight = 0;
	for (int size : sizes) {
	    Dimension northDim = fill (sortedComponents.get (northPos), isMinimum, isHorizontal, size);
	    Dimension southDim = fill (sortedComponents.get (southPos), isMinimum, isHorizontal, size);
	    int width = Util.max (size, northDim.width, southDim.width);
	    int height = northDim.height+southDim.height;
	    int surface = height*width;
	    if (surface < minSurface) {
		minSurface = surface;
		minWidth = width;
		extraHeight = height;
	    }
	}
	Dimension result = new Dimension (minWidth, extraHeight);
	if (!isHorizontal)
	    swapAxes (result);
	return result;
    }
    Dimension layoutSize (Container target, boolean isMinimum) {
	synchronized (target.getTreeLock ()) {
	    Hashtable<Integer, ArrayList<Component>> sortedComponents = getSortedComponents (target);
	    Dimension extraSide = getExtra (sortedComponents, WEST, EAST, isMinimum, false, 0, 0);
	    int menuWidth = menu == null ? 0 : (isMinimum ? menu.getMinimumSize () : menu.getPreferredSize ()).width;
	    Dimension extra = getExtra (sortedComponents, NORTH, SOUTH, isMinimum, true, menuWidth, extraSide.width);
	    Insets insets = target.getInsets ();
	    return new Dimension (extra.width+insets.left+insets.right,
				  extraSide.height+extra.height+insets.top+insets.bottom);
	}
    }
    public float getLayoutAlignmentX (Container parent) {
	return 0.5f;
    }
    public float getLayoutAlignmentY (Container parent) {
	return 0.5f;
    }
    // ========================================
    public void invalidateLayout (Container target) {
    }
    public void layoutContainer (Container target) {
	synchronized (target.getTreeLock ()) {
	    Hashtable<Integer, ArrayList<Component>> sortedComponents = getSortedComponents (target);
	    Insets insets = target.getInsets ();
	    int top = insets.top;
	    int bottom = target.getHeight () - insets.bottom;
	    int left = insets.left;
	    int right = target.getWidth () - insets.right;

            int maxWidth = right-left;
	    top += setComponents (target, sortedComponents.get (NORTH), insets.top, insets.left, true, maxWidth);
	    bottom -= setComponents (target, sortedComponents.get (SOUTH), insets.top, insets.left, true, maxWidth);
	    moveComponents (sortedComponents.get (SOUTH), 0, bottom-insets.top);

            int maxHeight = bottom-top;
	    left += setComponents (target, sortedComponents.get (WEST), top, insets.left, false, maxHeight);
	    right -= setComponents (target, sortedComponents.get (EAST), top, insets.left, false, maxHeight);
	    moveComponents (sortedComponents.get (EAST), right-insets.left, 0);

	    if (center != null && center.isVisible ())
		center.setBounds (left, top, right - left, bottom - top);
	    target.repaint ();
	}
    }
    private void moveComponents (ArrayList<Component> components, int dx, int dy) {
	int componentCount = components.size ();
	for (int i = 0; i < componentCount; i++) {
	    Component c = components.get (i);
	    c.setLocation (c.getX ()+dx, c.getY ()+dy);
	}
    }
    private int setComponents (Container target, ArrayList<Component> components, int top, int left, boolean isHorizontal, int maxWidth) {
	int x = 0, y = 0, rowHeight = 0, rowStart = 0;
	int componentCount = components.size ();
	for (int i = 0; i < componentCount; i++) {
	    Component c = components.get (i);
	    Dimension d = c.getPreferredSize ();
	    c.setSize (d.width, d.height);
	    if (!isHorizontal)
		swapAxes (d);
	    if (x == 0 || x + d.width <= maxWidth) {
		if (x > 0)
		    x += isHorizontal ? hgap : vgap;
		x += d.width;
		rowHeight = Math.max (rowHeight, d.height);
		continue;
	    }
	    setComponents (target, left, top, components, rowStart, i, isHorizontal, y, rowHeight);
	    x = d.width;
	    y += (isHorizontal ? vgap : hgap) + rowHeight;
	    rowHeight = d.height;
	    rowStart = i;
	}
	setComponents (target, left, top, components, rowStart, componentCount, isHorizontal, y, rowHeight);
	return y+rowHeight;
    }
    private void setComponents (Container target, int x, int y,
				ArrayList<Component> components, int rowStart, int rowEnd,
				boolean isHorizontal, int delta, int maxSize) {
        for (int i = rowStart; i < rowEnd; i++) {
            Component c = components.get (i);
	    if (isHorizontal) {
		int cy = y + delta + (maxSize - c.getHeight ())/2;
		c.setLocation (x, cy);
		x += c.getWidth () + hgap;
	    } else {
		int cx = x + delta + (maxSize - c.getWidth ())/2;
		c.setLocation (cx, y);
		y += c.getHeight () + vgap;
	    }
        }
    }
    // ========================================
    public String toString () {
	return getClass ().getName () + "[hgap=" + hgap + ",vgap=" + vgap + "]";
    }
}
