package misc;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Image;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;

import javax.swing.AbstractButton;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

public abstract class Controller<T> implements ApplicationManager, OwnFrame, ActionListener {

    // ========================================
    // remplace <jvmarg value="-Djava.net.preferIPv4Stack=true"/>
    // a placer avant la création de socket (y compris celle pour X11 faite par swing)
    static {
	java.util.Properties props = System.getProperties ();
	props.setProperty ("java.net.preferIPv4Stack", ""+true);
	System.setProperties (props);
    }

    // ========================================
    static public final List<String> actionsNames = Arrays.asList ("Quit");
    @SuppressWarnings("unchecked")
	static public final Hashtable<String, Method> actionsMethod = Util.collectMethod (Controller.class, actionsNames);

    public void  actionPerformed (ActionEvent e) {
	Util.actionPerformed (actionsMethod, e, this);
    }

    // ========================================
    protected JFrame jFrame;
    protected Component component;
    protected JMenuBar menuBar;
    public JFrame getJFrame () { return jFrame; }

    // ========================================
    public Controller (T t) {
	createModel (t);
	createAndShowFrame (createGUI (), createMenuBar ());
	Plugins.hideSate ();
    }

    // ========================================
    // XXX ATTENTION
    // createModel est appelé avant la création des attribut pas le constructer de "super"
    // les attrubuts ne doivent donc pas être initialisé durant leur déclaration
    protected abstract void createModel (T t);
    public abstract String getTitle ();
    public abstract Image getIcon ();

    public void updateBundle () {
	SwingUtilities.invokeLater (new Runnable () {
		public void run () {
		    jFrame.setTitle (getTitle ());
		}
	    });
    }

    // ========================================
    public void reborn () {
	// XXX on pert les boites de dialog attachées à l'ancienne fenêtre
	jFrame.dispose ();
	jFrame.setJMenuBar (null);
	jFrame.getContentPane ().remove (component);
	createAndShowFrame (component, menuBar);
    }

    // ========================================
    protected void createAndShowFrame (final Component component, final JMenuBar menuBar) {
	final JFrame newJFrame = new JFrame (getTitle ());
	newJFrame.setIconImage (getIcon ());
	newJFrame.setJMenuBar (menuBar);
	newJFrame.getContentPane ().add (component, BorderLayout.CENTER);
	newJFrame.pack ();
	newJFrame.addWindowListener (new WindowAdapter () {
		public void windowClosing (WindowEvent e) {
		    Config.saveLocation ("Frame", jFrame);
		    if (tryClosingWindows ())
			quit ();
		    else
			reborn ();
		}
	    });
	if (jFrame == null) {
	    newJFrame.setLocationRelativeTo (null);
	    Config.loadLocation ("Frame", newJFrame, newJFrame.getLocation ());
	} else
	    newJFrame.setLocation (jFrame.getLocation ());
	jFrame = newJFrame;
	this.component = component;
	this.menuBar = menuBar;
	jFrame.setVisible (true);
	newJFrame ();
    }

    // ========================================
    protected void newJFrame () { }

    // ========================================
    protected Component createGUI () {
	Bundle.addBundleObserver (this);
	return new JLabel ("Empty controller");
    }

    // ========================================
    protected JMenuBar createMenuBar () {
	return null;
    }

    // ========================================
    public void quit () {
	System.exit (0);
    }

    // ========================================
    public void actionQuit () {
	Config.saveLocation ("Frame", jFrame);
	if (tryClosingWindows ())
	    quit ();
    }

    // ========================================
    protected boolean tryClosingWindows () {
	switch (JOptionPane.showConfirmDialog (jFrame, "Do you want to quit ?", "Quit",
					       JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE)) {
	case JOptionPane.YES_OPTION:
	    return true;
	case JOptionPane.NO_OPTION:
	case JOptionPane.CLOSED_OPTION:
	    return false;
	}
	return true;
    }

    // ========================================
    public void addMenuItem (JMenu... jMenu) {
	Util.addMenuItem (actionsNames, this, jMenu[0]);
    }

    // ========================================
    public void addIconButtons (Container... containers) {
	Util.addIconButton (actionsNames, this, containers[0]);
    }

    // ========================================
    public void addActiveButtons (Hashtable<String, AbstractButton> buttons) {
    }

    // ========================================
    static public void main (String[] args) {
	Config.load ("Misc");
	Bundle.load ("Help");
	Bundle.load ("Controller");

	@SuppressWarnings ("serial")
	    class JControllerTestMenu extends JMenuBar {
		public JControllerTestMenu (ApplicationManager controllerManager, ApplicationManager helpManager) {
		    setLayout (new BoxLayout (this, BoxLayout.X_AXIS));
		    JMenu fileMenu = Util.addJMenu (this, "File");
		    add (Box.createHorizontalGlue ());
		    JMenu helpMenu = Util.addJMenu (this, "Help");
		    controllerManager.addMenuItem (fileMenu);
		    helpManager.addMenuItem (helpMenu);
		    Hashtable<String, AbstractButton> buttons = new Hashtable<String, AbstractButton> ();
		    Util.collectButtons (buttons, fileMenu);
		    Util.collectButtons (buttons, helpMenu);
		    controllerManager.addActiveButtons (buttons);
		    helpManager.addActiveButtons (buttons);
		}
	    };

	class ControllerTest extends Controller<String> {
	    public ControllerTest () { super (""); }
	    protected void createModel (String s) {}
	    public String getTitle () { return null; }
	    public Image getIcon () {
		return Util.loadImageIcon (Config.getString ("MiscIcon",
							     Config.imagesJarDir+"misc"+Config.imagesExt)).getImage ();
	    }
	    protected JMenuBar createMenuBar () {
		return new JControllerTestMenu (this, new HelpManager (this, "Misc"));
	    }
	    protected Component createGUI () {
		return new JLabel ("Affichage de la licence.");
	    }
	    protected boolean tryClosingWindows () {
		Config.save ("Misc");
		return true;
	    }
	};
	SwingUtilities.invokeLater (new Runnable () {
		public void run () {
		    new ControllerTest ();
		}
	    });
    }

    // ========================================
}
