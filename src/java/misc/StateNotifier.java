// ================================================================================
// François MERCIOL 2013
// Name		: StateNotifier.java
// Language	: Java
// Author	: François Merciol
// CopyLeft	: Cecil B
// Creation	: 2012
// Version	: 0.1 (xx/xx/xx)
// ================================================================================

package misc;

import java.lang.reflect.Method;
import java.util.Hashtable;
import java.util.List;
import java.util.TreeSet;

public class StateNotifier {

    // ========================================
    private Hashtable<Object, TreeSet<String>> observers = new Hashtable<Object, TreeSet<String>> ();
    private Hashtable<String, Hashtable<Object, Method>> observableNames= new Hashtable<String, Hashtable<Object, Method>> ();

    // ========================================
    private void broadcast (String observable) {
	Hashtable<Object, Method> objectMethod = observableNames.get (observable);
	if (objectMethod == null)
	    return;
	try {
	    for (Object object : objectMethod.keySet ())
		objectMethod.get (object).invoke (object);
	} catch (Exception e) {
	    Log.keepLastException ("ModelInfoSender::broadcast <"+observable+">", e);
	}
    }

    // ========================================
    private void broadcast (String observable, Object... args) {
	Hashtable<Object, Method> objectMethod = observableNames.get (observable);
	if (objectMethod == null)
	    return;
	try {
	    for (Object object : objectMethod.keySet ())
		objectMethod.get (object).invoke (object, new Object[]{args});
	} catch (Exception e) {
	    Log.keepLastException ("ModelInfoSender::broadcast <"+observable+">", e);
	}
    }

    // ========================================
    public void broadcastUpdate (String observable) {
	broadcast ("update"+observable);
    }
    public void broadcastDisplay (String observable, Object... args) {
	broadcast ("display"+observable, args);
    }

    // ========================================
    private void addObserver (Object object, boolean arg, String... observable) {
	TreeSet<String> newNames = new TreeSet<String> ();
	for (String obs : observable) {
	    String name = (arg ? "display" : "update")+obs;
	    try {
		Method method = arg ?
		    object.getClass ().getMethod (name, Object[].class):
		    object.getClass ().getMethod (name);
		Hashtable<Object, Method> objectMethod = observableNames.get (name);
		if (objectMethod == null)
		    observableNames.put (name, objectMethod = new Hashtable<Object, Method> ());
		objectMethod.put (object, method);
		newNames.add (name);
	    } catch (NoSuchMethodException e) {
		Log.keepLastException ("StateNotifier::addObserver no method <"+name+"> in class <"+object.getClass ()+">", e);
	    }
	}
	if (newNames.size () == 0)
	    return;
	try {
	    TreeSet<String> oldNames = observers.get (object);
	    oldNames.addAll (newNames);
	} catch (Exception e) {
	    observers.put (object, newNames);
	}
    }

    // ========================================
    public void addUpdateObserver (Object object, String... observable) {
	addObserver (object, false, observable);
    }
    public void addMsgObserver (Object object, String... observable) {
	addObserver (object, true, observable);
    }

    // ========================================
    public void removeObserver (Object object) {
	TreeSet<String> names = observers.get (object);
	if (names == null)
	    return;
	for (String name : names) {
	    Hashtable<Object, Method> objectMethod = observableNames.get (name);
	    objectMethod.remove (object);
	    if (objectMethod.size () == 0)
		observableNames.remove (name);
	}
	observers.remove (object);
    }

    // ========================================
}
