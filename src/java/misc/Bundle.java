package misc;

import java.awt.Component;
import java.awt.Dialog;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLConnection;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Hashtable;
import java.util.Locale;
import java.util.Properties;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;
import java.util.Vector;
import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.DefaultListCellRenderer;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.border.TitledBorder;

/**
   Managed the internalization by using files properties and ResourceBundle.
   This class could be used to managed all texts show in a application (buttons, menus, labels).
   The files properties could be embedded in a jar (mix of class and data) or a dedicated directory.
   The name file look like : applicationBundle[_langage[_COUNTRY[_VARIANT]]].properties .
   Each file contains a "bundle" of key/value association.
   The values are string represent message or formated sentences (with numbered holes to put variables values).
*/
public class Bundle {

    static public final String FS = System.getProperty ("file.separator");

    // = Constants ============================
    /** Bundle file extension. */
    static public final String bundleExt = ".properties";

    /** Postfix for title. */
    static public final String titlePostfix = "Title";

    /** Prefixe for all actioners (button, menus, menuItem) */
    static public final String actionPrefix = "Action";

    /** XXX commentaire */
    static public final String labelPrefix = "Label";
    static public final String storyPrefix = "Story";

    /** XXX commentaire */
    static public final String messagePrefix = "Message";

    /** XXX commentaire */
    static public final String exceptionPrefix = "Exception";

    /** XXX commentaire */
    static public final String userPrefix = "User_";

    /** XXX commentaire */
    static public final String enumPrefix = "Enum";

    /** Message shows when when try use bundle before load it. */
    static private final String bundleUseException = "Can''t find token \"{0}\" in Bundle. Continue?";
    static private final String bundleSaveException = "Bundle {0} can''t be save.";

    // = Attributs ============================
    static class ApplicationInfo {
	public ResourceBundle messages;
	public Hashtable<String, String> patch = new Hashtable<String, String> ();
	public boolean modified;
	public ApplicationInfo (ResourceBundle messages) { this.messages = messages; }
    }

    /** Labels to bundle to find to display.*/
    static private Hashtable<String, String> labels = new Hashtable<String, String> ();
    /** Applications bundle name to bundle. */
    static private Hashtable<String, ApplicationInfo> applications = new Hashtable<String, ApplicationInfo> ();
    static private ArrayList<String> applicationsOrder = new ArrayList<String> ();
    /** The current locale used to display messages. */
    static private Locale locale;


    // = Accessors ============================
    /**
       Accessors to locale.
       @return current locale.
    */
    static public Locale getLocale () { return locale; }
    /**
       Accesor to locale.
       @param locale the new locale value.
    */
    static public void setLocale (Locale locale) {
	resetLocale (locale);
	broadcastBundleReloaded ();
    }

    static private final void initLocale () {
	if (locale != null)
	    return;
	Locale locale = Locale.getDefault ();
	if (Config.getString ("Language") != null)
	    try {
		locale = new Locale (Config.getString ("Language"),
				     Config.getString ("Country"),
				     Config.getString ("Variant"));
	    } catch (Exception e) {
	    }
	resetLocale (locale);
    }

    static private void resetLocale (Locale locale) {
	if (Bundle.locale == locale ||
	    (Bundle.locale != null && Bundle.locale.equals (locale)))
	    return;
	Locale.setDefault (locale);
	Bundle.locale = locale;
	for (String application : applicationsOrder) {
	    ResourceBundle messages = ResourceBundle.getBundle (application, locale, bundleControl);
	    applications.put (application, new ApplicationInfo (messages));
	}
    }

    static ResourceBundle.Control bundleControl = new ResourceBundle.Control () {
    	    private String toResourceName0 (String bundleName, String suffix) {
    		if (bundleName.contains ("://"))
    		    return null;
    		else
    		    return toResourceName (bundleName, suffix);
    	    }
    	    public ResourceBundle newBundle (String baseName, Locale locale, String format, ClassLoader loader, boolean reload)
    		throws IllegalAccessException, InstantiationException, IOException {
    		String bundleName = toBundleName (baseName, locale);
    		ResourceBundle bundle = null;
                final String resourceName = toResourceName0 (bundleName, "properties");
                if (resourceName == null)
                    return bundle;
		URLConnection connection = Config.getDataUrl (Config.dataDirname, Config.configDirname, resourceName).openConnection ();
		connection.setUseCaches (false);
                InputStream stream = connection.getInputStream ();
		try {
		    bundle = new PropertyResourceBundle (stream);
		} finally {
		    stream.close ();
		}
    		return bundle;
    	    }
    	};

    // = Reading files ========================
    /**
       Set the new application and load bundle with parameter saved in configuration or the default system locale.
       @param application the application bundle name.
    */
    static public final void load (String application) {
	initLocale ();
	load (application, locale);
    }

    /**
       Set the new application and load bundle according the locale in parameter.
       @param application the application bundle name.
       @param locale the localization to used.
    */
    static public final void load (String application, Locale locale) {
	try {
	    resetLocale (locale);
	    if (applicationsOrder.contains (application))
		return;
	    applicationsOrder.add (application);
	    ResourceBundle messages = ResourceBundle.getBundle (application, locale, bundleControl);
	    // XXX anciennes valeurs de patch perdues
	    applications.put (application, new ApplicationInfo (messages));
	    for (String key : messages.keySet ())
		labels.put (key, application);
	    boolean needSetLanguage = Config.getString ("Language") == null;
	    broadcastBundleReloaded ();
	    if (needSetLanguage)
		HelpManager.actionLocale (null);
	} catch (RuntimeException e) {
	    if (JOptionPane.YES_OPTION != JOptionPane.showConfirmDialog
		(null, "Would you like to continue?",
		 " Can't load Bundle "+application, JOptionPane.YES_NO_OPTION))
		throw e;
	}
    }

    static public final void save (String applicationName) {
	File configDir = new File (Config.findDataDir (true), Config.configDirname);
	File bundleFile = new File (configDir, applicationName+"_"+locale+bundleExt);
	try {
	    save (applicationName, bundleFile);
	} catch (IOException e) {
	    try {
		configDir.mkdirs ();
		save (applicationName, bundleFile);
	    } catch (IOException e2) {
		System.err.println (MessageFormat.format (bundleSaveException, applicationName));
	    }
	} catch (NullPointerException e) {
	    throw new IllegalArgumentException (bundleUseException);
	}
    }
    static private boolean configurationModified = false;
    static public final void save (String applicationName, File file)
	throws IOException {
	ApplicationInfo applicationInfo = applications.get (applicationName);
	if (!applicationInfo.modified && file.exists ())
	    return;
	if (!file.exists ())
	    file.createNewFile ();
	Properties properties = new Properties ();
	ResourceBundle messages = applicationInfo.messages;
	for (String key : messages.keySet ())
	    properties.setProperty (key, messages.getString (key));
	Hashtable<String, String> patch = applicationInfo.patch;
	for (String key : patch.keySet ())
	    properties.setProperty (key, patch.get (key));
	FileOutputStream fileOutputStream = new FileOutputStream (file);
	properties.store (fileOutputStream, "Produit automatiquement par Bundle");
	fileOutputStream.close ();
	FileInputStream fileInputStream = new FileInputStream (file);
	applicationInfo.messages = new PropertyResourceBundle (fileInputStream);
	fileInputStream.close ();
	applicationInfo.patch.clear ();
	applicationInfo.modified = false;
    }
    static public void setString (String applicationName, String key, String val) {
	ApplicationInfo applicationInfo = applications.get (applicationName);
	String oldVal = null;
	try {
	    oldVal = applicationInfo.messages.getString (key);
	} catch (Exception e) {
	}
	if (val == oldVal || val.equals (oldVal)) {
	    applicationInfo.patch.remove (key);
	    return;
	}
	oldVal = applicationInfo.patch.get (key);
	if (val == oldVal || val.equals (oldVal))
	    return;
	applicationInfo.patch.put (key, val);
	applicationInfo.modified = true;
	labels.put (key, applicationName);
    }

    // = Available files ======================
    /**
       Give the list of locale available for a dedicated application.
       @param application the application bundle name.
       @return a array of string denoted locale that could be used for this application.
    */
    static public final Locale[] getApplicationsLocales () {
	Vector<Locale> result = new Vector<Locale> ();
	scanLocale: for (Locale locale : getAvailableLocales ()) {
	    for (String application : applications.keySet ())
		if (ClassLoader.getSystemResource (Config.configSystemDir+application+"_"+locale+bundleExt) == null &&
		    ClassLoader.getSystemResource (Config.configJarDir+application+"_"+locale+bundleExt) == null)
		    continue scanLocale;
	    result.add (locale);
	}
	Locale [] locales = result.toArray (new Locale[0]);
	Arrays.sort (locales, new Comparator<Locale> () {
		public int compare(Locale o1, Locale o2) {
		    return o1.toString ().compareTo (o2.toString ());
		}
	    });
	return locales;
    }

    // ========================================

    static public Locale[] getAvailableLocales () {
	ArrayList<Locale> all = new ArrayList<Locale> (Arrays.asList (Locale.getAvailableLocales ()));
	//all.add (new Locale.Builder ().setLanguage ("br").setRegion ("BR").setVariant ("gallo").build ());
	//all.add (new Locale.Builder ().setLanguage ("br").setRegion ("BR").setVariant ("breton").build ());
	all.add (new Locale.Builder ().setLanguage ("br").setRegion ("FR").setVariant ("gallo").build ());
	all.add (new Locale.Builder ().setLanguage ("br").setRegion ("FR").setVariant ("breton").build ());
	//all.add (new Locale.Builder ().setLanguage ("es").setRegion ("ES").build ());
	return all.toArray (new Locale[0]);
    }
    static public final Locale[] getAllLocales () {
	Locale[] locales = getAvailableLocales ();
	Arrays.sort (locales, new Comparator<Locale> () {
		public int compare(Locale o1, Locale o2) {
		    return o1.toString ().compareTo (o2.toString ());
		}
	    });
	return locales;
    }

    static public ImageIcon getMixFlag (String language, String country) {
	ImageIcon countryFlag = Util.loadImageIcon (Config.dataDirname, Config.iconsDirname, Config.flagsDirname,
						    country.toLowerCase () + Config.iconsExt);
	ImageIcon langFlag = Util.loadImageIcon (Config.dataDirname, Config.iconsDirname, Config.flagsDirname,
						 language.toLowerCase () + Config.iconsExt);
	if (countryFlag == null)
	    return langFlag;
	if (langFlag == null)
	    return countryFlag;
	int width = countryFlag.getIconWidth ();
	int height = countryFlag.getIconHeight ();
	BufferedImage newFlag = new BufferedImage (width, height, BufferedImage.TYPE_INT_ARGB);
	Graphics2D g2 = newFlag.createGraphics ();
	Polygon topLeftCorner = new Polygon (new int[] {0, width, 0}, new int[] {0, 0, height}, 3);
	g2.setClip (topLeftCorner);
	g2.drawImage (langFlag.getImage (), 0, 0, width, height, null);
	Polygon bottomRightCorner = new Polygon (new int[] {0, width, width}, new int[] {height, 0, height}, 3);
	g2.setClip (bottomRightCorner);
	g2.drawImage (countryFlag.getImage (), 0, 0, width, height, null);
	return new ImageIcon (newFlag);
    }

    // ========================================
    static public final JComboBox<String> getJComboFlags (final Locale[] locales) {
	final ImageIcon [] flags = new ImageIcon [locales.length];
	String [] labels = new String [locales.length];
	final Hashtable<String,Integer> labelIndex = new Hashtable<String,Integer> ();
	for (int i = 0; i < locales.length; i++) {
	    labelIndex.put (labels [i] = locales[i].toString (), i);
	    flags [i] = getMixFlag (locales[i].getLanguage (), locales[i].getCountry ());
	    if (flags [i] != null)
		flags [i].setDescription (locales [i].toString ());
	}
	JComboBox<String> localesList = new JComboBox<String> (labels);
	localesList.setRenderer (new DefaultListCellRenderer () {
		{
		    setOpaque (true);
		    setVerticalAlignment (CENTER);
		}
		static private final long serialVersionUID = 1L;
		public Component getListCellRendererComponent (JList<?> list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
		    int selectedIndex = labelIndex.get (value);

		    if (isSelected) {
			setBackground (list.getSelectionBackground ());
			setForeground (list.getSelectionForeground ());
		    } else {
			setBackground (list.getBackground ());
			setForeground (list.getForeground ());
		    }
		    setIcon (flags[selectedIndex]);
		    setText (locales[selectedIndex].toString ());
		    return this;
		}
	    });
	return localesList;
    }

    // = Use localized texts ==================
    /**
       Give the string to display according to the message identifier.
       @param messageId a sting used to tag message.
       @return the string to display.
    */
    static public boolean askBundleUseException = true;
    static public final String getString (String messageId, String defaultValue) {
	try {
	    ApplicationInfo applicationInfo = applications.get (labels.get (messageId));
	    String val = applicationInfo.patch.get (messageId);
	    if (val != null)
		return val;
	    return applicationInfo.messages.getString (messageId);
	} catch (Exception e) {
	    if (defaultValue != null)
		return defaultValue;
	    if (askBundleUseException && JOptionPane.YES_OPTION !=
		JOptionPane.showConfirmDialog (null, MessageFormat.format (bundleUseException, messageId),
					       " Bundle corrupted ", JOptionPane.YES_NO_OPTION))
		throw new IllegalArgumentException (MessageFormat.format (bundleUseException, messageId));
	    askBundleUseException = false;
	    System.err.println (messageId+"= ### Bundle ###");
	    return messageId;
	}
    }
    static public final String getStory (String messageId) { return getString (storyPrefix+messageId, messageId); }
    static public final String getLabel (String messageId) { return messageId == null ? "" : getString (labelPrefix+messageId, null); }
    static public final String getMessage (String messageId) { return getString (messagePrefix+messageId, null); }
    static public final String getException (String messageId) { return getString (exceptionPrefix+messageId, null); }
    static public final String getAction (String messageId) { return getString (actionPrefix+messageId, null); }
    static public final String getTitle (String messageId) { return getString (messageId+titlePostfix, null); }
    static public final String getUser (String messageId) { return getString (userPrefix+messageId, messageId); }
    static public final void setUser (String applicationName, String messageId, String value) {
	setString (applicationName, userPrefix+messageId, value);
    }
    static public final String getEnum (String enumId, String valueId) { return getString (enumPrefix+enumId+valueId, null); }
    static public final String getEnum (Enum<?> enumValue) { return getEnum (enumValue.getClass ().getSimpleName (), enumValue.toString ()); }
    static public final JComboBox<String> getEnum (Class<?> enumClass, Enum<?> defaultValue) {
	String enumName = enumClass.getSimpleName ();
	if (!enumClass.isEnum ())
	    throw new IllegalArgumentException (enumName+" is not Enum!");
	JComboBox<String> jComboBox	= new JComboBox<String> ();
	try {
	    Object[] values = (Object[]) enumClass.getMethod ("values").invoke (null);
	    for (Object value : values)
		jComboBox.addItem (getEnum (enumName, value.toString ()));
	    jComboBox.setEditable (false);
	    if (defaultValue != null)
		jComboBox.setSelectedIndex (defaultValue.ordinal ());
	} catch (Exception e) {
	    throw new IllegalArgumentException (enumName+" is not Enum!");
	}
	return jComboBox;
    }

    // = Automatic update =====================

    static public void setBorder (JComponent jComponent) {
	jComponent.setBorder (BorderFactory.createTitledBorder (Bundle.getTitle (Util.getClassName (jComponent.getClass ()))));
    }
    static public void updateBorder (JComponent jComponent) {
	((TitledBorder) jComponent.getBorder ()).setTitle (Bundle.getTitle (Util.getClassName (jComponent.getClass ())));
    }

    /** List of actioners (button, ...) to update if the locale is updated. */
    static private Vector<AbstractButton> allActioners = new Vector<AbstractButton> ();

    /** List of actioners (menu, ...) to update if the locale is updated. */
    static private Vector<AbstractButton> allMenus = new Vector<AbstractButton> ();

    /** List of frame to update if the locale is updated. */
    static private Hashtable<Dialog, String> allDialogs = new Hashtable<Dialog, String> ();

    /** List of icon ToolTips to update if the locale is updated. */
    static private Vector<AbstractButton> allIconToolTips = new Vector<AbstractButton> ();

    /** List of application module which managed localized texts to update if the locale is updated. */
    static private Hashtable<JLabel, String> allLabels = new Hashtable<JLabel, String> ();

    /** XXX commentaire. */
    static private Hashtable<JComboBox<String>, Class<?>> allEnums = new Hashtable<JComboBox<String>, Class<?>> ();

    /** XXX commentaire. */
    static private StateNotifier bundleObservable = new StateNotifier ();

    /**
       Say to each actioner and application module that locale change.
    */
    static void broadcastBundleReloaded () {
	if (Config.isLoaded ()) {
	    Config.setString ("Country", locale.getCountry ());
	    Config.setString ("Language", locale.getLanguage ());
	    Config.setString ("Variant", locale.getVariant ());
	}
	for (AbstractButton actioner : allActioners)
	    if (!actioner.getText ().isEmpty ())
		actioner.setText (getAction (actioner.getActionCommand ()));
	for (AbstractButton menu : allMenus)
	    if (!menu.getText ().isEmpty ())
		menu.setText (getTitle (menu.getActionCommand ()));
	for (Dialog dialog : allDialogs.keySet ())
	    dialog.setTitle (getTitle (allDialogs.get (dialog)));
	for (AbstractButton actioner : allIconToolTips)
	    actioner.setToolTipText (Bundle.getAction (actioner.getActionCommand ()));
	for (JLabel jLabel : allLabels.keySet ())
	    jLabel.setText (getLabel (allLabels.get (jLabel)));
	for (JComboBox<String> jComboBox : allEnums.keySet ())
	    try {
		Class<?> enumClass = allEnums.get (jComboBox);
		String enumName = enumClass.getSimpleName ();
		// XXX remove listeners
		// XXX sauve selection multiple ?
		int idx = jComboBox.getSelectedIndex ();
		jComboBox.removeAllItems ();
		Object[] values = (Object[]) enumClass.getMethod ("values").invoke (null);
		for (Object value : values)
		    jComboBox.addItem (getEnum (enumName, value.toString ()));
		jComboBox.setEditable (false);
		jComboBox.setSelectedIndex (idx);
		// XXX add listeners
	    } catch (Exception e) {
	    }
	bundleObservable.broadcastUpdate ("Bundle");
    }

    /**
       Declare an actioner to updated if the locale is updated.
       @param button the actioner to update. The messageId is find by performed getActionCommand method on it.
    */
    static public void addLocalizedActioner (AbstractButton button) {
	allActioners.add (button);
    }

    static public void addLocalizedMenu (AbstractButton button) {
	allMenus.add (button);
    }

    static public void addLocalizedDialog (Dialog dialog, String titleId) {
	allDialogs.put (dialog, titleId);
    }
    /**
       XXX commentaire
    */
    static public void addLocalizedToolTip (AbstractButton button) {
	allIconToolTips.add (button);
    }

    /**
       XXX commentaire
    */
    static public void addLocalizedLabel (JLabel jLabel, String messageId) {
	allLabels.put (jLabel, messageId);
    }

    /**
       XXX commentaire
    */
    static public void addLocalizedEnum (JComboBox<String> jComboBox, Class<?> enumClass) {
	allEnums.put (jComboBox, enumClass);
    }

    /**
       Declare an application module which managed localized to updated if the locale is updated.
       @param object the application module that managed some texts to localized.
    */
    static public void addBundleObserver (Object object) {
	bundleObservable.addUpdateObserver (object, "Bundle");
    }

    // ========================================
}
