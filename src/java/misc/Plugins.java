package misc;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Vector;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

public class Plugins {

    // ========================================
    static public URL getDataUrl (String name) {
	try {
	    File file = (new File (name));
	    if (file.exists ()) {
		URL result = file.toURI ().toURL ();
		return result;
	    }
	} catch (Exception e) {
	}
	try {
	    URL result = ClassLoader.getSystemResource (name);
	    return result;
	} catch (Exception e) {
	}
	return null;
    }
    // ========================================
    static public ImageIcon loadImageIcon (String name) {
	URL url = getDataUrl ("data/images/button/"+name+".png");
	return (url != null) ? new ImageIcon (url) : null;
    }
    // ========================================
    // Change Util.version too !
    static public final Long version = 20171101L;
    static public final String[] stateLabel = {"not found", "old version", "updated"};
    static public final ImageIcon[] stateIcon = { loadImageIcon ("Bad"), loadImageIcon ("Warning"), loadImageIcon ("Good")};

    static public class PluginInfo {
	public String name;
	public String url;
	public boolean mandatory, loaded, update;
	public PluginInfo (String name, String url, boolean mandatory) {
	    this.name = name;
	    this.url = url;
	    this.mandatory = mandatory;
	}
    };

    static ArrayList<PluginInfo> pluginsInfo = new ArrayList<PluginInfo> ();

    // ========================================
    @SuppressWarnings ("rawtypes")
    static public void check (String name, String url, String className, boolean mandatory, Long dateNeed) {
	PluginInfo pluginInfo = new PluginInfo (name, url, mandatory);
	pluginsInfo.add (pluginInfo);
	try {
	    Class plugin = ClassLoader.getSystemClassLoader ().loadClass (className);
	    pluginInfo.loaded = true;
	    pluginInfo.update =
		dateNeed == null ||
		dateNeed <= (Long) plugin.getDeclaredField ("version").get (null);
	} catch (Exception e) {
	}
    }

    // ========================================
    @SuppressWarnings ("serial")
    static public class ImageCellRenderer extends JLabel implements TableCellRenderer {
	public Component getTableCellRendererComponent (JTable jTable, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
	    if (value instanceof ImageIcon) {
		setIcon ((ImageIcon) value);
		setText (null);
		    
	    } else {
		setIcon (null);
		setText ((String) value);
	    }
	    return this;
	}
    }

    // ========================================
    static public JDialog jDialog;
    static public boolean warning = false;

    static public void showSate () {
	Vector<Vector<Object>> data = new Vector<Vector<Object>> ();
	boolean ready = true;
	for (PluginInfo pluginInfo : pluginsInfo) {
	    ready &= !pluginInfo.mandatory || pluginInfo.loaded;
	    int state = (pluginInfo.loaded ? (pluginInfo.update ? 2 : 1) : 0);
	    if (state != 2)
		warning = true;
	    data.add
		(new Vector<Object>
		 (Arrays.asList (pluginInfo.name, stateIcon[state], stateLabel[state], (pluginInfo.update ? "" : pluginInfo.url) )));
	}
	Vector<String> columnNames = new Vector<String> (Arrays.asList ("Plugin name", "", "State", "URL"));
	@SuppressWarnings ("serial")
	    JTable table = new JTable (data, columnNames) {
		    public Component prepareRenderer (final TableCellRenderer renderer, final int row, final int column) {
			final Component prepareRenderer = super.prepareRenderer (renderer, row, column);
			final TableColumn tableColumn = getColumnModel ().getColumn (column);
			int width = Math.max (prepareRenderer.getPreferredSize ().width, tableColumn.getPreferredWidth ());
			tableColumn.setPreferredWidth (width);
			return prepareRenderer;
		    }
		};
	table.setDefaultRenderer (Object.class, new ImageCellRenderer ());

	final TableCellRenderer renderer = table.getTableHeader ().getDefaultRenderer ();
	for (int i = 0; i < table.getColumnCount (); ++i) {
	    int width = renderer.getTableCellRendererComponent (table, table.getModel ().getColumnName (i), false, false, 0, i).getPreferredSize ().width;
	    TableColumn tableColumn = table.getColumnModel ().getColumn (i);
	    tableColumn.setPreferredWidth (width);
	}

	JPanel container = new JPanel ();
	container.setLayout (new BorderLayout());
	container.add (table.getTableHeader(), BorderLayout.PAGE_START);
	container.add (table, BorderLayout.CENTER);

	if (!ready) {
	    Dimension containerSize = container.getSize ();
	    containerSize.width = Math.max (containerSize.width, 600);
	    containerSize.height = Math.max (containerSize.height, 100);
	    container.setPreferredSize (containerSize);

	    container.add (new JLabel ("Would you like to continue?"), BorderLayout.PAGE_END);
	    if (JOptionPane.YES_OPTION !=
		JOptionPane.showConfirmDialog (null, container, " Can't load plugins ", JOptionPane.YES_NO_OPTION))
		System.exit (1);
	    return;
	}
	jDialog = new JDialog ((java.awt.Frame)null, " Plugins state ", false);
	jDialog.getContentPane ().add (container, BorderLayout.CENTER);
	jDialog.setVisible (true);
	jDialog.pack ();
	jDialog.setLocationRelativeTo (null);
	jDialog.toFront ();
    }

    static public void hideSate () {
	if (jDialog == null)
	    return;
	jDialog.pack ();
	//jDialog.toFront ();
	if (!warning)
	    jDialog.setVisible (false);
    }

    // ========================================
}
