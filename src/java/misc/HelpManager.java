package misc;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;
import java.util.Vector;
import javax.swing.AbstractButton;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import misc.Bundle;
import misc.Log;
import misc.Util;

/**
   Les drapeaux peuvent être trouvé sur http://flags.blogpotato.de/
*/
@SuppressWarnings ("serial") public class HelpManager implements ApplicationManager, ActionListener {

    // ========================================
    private Frame frame;
    private OwnFrame controller;
    private String applicationName;
    private JConsole jConsole;

    // ========================================
    static public final String
    //actionPackBug = "PackBug",
	actionForcePack = "ForcePack",
	actionBugReport = "BugReport",
	actionJConsole	= "JConsole";

    static public final List<String> actionsNames =
	Arrays.asList ("About", "Licence", "Locale");
    static public final List<String> actionsBugNames =
	Arrays.asList (/*actionPackBug, */actionForcePack, actionBugReport, actionJConsole);
    @SuppressWarnings("unchecked")
    static public final Hashtable<String, Method> actionsMethod =
	Util.collectMethod (HelpManager.class, actionsNames, actionsBugNames);

    public void actionPerformed (ActionEvent e) {
	Util.actionPerformed (actionsMethod, e, this);
    }

    // ========================================
    public HelpManager (OwnFrame controller, String applicationName) {
	this.controller = controller;
	this.applicationName = applicationName;
	frame = new Frame ();
	frame.setIconImage (controller.getIcon ());
	aboutDialog = new HtmlDialog (frame, "About", Config.textsJarDir+"About"+applicationName+Config.htmlExt);
	licenceDialog = new HtmlDialog (frame, "Licence", Config.textsJarDir+applicationName+"Licence"+Config.htmlExt);
	jConsole = new JConsole (frame);
	//Util.packBug = Config.getBoolean (actionPackBug+Config.checkedPostfix, false);
    }

    // ========================================
    private HtmlDialog aboutDialog;
    private HtmlDialog licenceDialog;

    // ========================================
    public void actionAbout () {
	aboutDialog.restart ();
	aboutDialog.setVisible (true);
    }

    // ========================================
    public void actionLicence () {
	licenceDialog.restart ();
	licenceDialog.setVisible (true);
    }

    // ========================================
    public void actionLocale () {
	actionLocale (controller);
    }

    static public void actionLocale (OwnFrame controller) {
	try {
	    Locale [] locales = Bundle.getApplicationsLocales ();
	    JComboBox<String> localesList = Bundle.getJComboFlags (locales);
	    Locale currentLocale = Bundle.getLocale ();
	    int currentIndex = 0;
	    if (currentLocale != null)
		for (int i = 0; i < locales.length; i++)
		    if (currentLocale.equals (locales[i])) {
			currentIndex = i;
			break;
		    }
	    localesList.setSelectedIndex (currentIndex);
	    JPanel jPanel = new JPanel (new BorderLayout ());
	    jPanel.add (new JLabel (Bundle.getMessage ("ChooseLocale")), BorderLayout.PAGE_START);
	    jPanel.add (localesList, BorderLayout.CENTER);

	    Frame frame = controller != null ? controller.getJFrame () : null;
	    Frame frmOpt = null;
	    if (frame == null) {
		frmOpt = new JFrame ();
		frmOpt.setVisible (true);
		frmOpt.setLocationRelativeTo (null);
		frmOpt.setAlwaysOnTop (true);
		frame = frmOpt;
	    }
	    try {
		if (JOptionPane.OK_OPTION !=
		    JOptionPane.showConfirmDialog (frame, jPanel, Bundle.getTitle ("ChangeLocale"),
						   JOptionPane.OK_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE))
		    return;
	    } finally {
		if (frmOpt != null)
		    frmOpt.dispose();
	    }
	    Bundle.setLocale (locales[localesList.getSelectedIndex ()]);
	    Util.packWindow (frame);
	} catch (Exception e) {
	    Log.keepLastException ("HelpManager::actionLocale", e);
	}
    }

    // // ========================================
    // public void actionPackBug () {
    // 		Util.packBug = ! Util.packBug;
    // 		Config.setBoolean (actionPackBug+Config.checkedPostfix, Util.packBug);
    // 		ImageIcon icon = Util.loadActionIcon (actionPackBug+(Util.packBug ? "On" : "Off"));
    // 		for (AbstractButton packBugCommand : packBugCommands) {
    // 				packBugCommand<.setSelected (Util.packBug);
    // 				// XXX bug Java
    // 				packBugCommand.setIcon (icon);
    // 		}
    // }

    // ========================================
    public void actionForcePack () {
	controller.reborn ();
    }

    // ========================================
    public void actionBugReport () {
	Log.dumpSaveDialog (controller.getJFrame ());
    }

    // ========================================
    public void actionJConsole (boolean checked) {
	jConsole.setVisible (checked);
	Util.updateCheckBox (actionJConsole, checked);
    }

    // ========================================
    // static private Vector<AbstractButton> packBugCommands = new Vector<AbstractButton> ();
    // static public void addPackBugCommand (AbstractButton packBugCommand) {
    // 		if (packBugCommand == null)
    // 				return;
    // 		packBugCommands.add (packBugCommand);
    // 		packBugCommand.setSelected (Util.packBug);
    // 		// XXX bug Java
    // 		packBugCommand.setIcon (Util.loadActionIcon (actionPackBug+(Util.packBug ? "On" : "Off")));
    // }
    // static public void removePackBugCommand (AbstractButton packBugCommand) {
    // 		packBugCommands.remove (packBugCommand);
    // }

    // ========================================
    public void addMenuItem (JMenu... jMenu) {
	Util.addMenuItem (actionsNames, this, jMenu[0]);
	//Util.addCheckMenuItem (actionPackBug, this, Util.packBug, jMenu[0]);
	Util.addMenuItem (actionForcePack, this, jMenu[0]);
	Util.addMenuItem (actionBugReport, this, jMenu[0]);
	Util.addCheckMenuItem (actionJConsole, this, jMenu[0]);
    }

    // ========================================
    public void addIconButtons (Container... containers) {
	Util.addIconButton (actionsNames, this, containers[0]);
	//Util.addIconButton (actionPackBug, this, containers[0]);
	Util.addIconButton (actionForcePack, this, containers[0]);
	Util.addIconButton (actionBugReport, this, containers[0]);
   }

    // ========================================
    public void addActiveButtons (Hashtable<String, AbstractButton> buttons) {
	//addPackBugCommand (buttons.get (actionPackBug));
	Log.addDumpCommand (buttons.get (actionBugReport));
    }
    // ========================================
}
