package misc;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Frame;
import java.io.IOException;
import java.net.URL;
import java.text.MessageFormat;
import javax.swing.BorderFactory;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import javax.swing.text.html.HTMLDocument;
import javax.swing.text.html.HTMLFrameHyperlinkEvent;

// XXX rechercher le fichier à afficher en fonction de la langue
// XXX mise à jour dynamique en cas de changement de langue

@SuppressWarnings ("serial") public class HtmlDialog extends TitledDialog {

    // ========================================
    public JEditorPane editorPane;

    protected URL startedURL;

    // ========================================
    public HtmlDialog (Frame frame, String titleName, String fileName) {
	super (frame, titleName);
	editorPane = new JEditorPane ();
	changeFileName (fileName);
	editorPane.setBackground (getBackground ());
	editorPane.setBorder (BorderFactory.createCompoundBorder (BorderFactory.createRaisedBevelBorder (),
								  BorderFactory.createEmptyBorder (2, 5, 2, 5)));
	editorPane.setEditable (false);
	editorPane.setCaretPosition (0);
	editorPane.scrollToReference ("Top");
	JScrollPane editorScrollPane = new JScrollPane (editorPane);
	editorScrollPane.setVerticalScrollBarPolicy (JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
	editorScrollPane.setPreferredSize (new Dimension (600, 300));
	getContentPane ().add (editorScrollPane, BorderLayout.CENTER);
    }

    // ========================================
    public void changeFileName (String fileName) {
	try {
	    URL newURL = Config.getDataUrl (fileName);
	    if (newURL.equals (startedURL))
		return;
	    startedURL = newURL;
	    restart ();
	} catch (Exception e) {
	}
    }

    // ========================================
    HyperlinkListener hyperlinkListener = new HyperlinkListener () {
	    public void hyperlinkUpdate (HyperlinkEvent e) {
		if (e.getEventType () == HyperlinkEvent.EventType.ACTIVATED) {
		    if (e instanceof HTMLFrameHyperlinkEvent) {
			((HTMLDocument) editorPane.getDocument ()).processHTMLFrameHyperlinkEvent ((HTMLFrameHyperlinkEvent) e);
		    } else {
			try {
			    editorPane.setPage (e.getURL ());
			} catch (Exception ioe) {
			    System.err.println ("IOE: " + ioe);
			}
		    }
		}
	    }
	};

	public void restart () {
	try {
	    editorPane.setPage (startedURL);
	    editorPane.setDragEnabled (true);
	    editorPane.removeHyperlinkListener (hyperlinkListener);
	    editorPane.addHyperlinkListener (hyperlinkListener);
	} catch (Exception e) {
	}
    }

    // ========================================
}
