// ================================================================================
// Copyright (c) Francois Merciol 2002
// Project	: Classe
// Name		: DatePanel.java
// Language	: Java
// Type		: Source file for DatePanel class
// Author	: Francois Merciol
// Creation	: 02/09/2002
// ================================================================================
// History
// --------------------------------------------------------------------------------
// Author	: 
// Date		: 
// Reason	: 
// ================================================================================
// To be improved :
// ================================================================================
package misc;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JProgressBar;

@SuppressWarnings ("serial") public class ProgressStatePanel extends JPanel implements ActionListener {

    // ========================================
    JProgressBar jProgressBar = new JProgressBar ();
    JButton stopButton = new JButton (Util.loadActionIcon ("Abort"));
    ProgressState progressState;

    // ========================================
    public ProgressStatePanel (ProgressState progressState) {
	this.progressState = progressState;
	jProgressBar.setMaximum (1);
	jProgressBar.setString ("");
	jProgressBar.setStringPainted (true);
	jProgressBar.setAlignmentX (Component.LEFT_ALIGNMENT);

	setLayout (new BorderLayout ());
	add (jProgressBar, BorderLayout.CENTER);
	stopButton.setActionCommand ("Abort");
	stopButton.addActionListener (this);
	stopButton.setEnabled (false);
	add (stopButton, BorderLayout.LINE_END);
    }

    public void  actionPerformed (ActionEvent e) {
	if (e.getSource () != stopButton)
	    return;
	progressState.abort ();
    }

    // ========================================
    public void updateProgress () {
	switch (progressState.state) {
	case Value:
	    jProgressBar.setValue (progressState.value);
	    break;
	case Init:
	    jProgressBar.setString (progressState.domain);
	    jProgressBar.setValue (progressState.value);
	    jProgressBar.setMaximum (progressState.maxValue);
	    stopButton.setEnabled (true);
	    break;
	case End:
	    jProgressBar.setString ("");
	    jProgressBar.setValue (0);
	    stopButton.setEnabled (false);
	    break;
	}
    }

    // ========================================
}
