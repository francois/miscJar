package misc;

import java.util.ArrayList;
import java.util.Stack;
import java.util.Vector;

public class Story {

    // ========================================
    static public final String
	BroadcastStory		= "Story";

    public abstract class Command {
	public final String name;
	public Command (String name) { this.name = name; }
	public abstract void exec ();
	public abstract void undo ();
	public void displayExec () { display (); }
	public void displayUndo () { display (); }
	public void display () {}
    }

    public abstract class Commands extends Command {
	ArrayList<Command> commands = new ArrayList<Command> ();
	public Commands (String name) { super (name); }
	public Story getStory () { return Story.this; }
	public int size () { return commands.size (); }
	public void add (Command command) {
	    commands.add (command);
	}
	public void exec () {
	    // XXX si erreur undo jusqu'a l'etape
	    for (Command command : commands) {
		command.exec ();
		command.displayExec ();
	    }
	}
	public void undo () {
	    for (int i = commands.size ()-1; i >= 0; i--) {
		Command command = commands.get (i);
		command.undo ();
		command.displayUndo ();
	    }
	}
	public String toString () {
	    String result = "";
	    for (Command command : commands)
		result += " "+command.name;
	    return result;
	}
    }

    // ========================================
    StateNotifier observable;
    Stack<Command> undo = new Stack<Command> ();
    Stack<Command> redo = new Stack<Command> ();

    public Vector<String> getUndoCmd () {
	return getCmd (undo);
    }
    public Vector<String> getRedoCmd () {
	return getCmd (redo);
    }
    public Vector<String> getCmd (Stack<Command> commands) {
	Vector<String> result = new Vector<String> (commands.size ());
	for (int i = commands.size () - 1; i >= 0; i--)
	    result.add (commands.get (i).name);
	return result;
    }

    public Story (StateNotifier observable) {
	this.observable = observable;
    }
    public void addUpdateObserver (Object object) {
	observable.addUpdateObserver (object, BroadcastStory);
    }
    public void removeObserver (Object object) {
	observable.removeObserver (object);
    }

    public void add (Command command) {
	try {
	    if (command instanceof Commands && ((Commands) command).commands.size () < 1)
		return;
	    command.exec ();
	    command.displayExec ();
	    undo.push (command);
	} finally {
	    redo.clear ();
	    observable.broadcastUpdate (BroadcastStory);
	}
    };

    // ========================================
    Command lastSaved;
    Command lastCommand () {
	return undo.empty () ? null : undo.peek ();
    }

    public void markNotSaved () {
	lastSaved = new Command ("") { public void exec () {}	public void undo () {} };
	observable.broadcastUpdate (BroadcastStory);
    }
    public void markSaved () {
	lastSaved = lastCommand ();
	observable.broadcastUpdate (BroadcastStory);
    }
    public boolean isModified () {
	return lastSaved != lastCommand ();
    }

    // ========================================
    public boolean undoAvailable () {
	return !undo.empty ();
    }
    public boolean redoAvailable () {
	return !redo.empty ();
    }
    public ArrayList<String> getUndoCommande () { return getString (undo); }
    public ArrayList<String> getRedoCommande () { return getString (redo); }
    ArrayList<String> getString (Stack<Command> stack) {
	ArrayList<String> result = new ArrayList<String> ();
	for (Command command : stack)
	    result.add (command.name);
	return result;
    }

    // ========================================
    public void undo (int level) {
	try {
	    for (int i = 0; i < level; i++) {
		Command command = undo.pop ();
		command.undo ();
		command.displayUndo ();
		redo.push (command);
	    }
	} catch (Exception e) {
	}
	observable.broadcastUpdate (BroadcastStory);
    }

    public void redo (int level) {
	try {
	    for (int i = 0; i < level; i++) {
		Command command = redo.pop ();
		command.exec ();
		command.displayExec ();
		undo.push (command);
	    }
	} catch (Exception e) {
	}
	observable.broadcastUpdate (BroadcastStory);
    }

    // ========================================
}
