package misc;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.net.Socket;
import java.net.SocketException;
import java.text.MessageFormat;
import java.util.Hashtable;
import java.util.StringTokenizer;
import java.util.TreeSet;

public class CommandLineWaiter<Model> {

    // ========================================
    static public final int MAX_COL = 6;

    // ========================================
    protected Model model;
    protected CommandLineServer<Model> commandLineServer;

    private TreeSet<String> commands;
    private Hashtable<String, String> help;
    private Hashtable<String, Method> actionsMethod;

    /** The original call socket to be close at the end of analyse. */
    protected Socket call;
    /** Input chanel (of the socket). */
    protected BufferedReader in;
    /** Output chanel (of the socket). */
    protected PrintWriter out;
    protected String line;
    protected StringTokenizer arguments;

    public boolean hasMoreArg () {
	return arguments.hasMoreTokens ();
    }
    public String nextArg () {
	return arguments.nextToken ().trim ();
    }

    public void startCall () {}

    // ========================================
    public String getCommandLine (BufferedReader in)
	throws IOException {
	try {
	    return in.readLine ().trim ();
	} catch (SocketException e) {
	    return null;
	} catch (NullPointerException e) {
	    return null;
	}
    }
    // ========================================
    /**
     * Send a answer to the client with a newline and flush the output chanel.
     * @param str the answer to send.
     */
    public void answerln (String str) {
	out.println (str);
	out.flush ();
    }

    public void syntaxError () {
	answerln (Bundle.getMessage ("SyntaxeError"));
    }

    // ========================================
    /**
     * Give a help to the client.
     * @param cmd is null or empty, give the list of commands. Else, give a appropriate help for the command cmd.
     */
    public void actionHelp () {
	String arg = null;
	if (hasMoreArg ())
	    arg = nextArg ().toLowerCase ();
	if (arg == null || arg.isEmpty ())
	    answerln (Bundle.getString ("Help"+commandLineServer.getClassWaiterName ()+"_", null).replaceAll ("\\\\n", "\n")+
		      Util.toColumn (Util.set2string (commands), MAX_COL));
	else {
	    String explanation = help.get (arg);
	    if (explanation == null)
		answerln (MessageFormat.format (Bundle.getMessage ("NotACommand"), arg));
	    else
		answerln (arg.toLowerCase ()+" "+explanation);
	}
    }

    // ========================================
    public void actionQuit () {
	answerln (Bundle.getMessage ("SeeYouSoon").replaceAll ("\\\\n", "\n"));
	try {
	    call.close ();
	} catch (Exception e) {
	}
    }

    // ========================================
    public void actionExit () {
	answerln (Bundle.getMessage ("GoodBye").replaceAll ("\\\\n", "\n"));
	System.exit (0);
    }

    // ========================================
    /**
     * Open a chanel and start a shell interpretor.
     * @param call the client connection.
     */
    public CommandLineWaiter (CommandLineServer<Model> commandLineServer, Model model, Socket call) {
	this.commandLineServer = commandLineServer;
	this.model = model;
	this.call = call;

	commands = commandLineServer.getCommands ();
	help = commandLineServer.getHelp ();
	actionsMethod = commandLineServer.getActionsMethod ();
	try {
	    in = new BufferedReader (new InputStreamReader (call.getInputStream ()));
	    out = new PrintWriter (call.getOutputStream ());
	    startCall ();
	    (new Thread () {
		    public void run() {
			analyse ();
		    }
		}).start();
	} catch (IOException e) {
	    if (!call.isClosed ())
		Log.keepLastException ("CommandLineWaiter", e);
	}
    }

    // ========================================
    public void analyse () {
	String cmd = null;
	try {
	    for (; ! call.isClosed (); ) {
		line = getCommandLine (in);
		if (line == null)
		    break;
		if (line.isEmpty ())
		    continue;
		arguments = new StringTokenizer (line);
		if (!arguments.hasMoreTokens ())
		    continue;
		cmd = arguments.nextToken ().trim ().toLowerCase ();
		if (commands.contains (cmd))
		    actionsMethod.get (cmd).invoke (this);
		else
		    answerln (MessageFormat.format (Bundle.getMessage ("NotACommand"), cmd));
	    }
	} catch (Exception e) {
	    Log.keepLastException ("CommandLineWaiter::analyse ("+cmd+")", e);
	} finally {
	    try {
		call.close ();
	    } catch (Exception e) {
	    }
	}
    }

    // ========================================
}
