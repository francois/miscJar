package network;

import java.io.IOException;
import java.net.URLConnection;
import java.net.URL;
import java.net.Proxy;

public class XMLConnection {

    // ========================================
    static public URLConnection getXMLConnection (URL url)
	throws IOException {
	URLConnection urlConnection = url.openConnection ();
	urlConnection.setDoOutput (true);
	urlConnection.setUseCaches (false);
	urlConnection.setRequestProperty ("Accept", "application/xml");
	urlConnection.setRequestProperty ("User-Agent", "Merciol");
	urlConnection.setRequestProperty ("Content-type", "application/xml");
	return urlConnection;
    }

    // ========================================
    static public URLConnection getXMLConnection (URL url, Proxy proxy)
	throws IOException{
	URLConnection urlConnection = (proxy == null) ? url.openConnection () : url.openConnection (proxy);
	urlConnection.setDoOutput (true);
	urlConnection.setUseCaches (false);
	urlConnection.setRequestProperty ("Accept", "application/xml");
	urlConnection.setRequestProperty ("User-Agent", "Merciol");
	urlConnection.setRequestProperty ("Content-type", "application/xml");
	return urlConnection;
    }

    public void connect () {
	throw new IllegalArgumentException ("Can't reconnect an XMLConnection");
    }

    // ========================================
}
