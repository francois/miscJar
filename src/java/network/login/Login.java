package network.login;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Random;
import java.util.Vector;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import misc.Bundle;
import misc.Log;
import misc.Util;
import network.Protocol;
import network.Waiter;

/**
 * <?xml version="1.0" encoding="UTF-8"?>
 * <bundle>
 *   <paquet>
 *     <application name="login">
 *       <argument user="unknown" />
 *       <argument user="unknown" challenge="AA6C1B8851EBBAC2" try="x" />
 *       <argument user="reject" />
 *       <argument user="accept" result="AA6C1B8851EBBAC2" />

 *       <argument info="passwdUnchanged" />
 *       <argument info="passwdUpdated" />
 *     </application>
 *   </paquet>
 * </bundle>
 *
 * <?xml version="1.0" encoding="UTF-8"?>
 * <bundle>
 *   <paquet pearId="AA6C1B8851EBBAC2">
 *     <application name="login">
 *       <argument ask="connect" login="inconnu_561C" />
 *       <argument ask="connect" login="inconnu_561C" result="AA6C1B8851EBBAC2" />
 *       <argument ask="disconnect" />
 *       <argument ask="changepass" old="" new="" />
 *     </application>
 *   </paquet>
 * </bundle>
 */

public class Login implements Waiter {

    static public final boolean trace = false;

    // ========================================
    static public final String applicationName        = "login";
    static public final String userToken              = "user";
    static public final String askToken               = "ask";
    static public final String loginToken             = "login";
    static public final String challengeToken         = "challenge";
    static public final String tryIndexToken          = "try";
    static public final String resultToken            = "result";
    static public final String oldToken               = "old";
    static public final String newToken               = "new";
    static public final String unknownToken           = "unknown";
    static public final String rejectToken            = "reject";
    static public final String acceptToken            = "accept";
    static public final String connectToken           = "connect";
    static public final String disconnectToken        = "disconnect";
    static public final String changepassToken        = "changepass";
    static public final String groupToken             = "group";
    static public final String groupsToken            = "groups";
    static public final String setGroupToken          = "setGroup";
    static public final String infoToken              = "info";
    static public final String nameToken              = "name";
    static public final String joinToken              = "join";
    static public final String leftToken              = "left";
    static public final String createdToken           = "created";
    static public final String updatedToken           = "updated";
    static public final String ipToken                = "ip";
    static public final String updatedByToken         = "updatedBy";
    static public final String adminToken             = "admin";
    static public final String memberToken            = "member";
    static public final String createGroupToken       = "createGroup";
    static public final String removeGroupToken       = "removeGroup";
    static public final String addGroupAdminToken     = "addGroupAdmin";
    static public final String removeGroupAdminToken  = "removeGroupAdmin";
    static public final String addGroupMemberToken    = "addGroupMember";
    static public final String removeGroupMemberToken = "removeGroupMember";

    // ========================================
    private Protocol protocol;
    /** unknown, reject, accept, untrusted */
    private String state = unknownToken;
    private String login = "";
    private String tryIndex = "";
    private String cifferPasswd = "";
    private String challenge;
    private String group = "";
    private String[] groups = new String [0];

    public synchronized void setProtocol (Protocol protocol) {
	if (trace)
	    System.err.println ("setProtocol: protocol:"+protocol);
	if (this.protocol != null)
	    this.protocol.removeWaiter (applicationName, this);
	this.protocol = protocol;
	if (protocol != null)
	    protocol.addWaiter (applicationName, this);
    }

    public String getState () { return state; }
    public String getTryIndex () { return tryIndex; }
    public String getLogin () { return login; }
    public String getGroup () { return group; }
    public Protocol getProtocol () { return protocol; }

    // ========================================
    static public String getRandomLogin () {
	return
	    Bundle.getString ("anonymous", null)+"_"+Integer.toHexString ((new Random ()).nextInt ()).toUpperCase ().substring (0, 4);
    }

    // ========================================
    public Login (String login) {
	this.login = login;
    }

    public Login (String login, Protocol protocol) {
	this (login);
	setProtocol (protocol);
    }
    
    // ========================================
    public synchronized void unlog () {
	if (protocol == null)
	    return;
	protocol.send (applicationName, askToken, disconnectToken);
    }

    // ========================================
    public synchronized void log (String login, String cifferPasswd) {
	if (trace)
	    System.err.println ("log: login:"+login+" cifferPasswd:"+cifferPasswd+" challenge:"+challenge);
	if (login == null)
	    return;
	if (protocol == null)
	    return;
	this.login = login;
	try {
	    if (challenge != null) {
		this.cifferPasswd = cifferPasswd;
		protocol.send (applicationName, askToken, connectToken, loginToken, login, resultToken, Util.sha1 (challenge+cifferPasswd));
	    } else
		protocol.send (applicationName, askToken, connectToken, loginToken, login);
	} catch (Exception e) {
	    Log.keepLastException ("Login::log", e);
	    protocol.send (applicationName, askToken, connectToken, loginToken, login);
	}
    }

    // ========================================
    public synchronized void setGroup (String groupName) {
	if (protocol == null)
	    return;
	protocol.send (applicationName, askToken, setGroupToken, groupToken, groupName);
    }

    public synchronized void unsetGroup () {
	setGroup ("");
    }

    public synchronized void askGroups () {
	if (protocol == null)
	    return;
	groups = new String [0];
	protocol.send (applicationName, askToken, groupsToken);
    }

    public synchronized void askGroup (String groupName) {
	if (protocol == null)
	    return;
	protocol.send (applicationName, askToken, groupToken, groupToken, groupName);
    }

    public synchronized void createGroup (String groupName) {
	if (protocol == null)
	    return;
	protocol.send (applicationName, askToken, createGroupToken, groupToken, groupName);
    }

    public synchronized void removeGroup (String groupName) {
	if (protocol == null)
	    return;
	protocol.send (applicationName, askToken, removeGroupToken, groupToken, groupName);
    }

    public synchronized void addGroupAdmin (String groupName, String admin) {
	if (protocol == null)
	    return;
	protocol.send (applicationName, askToken, addGroupAdminToken, groupToken, groupName, loginToken, admin);
    }

    public synchronized void removeGroupAdmin (String groupName, String admin) {
	if (protocol == null)
	    return;
	protocol.send (applicationName, askToken, removeGroupAdminToken, groupToken, groupName, loginToken, admin);
    }

    public synchronized void addGroupMember (String groupName, String member) {
	if (protocol == null)
	    return;
	protocol.send (applicationName, askToken, addGroupMemberToken, groupToken, groupName, loginToken, member);
    }

    public synchronized void removeGroupMember (String groupName, String member) {
	if (protocol == null)
	    return;
	protocol.send (applicationName, askToken, removeGroupMemberToken, groupToken, groupName, loginToken, member);
    }

    // ========================================
    public synchronized void receiveUser (String state, String result, String challenge, String tryIndex) {
	if (unknownToken.equals (state)) {
	    this.state = state;
	    this.challenge = challenge;
	    this.tryIndex = tryIndex;
	    group = "";
	    broadcastUpdateGroup ();
	    broadcastLoginState ();
	} else if (rejectToken.equals (state)) {
	    this.state = state;
	    broadcastLoginState ();
	} else if (acceptToken.equals (state)) {
	    this.tryIndex = "";
	    String check = null;
	    try {
		check = Util.sha1 (this.challenge+login+cifferPasswd);
	    } catch (Exception e) {
	    }
	    if (trace)
		System.err.println ("receive: challenge:"+this.challenge+" login:"+login+" cifferPasswd:"+cifferPasswd+" check:"+check);
	    if (result.equals (check)) {
		this.state = state;
		broadcastLoginState ();
		broadcastUpdateLogin ();
	    } else {
		this.state = "Untrusted";
		broadcastLoginState ();
	    }
	} else
	    System.err.println ("receive: unknown state:"+state+" challenge:"+challenge+" result:"+result);
    }

    public class Group {
	public String name;
	public String created;
	public String updated;
	public String ip;
	public String updatedBy;
	public String[] admin = new String [0];
	public String[] member = new String [0];
    }

    // ========================================
    public synchronized void receive (Element argument) {
	String state = argument.getAttribute (userToken);
	if (!state.isEmpty ()) {
	    String tryIndex = argument.getAttribute (tryIndexToken);
	    String challenge = argument.getAttribute (challengeToken);
	    String result = argument.getAttribute (resultToken);
	    if (trace)
		System.err.println ("receive: state:"+state+" challenge:"+challenge+" result:"+result);
	    receiveUser (state, result, challenge, tryIndex);
	    return;
	}
	String groupA = argument.getAttribute (groupToken);
	if (!groupA.isEmpty ()) {
	    if (joinToken.equals (groupA))
		this.group = argument.getAttribute (nameToken);
	    else if (leftToken.equals (groupA))
		this.group = "";
	    broadcastUpdateGroup ();
	    return;
	}
	String info = argument.getAttribute (infoToken);
	if (!info.isEmpty ()) {
	    if (groupsToken.equals (info)) {
		NodeList groupsName = argument.getElementsByTagName (groupToken);
		Vector<String> result = new Vector<String> ();
		for (int i = 0; i < groupsName.getLength (); i++) {
		    Element groupName = (Element) groupsName.item (i);
		    result.add (groupName.getAttribute (nameToken));
		}
		groups = result.toArray (new String [0]);
		Arrays.sort (groups);
		broadcastUpdateGroups ();
	    } else if (groupToken.equals (info)) {

		Group group = new Group ();
		NodeList groupsNL = argument.getElementsByTagName (groupToken);
		// XXX vérification 1 seul élément
		Element groupE  = (Element) groupsNL.item (0);
		group.name      = groupE.getAttribute (nameToken);
		group.created   = groupE.getAttribute (createdToken);
		group.updated   = groupE.getAttribute (updatedToken);
		group.ip        = groupE.getAttribute (ipToken);
		group.updatedBy = groupE.getAttribute (updatedByToken);

		NodeList AdminNL = argument.getElementsByTagName (adminToken);
		Vector<String> result = new Vector<String> ();
		for (int i = 0; i < AdminNL.getLength (); i++) {
		    Element AdminE = (Element) AdminNL.item (i);
		    result.add (AdminE.getAttribute (nameToken));
		}
		group.admin = result.toArray (new String [0]);
		Arrays.sort (group.admin);
		NodeList memberNL = argument.getElementsByTagName (memberToken);
		result = new Vector<String> ();
		for (int i = 0; i < memberNL.getLength (); i++) {
		    Element memberE = (Element) memberNL.item (i);
		    result.add (memberE.getAttribute (nameToken));
		}
		group.member = result.toArray (new String [0]);
		Arrays.sort (group.member);

		broadcastInfoGroup (group);
	    }
	    // XXX passwdUnchanged
	    // XXX passwdUpdated
	}
    }

    // ========================================
    Vector<LoginObserver> loginObservers = new Vector<LoginObserver> ();
    public void addLoginObserver (LoginObserver loginObserver) { loginObservers.add (loginObserver); }
    public void removeLoginObserver (LoginObserver loginObserver) { loginObservers.remove (loginObserver); }

    public void broadcastUpdateLogin () {
	if (trace)
	    System.err.println ("broadcastUpdateLogin: login:"+login);
	for (LoginObserver loginObserver : loginObservers)
	    loginObserver.updateLogin (login);
    }

    public void broadcastUpdateGroup () {
	if (trace)
	    System.err.println ("broadcastUpdateGroup: group:"+group);
	for (LoginObserver loginObserver : loginObservers)
	    loginObserver.updateGroup (group);
    }

    public void broadcastUpdateGroups () {
	if (trace)
	    System.err.println ("broadcastUpdateGroups: groups size:"+groups.length);
	for (LoginObserver loginObserver : loginObservers)
	    loginObserver.updateGroups (groups);
    }

    public void broadcastInfoGroup (Group group) {
	if (trace)
	    System.err.println ("broadcastInfoGroup: group:");
	for (LoginObserver loginObserver : loginObservers)
	    loginObserver.infoGroup (group);
    }

    public void broadcastLoginState () {
	if (trace)
	    System.err.println ("broadcastLoginState: state:"+state);
	for (LoginObserver loginObserver : loginObservers)
	    loginObserver.loginState (state);
    }

    // ========================================
}
