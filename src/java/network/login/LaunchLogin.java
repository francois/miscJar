package network.login;

import javax.swing.SwingUtilities;
import misc.Bundle;
import misc.Config;

public class LaunchLogin {

    // ========================================
    static public void main (String[] args) {
	Config.setPWD (LaunchLogin.class);
	Config.load ("Login");
	Bundle.load ("Help");
	Bundle.load ("ToolBar");
	Bundle.load ("Controller");
	Bundle.load ("Protocol");
	Bundle.load ("Login");

        SwingUtilities.invokeLater (new Runnable () {
		public void run () {
		    new LoginController ();
		}
	    });
    }

    // ========================================
}
