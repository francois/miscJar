package network.login;

import java.util.Hashtable;
import javax.swing.AbstractButton;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JMenu;
import javax.swing.JMenuBar;

import network.ProtocolManager;

import misc.ApplicationManager;
import misc.Log;
import misc.ToolBarManager;
import misc.Util;

/**
   La barre de menu.
*/
@SuppressWarnings ("serial") public class JLoginMenuBar extends JMenuBar {

    // ========================================
    public JLoginMenuBar (ApplicationManager controllerManager,
			  ProtocolManager protocolManager, LoginManager loginManager,
			  ApplicationManager helpManager, ToolBarManager toolBarManager) {
	setLayout (new BoxLayout (this, BoxLayout.X_AXIS));
	JMenu fileMenu = Util.addJMenu (this, "File");
	JMenu networkMenu = Util.addJMenu (this, "Network");
	JMenu loginMenu = Util.addJMenu (this, "Connection");
	add (Box.createHorizontalGlue ());
	JMenu helpMenu = Util.addJMenu (this, "Help");

	controllerManager.addMenuItem (fileMenu);
	protocolManager.addMenuItem (networkMenu);
	loginManager.addMenuItem (loginMenu);
	helpManager.addMenuItem (helpMenu);
	toolBarManager.addMenuItem (helpMenu);

	Hashtable<String, AbstractButton> buttons = new Hashtable<String, AbstractButton> ();
	Util.collectButtons (buttons, fileMenu);
	Util.collectButtons (buttons, networkMenu);
	Util.collectButtons (buttons, loginMenu);
	Util.collectButtons (buttons, helpMenu);

	controllerManager.addActiveButtons (buttons);
	protocolManager.addActiveButtons (buttons);
	loginManager.addActiveButtons (buttons);
	helpManager.addActiveButtons (buttons);
	toolBarManager.addActiveButtons (buttons);
    }

    // ========================================
}
