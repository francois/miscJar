package network.login;

import java.awt.Component;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Hashtable;
import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.plaf.basic.BasicBorders;

import misc.Util;
import network.ProtocolManager;
import static misc.Util.GBC;
import static misc.Util.GBCNL;

@SuppressWarnings ("serial") public class JLogin extends JPanel implements LoginObserver {

    // ========================================
    private Login login;

    public JLabel loggedLabel = new JLabel ();
    public JLabel groupLabel = new JLabel ();
    private JLabel stateLabel = new JLabel ();

    public Login getLogin () { return login; }

    // ========================================
    public JLogin (ProtocolManager protocolManager, LoginManager loginManager) {
	super (new GridBagLayout ());
	login = loginManager.getLogin ();
	login.addLoginObserver (this);

	Util.addLabelFields (this, "Logged", loggedLabel);
	Util.addLabelFields (this, "Group", groupLabel);
	Util.addLabelFields (this, "State", stateLabel);
    }

    // ========================================
    public void updateLogin (String login) {
	loggedLabel.setText (login);
    }

    public void updateGroup (String group) {
	groupLabel.setText (group);
    }

    public void updateGroups (String[] groupsName) {
    }

    public void infoGroup (Login.Group group) {
    }

    public void loginState (String state) {
	stateLabel.setText (state);
    }

    // ========================================
}
