package network.login;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Image;
import java.text.MessageFormat;
import javax.swing.JMenuBar;
import javax.swing.JPanel;

import network.ProtocolManager;

import misc.Controller;
import misc.Bundle;
import misc.Config;
import misc.HelpManager;
import misc.MultiToolBarBorderLayout;
import misc.ToolBarManager;
import misc.Util;

public class LoginController extends Controller<Login> implements LoginObserver {

    // ========================================
    public LoginController () {
	super (new Login (Config.getString ("pseudo", Login.getRandomLogin ())));
    }

    // ========================================
    Login login;

    ProtocolManager protocolManager;
    LoginManager loginManager;
    HelpManager helpManager;
    ToolBarManager toolBarManager;

    JLogin jLogin;
    JLoginMenuBar jLoginMenuBar;
    JLoginToolBar jLoginToolBar;

    // ========================================
    public String getTitle () { return MessageFormat.format (Bundle.getTitle ("Login"), login.getLogin ()); }
    public Image getIcon () { return Util.loadImage (Config.getString ("LoginIcon", "data/images/login/login.png")); }

    // ========================================
    protected boolean tryClosingWindows () {
	Config.save ("Login");
	return true;
    }

    // ========================================
    protected void createModel (Login login) {
	this.login = login;
	login.addLoginObserver (this);
    }

    // ========================================
    protected Component createGUI () {
	JPanel contentPane = new JPanel (new MultiToolBarBorderLayout ());
	toolBarManager = new ToolBarManager (getIcon (), contentPane);

	protocolManager = new ProtocolManager (this);
	protocolManager.addWaiter (login);
	protocolManager.start ();

	loginManager = new LoginManager (this, login);
	helpManager = new HelpManager (this, "Login");
	jLogin = new JLogin (protocolManager, loginManager);
 
	jLoginToolBar = new JLoginToolBar (this, protocolManager, loginManager, helpManager, toolBarManager);
	contentPane.add (jLogin, BorderLayout.CENTER);
	return contentPane;
    }

    // ========================================
    protected JMenuBar createMenuBar () {
	jLoginMenuBar = new JLoginMenuBar (this, protocolManager, loginManager, helpManager, toolBarManager);
	return jLoginMenuBar;
    }

    // ========================================
    public void updateLogin (String login) {}
    public void updateGroup (String group) {}
    public void updateGroups (String[] groupsName) {}
    public void infoGroup (Login.Group group) {}
    public void loginState (String state) {}

    // ========================================
}
