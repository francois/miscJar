package network.login;

/**
   Modificaion de modèle qui peuvent être observé.
*/
public interface LoginObserver {

    // ========================================
    public void updateLogin (String login);
    public void updateGroup (String groupName);
    public void updateGroups (String[] groupsName);
    public void infoGroup (Login.Group group);
    public void loginState (String state);

    // ========================================
}
