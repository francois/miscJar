package network.login;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

import javax.swing.AbstractButton;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import misc.ApplicationManager;
import misc.Bundle;
import misc.Config;
import misc.Log;
import misc.OwnFrame;
import misc.Util;
import static misc.Util.GBC;
import static misc.Util.GBCNL;

/**
   XXX loginTitle a mettre à jour "Login"+Bundle.titlePostfix

   comportement déclanché par des actionneurs graphiques (menu ou bouton).
*/
@SuppressWarnings ("serial") public class LoginManager implements ApplicationManager, ActionListener, LoginObserver {

    // ========================================
    static public final String actionLog          = "Log";
    static public final String actionUnlog        = "Unlog";
    static public final String actionJoinGroup    = "JoinGroup";
    static public final String actionLeftGroup    = "LeftGroup";
    static public final String actionEditLogin    = "EditLogin";
    static public final String actionEditGroup    = "EditGroup";
    static public final String actionCreateGroup  = "CreateGroup";
    static public final String actionRemoveGroup  = "RemoveGroup";
    static public final String actionAddAdmin     = "AddAdmin";
    static public final String actionRemoveAdmin  = "RemoveAdmin";
    static public final String actionAddMember    = "AddMember";
    static public final String actionRemoveMember = "RemoveMember";

    static public final List<String> loginActionsNames =
	Arrays.asList (actionLog, actionUnlog, actionJoinGroup, actionLeftGroup,
		       actionEditLogin, actionEditGroup);
    static public final List<String> dialogActionsNames =
	Arrays.asList (actionCreateGroup, actionRemoveGroup,
		       actionAddAdmin, actionRemoveAdmin, actionAddMember, actionRemoveMember);
    @SuppressWarnings ("unchecked")
    static public final List<String> actionsNames =
	Util.merge (loginActionsNames, dialogActionsNames);
    @SuppressWarnings ("unchecked")
    static public final Hashtable<String, Method> actionsMethod =
	Util.collectMethod (LoginManager.class, actionsNames);

    public void  actionPerformed (ActionEvent e) {
	Util.actionPerformed (actionsMethod, e, this);
    }

    // ========================================
    private OwnFrame controller;
    private Login login;

    private JPanel loginPanel;
    private JComboBox<String> pseudoCB;
    private JPasswordField pseudoPasswordField;
    private JLabel tryLabel;

    private JPanel groupPanel;
    private JList<String> groupsList;

    private JPanel editLoginPanel;

    private JLabel pseudoLabel;
    private JLabel createdAtLoginLabel;
    private JLabel updatedAtLoginLabel;
    private JLabel updatedByIPLoginLabel;
    private JLabel updatedByLoginLabel;
    private JTextField emailTF;
    private JPasswordField oldPasswordField;
    private JPasswordField newPasswordField;
    private JPasswordField confirmPasswordField;

    private String askedGroupName;

    private JPanel editGroupPanel;
    private JComboBox<String> groupNameCB;
    private JList<String> groupsNameList;
    private JButton createGroupButton;
    private JButton removeGroupButton;

    private JLabel groupNameLabel;
    private JLabel createdLabel;
    private JLabel updatedLabel;
    private JLabel ipLabel;
    private JLabel updatedByLabel;

    private JComboBox<String> adminCB;
    private JList<String> adminList;
    private JButton addAdminButton;
    private JButton removeAdminButton;

    private JComboBox<String> memberCB;
    private JList<String> memberList;
    private JButton addMemberButton;
    private JButton removeMemberButton;

    public Login getLogin () { return login; }

    public void addMenuItem (JMenu... jMenu) {
	int idx = 0;
	Util.addMenuItem (loginActionsNames, this, jMenu[idx++]);
    }
    public void addIconButtons (Container... containers) {
	int idx = 0;
	Util.addIconButton (loginActionsNames, this, containers[idx++]);
    }

    // ========================================
    public LoginManager (OwnFrame controller, Login login) {
	this.controller = controller;
	this.login = login;

	for (String actionName : Arrays.asList (actionLog, actionUnlog,
						actionEditLogin, actionEditGroup, actionJoinGroup, actionLeftGroup))
	    commands.put (actionName, new Vector<AbstractButton> ());


	pseudoCB = new JComboBox<String> ();
	Config.loadJComboBox ("pseudo", pseudoCB, Login.getRandomLogin ());
	pseudoCB.setEditable (true);
	pseudoPasswordField = new JPasswordField ();
	tryLabel = new JLabel ();

	loginPanel = Util.getGridBagPanel ();

	Util.addLabelFields (loginPanel, "Login", pseudoCB);
	Util.addLabelFields (loginPanel, "Password", pseudoPasswordField);
	Util.addLabelFields (loginPanel, "Try", tryLabel);

	groupsList = new JList<String> ();
	groupsList.setSelectionMode (ListSelectionModel.SINGLE_INTERVAL_SELECTION);
	groupsList.setLayoutOrientation (JList.VERTICAL_WRAP);
	groupsList.setVisibleRowCount (-1);
	JScrollPane listScroller = new JScrollPane (groupsList);
	listScroller.setPreferredSize (new Dimension (250, 80));

	groupPanel = Util.getGridBagPanel ();

	Util.addLabel ("JoinGroup", SwingConstants.RIGHT, groupPanel, GBCNL);
	Util.addComponent (listScroller, groupPanel, GBCNL);

	groupsNameList = new JList<String> ();
	groupsNameList.setSelectionMode (ListSelectionModel.SINGLE_INTERVAL_SELECTION);
	groupsNameList.setLayoutOrientation (JList.VERTICAL_WRAP);
	groupsNameList.setVisibleRowCount (-1);
	JScrollPane groupsNameListScroller = new JScrollPane (groupsNameList);
	groupsNameListScroller.setPreferredSize (new Dimension (200, 50));

	groupsNameList.addListSelectionListener (new ListSelectionListener () {
		public void valueChanged (ListSelectionEvent e) {
		    actionSelectGroup ();
		}
	    });

	adminList = new JList<String> ();
	adminList.setSelectionMode (ListSelectionModel.SINGLE_INTERVAL_SELECTION);
	adminList.setLayoutOrientation (JList.VERTICAL_WRAP);
	adminList.setVisibleRowCount (-1);
	JScrollPane adminListScroller = new JScrollPane (adminList);
	adminListScroller.setPreferredSize (new Dimension (200, 50));

	memberList = new JList<String> ();
	memberList.setSelectionMode (ListSelectionModel.SINGLE_INTERVAL_SELECTION);
	memberList.setLayoutOrientation (JList.VERTICAL_WRAP);
	memberList.setVisibleRowCount (-1);
	JScrollPane memberListScroller = new JScrollPane (memberList);
	memberListScroller.setPreferredSize (new Dimension (200, 50));

	editLoginPanel = Util.getGridBagPanel ();

	pseudoLabel = new JLabel ();
	createdAtLoginLabel = new JLabel ();
	updatedAtLoginLabel = new JLabel ();
	updatedByLoginLabel = new JLabel ();
	updatedByIPLoginLabel = new JLabel ();
	emailTF = new JTextField (20);
	oldPasswordField = new JPasswordField (10);
	newPasswordField = new JPasswordField (10);
	confirmPasswordField = new JPasswordField (10);

	Util.addLabelFields (editLoginPanel, "Login", pseudoLabel);
	Util.addLabelFields (editLoginPanel, "CreatedAt", createdAtLoginLabel);
	Util.addLabelFields (editLoginPanel, "UpdatedAt", updatedAtLoginLabel);
	Util.addLabelFields (editLoginPanel, "UpdatedBy", updatedByLoginLabel);
	Util.addLabelFields (editLoginPanel, "UpdatedByIP", updatedByIPLoginLabel);
	Util.addLabelFields (editLoginPanel, "Email", emailTF);
	Util.addLabelFields (editLoginPanel, "OldPasswd", oldPasswordField);
	Util.addLabelFields (editLoginPanel, "NewPasswd", newPasswordField);
	Util.addLabelFields (editLoginPanel, "ConfirmPasswd", confirmPasswordField);

	groupNameCB = new JComboBox<String> ();
	adminCB = new JComboBox<String> ();
	memberCB = new JComboBox<String> ();
	Config.loadJComboBox ("groupName", groupNameCB, "");
	Config.loadJComboBox ("groupAdmin", adminCB, "");
	Config.loadJComboBox ("groupMember", memberCB, "");
	groupNameCB.setEditable (true);
	adminCB.setEditable (true);
	memberCB.setEditable (true);

	editGroupPanel = Util.getGridBagPanel ();

	Util.addComponent (groupNameCB, editGroupPanel, GBC);
	createGroupButton = Util.addButton (actionCreateGroup, this, editGroupPanel, GBCNL);
	Util.addComponent (groupsNameListScroller, editGroupPanel, GBC);
	removeGroupButton = Util.addButton (actionRemoveGroup, this, editGroupPanel, GBCNL);

	groupNameLabel = new JLabel ();
	createdLabel = new JLabel ();
	updatedLabel = new JLabel ();
	ipLabel = new JLabel ();
	updatedByLabel = new JLabel ();

	Util.addLabelFields (editGroupPanel, "GroupName", groupNameLabel);
	Util.addLabelFields (editGroupPanel, "CreatedAt", createdLabel);
	Util.addLabelFields (editGroupPanel, "UpdatedAt", updatedLabel);
	Util.addLabelFields (editGroupPanel, "UpdatedByIP", ipLabel);
	Util.addLabelFields (editGroupPanel, "UpdatedBy", updatedByLabel);


	Util.addComponent (adminCB, editGroupPanel, GBC);
	addAdminButton = Util.addButton (actionAddAdmin, this, editGroupPanel, GBCNL);
	Util.addComponent (adminListScroller, editGroupPanel, GBC);
	removeAdminButton = Util.addButton (actionRemoveAdmin, this, editGroupPanel, GBCNL);

	Util.addComponent (memberCB, editGroupPanel, GBC);
	addMemberButton = Util.addButton (actionAddMember, this, editGroupPanel, GBCNL);
	Util.addComponent (memberListScroller, editGroupPanel, GBC);
	removeMemberButton = Util.addButton (actionRemoveMember, this, editGroupPanel, GBCNL);

	login.addLoginObserver (this);
    }

    // ========================================
    public void actionLog () {
	tryLabel.setText (login.getTryIndex ());
	if (JOptionPane.showConfirmDialog (controller.getJFrame (),
					   loginPanel,
					   Bundle.getTitle ("Login"),
					   JOptionPane.YES_NO_OPTION,
					   JOptionPane.QUESTION_MESSAGE) != JOptionPane.YES_OPTION)
	    return;
	Config.saveJComboBox ("pseudo", pseudoCB);
	String passwd = null;
	try {
	    if (pseudoPasswordField.getPassword ().length > 0)
		passwd = Util.sha1 (new String (pseudoPasswordField.getPassword ()));
	} catch (Exception e) {
	    Log.keepLastException ("LoginManager::actionLog", e);
	}
	login.log ((String) pseudoCB.getSelectedItem (), passwd);
    }

    // ========================================
    public void actionUnlog () {
	login.unlog ();
    }

    // ========================================
    public void actionJoinGroup () {
	login.askGroups ();
	if (JOptionPane.
	    showConfirmDialog (controller.getJFrame (),
			       groupPanel,
			       Bundle.getTitle ("Group"),
			       JOptionPane.YES_NO_OPTION,
			       JOptionPane.QUESTION_MESSAGE) != JOptionPane.YES_OPTION)
	    return;
	login.setGroup (groupsList.getSelectedValue ());
    }

    // ========================================
    public void actionLeftGroup () {
	login.unsetGroup ();
    }

    // ========================================
    public void actionEditLogin () {
	if (JOptionPane.showConfirmDialog (controller.getJFrame (),
					   editLoginPanel,
					   Bundle.getTitle ("ManageLogin"),
					   JOptionPane.YES_NO_OPTION,
					   JOptionPane.QUESTION_MESSAGE) != JOptionPane.YES_OPTION)
	    return;
	// XXX ecrire la fonction : edit login, password et email
	JOptionPane.
	    showMessageDialog (controller.getJFrame (),
			       "Cette fonction n'est encore pas developpee",
			       "Developpement en cours",
			       JOptionPane.QUESTION_MESSAGE);
    }

    // ========================================
    public void actionEditGroup () {
	login.askGroups ();
	JOptionPane.
	    showMessageDialog (controller.getJFrame (),
			       editGroupPanel,
			       Bundle.getTitle ("ManageGroup"),
			       JOptionPane.QUESTION_MESSAGE);
	groupsNameList.setListData (new String [0]);
	adminList.setListData (new String [0]);
	memberList.setListData (new String [0]);
	Config.saveJComboBox ("groupName", groupNameCB);
	Config.saveJComboBox ("groupAdmin", adminCB);
	Config.saveJComboBox ("groupMember", memberCB);
    }

    // ========================================
    public void actionSelectGroup () {
	infoGroup (null);
	askedGroupName = groupsNameList.getSelectedValue ();
	if (!askedGroupName.isEmpty ())
	    login.askGroup (askedGroupName);
    }

    // ========================================
    public void actionCreateGroup () {
	login.createGroup ((String) groupNameCB.getSelectedItem ());
    }
    public void actionRemoveGroup () {
	login.removeGroup (groupsNameList.getSelectedValue ());
    }
    public void actionAddAdmin () {
	login.addGroupAdmin (groupNameLabel.getText (), adminCB.getItemAt (adminCB.getSelectedIndex ()));
    }
    public void actionRemoveAdmin () {
	login.removeGroupAdmin (groupNameLabel.getText (), adminList.getSelectedValue ());
    }
    public void actionAddMember () {
	login.addGroupMember (groupNameLabel.getText (), memberCB.getItemAt (memberCB.getSelectedIndex ()));
    }
    public void actionRemoveMember () {
	login.removeGroupMember (groupNameLabel.getText (), memberList.getSelectedValue ());
    }

    // ========================================
    public void updateLogin (String login) {}

    public void updateGroup (String group) {
	updateCommands ();
	groupsList.setSelectedValue (group, true);
    }

    public void updateGroups (String[] groupsName) {
	if (groupsName == null)
	    return;
	groupsNameList.setListData (groupsName);
	if (askedGroupName != null && Arrays.binarySearch (groupsName, askedGroupName) < 0) {
	    askedGroupName = null;
	    infoGroup (null);
	}
	groupsList.setListData (groupsName);
	groupsList.setSelectedValue (login.getGroup (), true);
    }

    public void infoGroup (Login.Group group) {
	if (askedGroupName == null || group == null) {
	    groupNameLabel.setText ("");
	    createdLabel.setText ("");
	    updatedLabel.setText ("");
	    ipLabel.setText ("");
	    updatedByLabel.setText ("");
	    adminList.setListData (new String [0]);
	    memberList.setListData (new String [0]);
	} else if (askedGroupName.equals (group.name)) {
	    groupNameLabel.setText (group.name);
	    createdLabel.setText (group.created);
	    updatedLabel.setText (group.updated);
	    ipLabel.setText (group.ip);
	    updatedByLabel.setText (group.updatedBy);
	    adminList.setListData (group.admin);
	    memberList.setListData (group.member);
	}
    }

    public void loginState (String state) {
	updateCommands ();
	if (Login.unknownToken.equals (state)) {
	    actionLog ();
	}
    }

    // ========================================
    private Hashtable<String, Vector<AbstractButton>> commands = new Hashtable<String, Vector<AbstractButton>> ();
    public void addActiveButtons (Hashtable<String, AbstractButton> buttons) {
	for (String actionName : loginActionsNames)
	    commands.get (actionName).add (buttons.get (actionName));
	updateCommands ();
    }

    // public void removeCommand (String actionName, AbstractButton command) {
    // 		commands.get (actionName).remove (command);
    // }

    // ========================================
    public synchronized void updateCommands () {
	boolean isUnknowned = Login.unknownToken.equals (login.getState ());
	for (AbstractButton logCommand : commands.get (actionLog))
	    logCommand.setEnabled (isUnknowned);
	for (AbstractButton unlogCommand : commands.get (actionUnlog))
	    unlogCommand.setEnabled (!isUnknowned);
	for (AbstractButton editLoginCommand : commands.get (actionEditLogin))
	    editLoginCommand.setEnabled (!isUnknowned);
	for (AbstractButton editGroupCommand : commands.get (actionEditGroup))
	    editGroupCommand.setEnabled (!isUnknowned);
	boolean isGroup = !login.getGroup ().isEmpty ();
	for (AbstractButton joinGroupCommand : commands.get (actionJoinGroup))
	    joinGroupCommand.setEnabled (!isUnknowned && !isGroup);
	for (AbstractButton leftGroupCommand : commands.get (actionLeftGroup))
	    leftGroupCommand.setEnabled (!isUnknowned && isGroup);
    }

    // ========================================
}
