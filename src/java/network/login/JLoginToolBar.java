package network.login;

import java.awt.Component;
import java.util.Hashtable;
import javax.swing.AbstractButton;
import javax.swing.JToolBar;

import network.ProtocolManager;

import misc.ApplicationManager;
import misc.ToolBarManager;
import misc.Util;

public class JLoginToolBar {
    static public String defaultCardinalPoint = "North";

    public JLoginToolBar (ApplicationManager controllerManager,
			  ProtocolManager protocolManager, LoginManager loginManager,
			  ApplicationManager helpManager, ToolBarManager toolBarManager) {
	JToolBar fileToolBar = toolBarManager.newJToolBar ("File", defaultCardinalPoint);
	JToolBar loginToolBar = toolBarManager.newJToolBar ("Connection", defaultCardinalPoint);
	JToolBar protocolToolBar = toolBarManager.newJToolBar ("Network", defaultCardinalPoint);
	JToolBar helpToolBar = toolBarManager.newJToolBar ("Help", defaultCardinalPoint);

	controllerManager.addIconButtons (fileToolBar);
	loginManager.addIconButtons (loginToolBar);
	protocolManager.addIconButtons (protocolToolBar);
	if (helpManager != null)
	    helpManager.addIconButtons (helpToolBar);
	toolBarManager.addIconButtons (helpToolBar);

	Hashtable<String, AbstractButton> buttons = new Hashtable<String, AbstractButton> ();
	Util.collectButtons (buttons, fileToolBar);
	Util.collectButtons (buttons, loginToolBar);
	Util.collectButtons (buttons, protocolToolBar);
	Util.collectButtons (buttons, helpToolBar);

	controllerManager.addActiveButtons (buttons);
	loginManager.addActiveButtons (buttons);
	protocolManager.addActiveButtons (buttons);
	helpManager.addActiveButtons (buttons);
	toolBarManager.addActiveButtons (buttons);
    }
}
