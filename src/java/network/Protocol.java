package network;

import java.io.IOException;
import java.io.InputStream;
import java.util.Hashtable;
import java.util.List;
import java.util.Random;
import java.util.Vector;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import misc.Log;
import misc.XML;

/**
   <bundle waitAnswer="yes">
   <paquet pearId="XXXXXXXX">
   <application name="chat">
   <argument date="HH:MM:SS" speudo="inconnu_XXXX">msg</argument>
   </application>
   </paquet>
   </bundle>
*/

public abstract class Protocol {

    // remplace <jvmarg value="-Djava.net.preferIPv4Stack=true"/>
    // a placer avant la création de socket dans le cas ou elle n'a pas déjà été fait avant l'utilisation de X11 (via swing)
    static public final void setIpV4 () {
	java.util.Properties props = System.getProperties ();
	props.setProperty ("java.net.preferIPv4Stack", ""+true);
	System.setProperties (props);
    }

    static {
	setIpV4 ();
    }

    // ========================================
    static public final boolean trace = false;

    static public final String bundleToken      = "bundle";
    static public final String waitAnswerToken  = "waitAnswer";
    static public final String paquetToken      = "paquet";
    static public final String pearIdToken      = "pearId";
    static public final String applicationToken = "application";
    static public final String nameToken        = "name";
    static public final String argumentToken    = "argument";

    // ========================================
    static public final String ServerProtocolType = "ServerProtocol";
    static public final String ClientProtocolType = "ClientProtocol";

    /** socket, serverHTTP or clientHTTP */
    protected String type;
    public String getType () { return type; }
    protected final String pearId = Long.toHexString ((new Random ()).nextLong ()).toUpperCase ();

    public Protocol (String type) {
	this.type = type;
	if (trace)
	    System.err.println (type+":pearId: "+pearId);
    }

    // ========================================
    protected Thread currentThread;

    public abstract void start ();

    public synchronized boolean isAlive () {
	return currentThread != null && currentThread.isAlive ();
    }

    public void stop () {
	currentThread = null;
    }

    // ========================================
    public class PendingRequest {
	public Document request;
	public Element bundle;
	public Element localPaquet;
	public boolean readyToSend;

	public PendingRequest () {
	    try {
		DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance ().newDocumentBuilder ();
		request = documentBuilder.newDocument ();
		request.setXmlStandalone (true);
		bundle = request.createElement (bundleToken);
		request.appendChild (bundle);
		localPaquet = request.createElement (paquetToken);
		localPaquet.setAttribute (pearIdToken, pearId);
		bundle.appendChild (localPaquet);
	    } catch (ParserConfigurationException e) {
		Log.keepLastException ("Protocol::PendingRequest", e);
	    }
	}
    }

    // ========================================
    static public Element getBundle (InputStream in)
	throws IOException {
	Document request = null;
	request = XML.readDocument (in);
	if (trace) {
	    System.err.println ("Protocol:getBundle: ");
	    XML.writeDocument (request, System.err);
	    System.err.flush ();
	}
	return request.getDocumentElement ();
    }

    // ========================================
    protected void broadcastApplication (final String applicationName, final Element applicationArgument) {
	Vector<Waiter> waiters = namedWaiters.get (applicationName);
	if (waiters == null) {
	    if (trace)
		Log.writeLog ("Protocol", "no "+applicationName+" application registered for "+type);
	    return;
	}
	for (final Waiter waiter : waiters)
	    (new Thread () {
		    public void run () {
			if (trace)
			    System.err.println (type+" start "+applicationName);
			waiter.receive (applicationArgument);
		    }
		}).start ();
    }

    // ========================================
    // c'est la réception
    protected synchronized void broadcastApplications (Element bundle) {
	NodeList paquets = bundle.getElementsByTagName (paquetToken);
	for (int i = paquets.getLength () - 1; i >= 0; i--) {
	    Element paquet = (Element) paquets.item (i);
	    NodeList applications = paquet.getElementsByTagName (applicationToken);
	    for (int j = applications.getLength () - 1; j >= 0; j--) {
		Element application = (Element) applications.item (j);
		String applicationName = application.getAttribute (nameToken);
		NodeList applicationArguments = application.getElementsByTagName (argumentToken);
		if (applicationArguments.getLength () > 1)
		    throw new IllegalArgumentException ("too many argument for application "+applicationName);
		broadcastApplication (applicationName, (Element) applicationArguments.item (0));
	    }
	}
    }

    // ========================================
    abstract protected void addPendingApplication (String applicationName, Element applicationArgument);

    // ========================================
    // gestion des applications
    // ========================================

    private Hashtable<String, Vector<Waiter>> namedWaiters = new Hashtable<String, Vector<Waiter>> ();

    public synchronized void addWaiter (String applicationName, Waiter waiter) {
	if (trace)
	    System.err.println (type+" addWaiter: "+applicationName+" waiter:"+waiter);
	if (waiter == null)
	    return;
	Vector<Waiter> waiters = namedWaiters.get (applicationName);
	if (waiters == null) {
	    waiters = new Vector<Waiter> ();
	    namedWaiters.put (applicationName, waiters);
	}
	if (! waiters.contains (waiter))
	    waiters.add (waiter);
    }

    public synchronized void removeWaiter (String applicationName, Waiter waiter) {
	if (trace)
	    System.err.println (type+" removeWaiter: "+applicationName+" waiter:"+waiter);
	if (waiter == null)
	    return;
	Vector<Waiter> waiters = namedWaiters.get (applicationName);
	if (waiters == null)
	    return;
	waiters.remove (waiter);
	if (waiters.size () < 1)
	    namedWaiters.remove (applicationName);
    }

    // ========================================
    // c'est l'envoi
    public synchronized void send (String applicationName, Element applicationArgument) {
	if (trace)
	    System.err.println (type+": send "+applicationName);
	addPendingApplication (applicationName, applicationArgument);
    }

    // ========================================
    // c'est l'envoi
    public synchronized void send (String applicationName, String... args) {
	if (trace)
	    System.err.print (type+": send "+applicationName);
	Element applicationArgument = createArgument ();
	for (int i = 0; i < args.length; i += 2) {
	    if (trace)
		System.err.print (" "+args[i]+"=\""+args[i+1]+"\"");
	    applicationArgument.setAttribute (args[i], args[i+1]);
	}
	if (trace)
	    System.err.println ();
	send  (applicationName, applicationArgument);
    }

    // ========================================
    static public Element createArgument () {
	try {
	    Document document = DocumentBuilderFactory.newInstance ().newDocumentBuilder ().newDocument ();
	    return document.createElement (argumentToken);
	} catch (ParserConfigurationException e) {
	    return null;
	}
    }

    // ========================================
}
