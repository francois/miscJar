package network;

public interface ProtocolObserver {

    public void startProtocol (Protocol protocol);
}
