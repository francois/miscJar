package network;

import org.w3c.dom.Element;

public interface Waiter {

    // ========================================
    public void setProtocol (Protocol protocol);

    // ========================================
    public void receive (Element argument);

    // ========================================
}
