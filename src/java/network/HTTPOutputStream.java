package network;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;

public class HTTPOutputStream extends ByteArrayOutputStream {

    // ========================================
    private static SimpleDateFormat dateFormat = new SimpleDateFormat ("EEE, d MMM yyyy HH:mm:ss z");

    // ========================================
    private OutputStream out;
    private String status = "HTTP/1.0 200 OK";
    public Hashtable<String, String> headers = new Hashtable<String, String> ();

    // ========================================
    public HTTPOutputStream (OutputStream out) {
	this.out = out;
	headers.put ("Server", "Merciol");
	headers.put ("Date", dateFormat.format (new Date ()));
	headers.put ("Content-Type", "text/html");
	headers.put ("Connection", "close");
    }

    public void setStatus (String status) {
	this.status = status;
    }

    public void setHeader (String key, String value) {
	headers.put (key, value);
    }

    public void  close ()
	throws IOException {
	out.write ((status+"\n").getBytes ());
	for (String key : headers.keySet ()) {
	    out.write ((key+": "+headers.get (key)+"\n").getBytes ());
	}
	out.write (("Content-Length: "+size ()+"\n").getBytes ());
	out.write ("\n".getBytes ());
	writeTo (out);
    }

    // ========================================
}
