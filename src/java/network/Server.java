package network;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.Hashtable;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import misc.Log;
import misc.XML;

public class Server extends Protocol {

    // ========================================
    static public final boolean trace = false;

    static public final String loginApplicationName = "login";
    static public final String askToken             = "ask";
    static public final String connectToken         = "connect";
    static public final String disconnectToken      = "disconnect";
    static public final String userToken            = "user";
    static public final String acceptToken          = "accept";
    static public final String unknownToken         = "unknown";

    // ========================================
    private int port;

    public Server (int port) {
	super (ServerProtocolType);
	this.port = port;
    }

    // ========================================
    public void start () {
	(new Thread () {
		public void run () {
		    try {
			final ServerSocket serverSocket = new ServerSocket (port);
			Log.writeLog ("Protocol", "Server started on port "+port);
			// pour pouvoir arreter proprement l'activité
			for (currentThread = Thread.currentThread (); currentThread == Thread.currentThread (); ) {
			    try {
				final Socket call = serverSocket.accept ();
				(new Thread () {
					public void run () {
					    try {
						Log.writeLog ("Protocol", "Server accept "+call);
						exchangeXML (new HTTPInputStream (call.getInputStream ()),
							     new HTTPOutputStream (call.getOutputStream ()));
						call.close ();
					    } catch (SocketException e) {
						Log.writeLog ("Protocol", "Server connection lost "+call);
						try {
						    serverSocket.close ();
						    Log.writeLog ("Protocol", "Server close");
						} catch (IOException e2) {
						    Log.writeLog ("Protocol", "Server: "+e2);
						}
					    } catch (IOException e) {
						Log.keepLastException ("Server::start", e);
					    }
					}
				    }).start ();
			    } catch (SocketTimeoutException e) {
			    }
			}
			serverSocket.close ();
		    } catch (SocketException e) {
			Log.writeLog ("Protocol", "Server connection lost: "+e);
		    } catch (IOException e) {
			//} catch (SocketException e) {
			Log.keepLastException ("Server::start", e);
		    }
		    Log.writeLog ("Protocol", "Server stopped on port "+port);
		}
	    }).start ();
    }

    // ========================================
    public void stop () {
	currentThread = null;
	try {
	    // force le accept pour sortir du run
	    (new Socket ("localhost", port)).close ();
	} catch (Exception e) {
	}
	pendingRequests = new Hashtable<String, PendingRequest> ();
    }

    // ========================================
    private String getAskAndDelLogin (Element paquet) {
	if (trace)
	    System.err.println (type+":getConnectAndDelLogin:");

	String result = null;
	NodeList applications = paquet.getElementsByTagName (applicationToken);
	for (int j = applications.getLength () - 1; j >= 0; j--) {
	    Element application = (Element) applications.item (j);
	    if (loginApplicationName.equals (application.getAttribute (nameToken))) {
		NodeList applicationArguments = application.getElementsByTagName (argumentToken);
		if (applicationArguments.getLength () < 1)
		    continue;
		Element applicationArgument = (Element) applicationArguments.item (0);
		String ask = applicationArgument.getAttribute (askToken);
		if (connectToken.equals (ask) || disconnectToken.equals (ask))
		    result = ask;
		paquet.removeChild (application);
	    }
	}
	return result;
    }

    // ========================================
    private Element createUserAnswer (String state) {
	Element argument = createArgument ();
	argument.setAttribute (userToken, state);
	return argument;
    }

    // ========================================
    /** Server way : receive then send. */
    private synchronized void exchangeXML (InputStream in, OutputStream out)
	throws IOException {
	Element bundle = getBundle (in);
	String waitRequest = bundle.getAttribute (waitAnswerToken);
	NodeList paquets = bundle.getElementsByTagName (paquetToken);
	if (paquets.getLength () != 1)
	    // Si c'est un client, il n'y a qu'un paquet
	    Log.writeLog ("Protocol", "exchangeXML: Only one paquet expected ("+paquets.getLength ()+")!");
	// XXX test si nombre incohérant (normalement un seul paquet par bundle)
	Element paquet = (Element) paquets.item (0);
	String pearId = paquet.getAttribute (pearIdToken);
	addClient (pearId);

	String ask = getAskAndDelLogin (paquet);
	if (ask != null) {
	    try {
		DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance ().newDocumentBuilder ();
		Document emptyRequest = documentBuilder.newDocument ();
		emptyRequest.setXmlStandalone (true);

		Element applicationNode = emptyRequest.createElement (applicationToken);
		applicationNode.setAttribute (nameToken, loginApplicationName);
		applicationNode.appendChild (emptyRequest.importNode (createUserAnswer (connectToken.equals (ask) ? acceptToken : unknownToken), true));

		addPendingApplicationToClient (pearId, applicationNode);
	    } catch (ParserConfigurationException e) {
		Log.keepLastException ("Server::exchangeXML", e);
	    }
	}

	broadcastClients (paquet, pearId);
	broadcastApplications (bundle);
	PendingRequest pendingRequest = getPendingRequest (pearId, waitRequest != null && "yes".equals (waitRequest));
	XML.writeDocument (pendingRequest.request, out);
	out.flush ();
	out.close ();
    }

    // ========================================
    private Hashtable<String, PendingRequest> pendingRequests = new Hashtable<String, PendingRequest> ();

    // ========================================
    private synchronized void addClient (String pearId) {
	if (trace)
	    System.err.println (type+":addClient: "+pearId);
	// XXX si pearId est null ???
	if (pearId == null || pendingRequests.get (pearId) != null)
	    return;
	resetClient (pearId);
	// YYY nouveau client => startProtocol
    }

    // ========================================
    private synchronized void resetClient (String pearId) {
	if (trace)
	    System.err.println (type+":resetClient: "+pearId);
	pendingRequests.put (pearId, new PendingRequest ());
    }

    // ========================================
    private synchronized void sendClient (String toClientId, Element paquet) {
	PendingRequest pendingRequest = pendingRequests.get (toClientId);
	pendingRequest.bundle.appendChild (pendingRequest.request.importNode (paquet, true));
	pendingRequest.readyToSend = true;
	notifyAll ();
	if (trace) {
	    System.err.println (type+":broadcastClients: "+pearId);
	    XML.writeDocument (pendingRequest.request, System.err);
	    System.err.flush ();
	}
    }

    // ========================================
    private synchronized void broadcastClients (Element paquet, String fromClientId) {
	NodeList applications = paquet.getElementsByTagName (applicationToken);
	if (applications.getLength () < 1)
	    return;
	for (String toClientId : pendingRequests.keySet ()) {
	    if (toClientId.equals (fromClientId))
		continue;
	    sendClient (toClientId, paquet);
	}
    }

    // ========================================
    private synchronized PendingRequest getPendingRequest (String pearId, boolean waitRequest) {
	if (waitRequest) {
	    if (trace)
		System.err.println (type+":getPendingRequest: waitting for "+pearId);
	    for (;;) {
		PendingRequest pendingRequest = pendingRequests.get (pearId);
		if (pendingRequest.readyToSend)
		    break;
		try {
		    wait ();
		} catch (InterruptedException e) {
		}
	    }
	    if (trace)
		System.err.println (type+": getPendingRequest "+pearId+" is ready");
	}
	PendingRequest old = pendingRequests.get (pearId);
	resetClient (pearId);
	return old;
    }

    // ========================================
    protected void addPendingApplicationToClient (String pearId, Element applicationNode) {
	PendingRequest pendingRequest = pendingRequests.get (pearId);
	pendingRequest.localPaquet.appendChild (pendingRequest.request.importNode (applicationNode, true));
	pendingRequest.readyToSend = true;
	notifyAll ();
	if (trace) {
	    System.err.println (type+":addPendingApplication: "+pearId);
	    XML.writeDocument (pendingRequest.request, System.err);
	    System.err.flush ();
	}
    }

    // ========================================
    protected void addPendingApplication (String applicationName, Element applicationArgument) {
	try {
	    if (loginApplicationName.equals (applicationName)) {
		String ask = applicationArgument.getAttribute (askToken);
		if (connectToken.equals (ask))
		    broadcastApplication (loginApplicationName, createUserAnswer (acceptToken));
		else if (disconnectToken.equals (ask))
		    broadcastApplication (loginApplicationName, createUserAnswer (unknownToken));
		return;
	    }

	    DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance ().newDocumentBuilder ();
	    Document emptyRequest = documentBuilder.newDocument ();
	    emptyRequest.setXmlStandalone (true);

	    Element applicationNode = emptyRequest.createElement (applicationToken);
	    applicationNode.setAttribute (nameToken, applicationName);
	    if (applicationArgument != null)
		applicationNode.appendChild (emptyRequest.importNode (applicationArgument, true));
	    for (String pearId : pendingRequests.keySet ())
		addPendingApplicationToClient (pearId, applicationNode);
	} catch (ParserConfigurationException e) {
	    Log.keepLastException ("Server::addPendingApplication", e);
	}
    }

    // ========================================
}
