package network;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.Method;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;
import java.util.TreeSet;
import java.util.Vector;

import javax.swing.AbstractButton;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.SwingConstants;

import misc.ApplicationManager;
import misc.Bundle;
import misc.Config;
import misc.OwnFrame;
import misc.Util;
import static misc.Util.GBC;
import static misc.Util.GBCNL;

@SuppressWarnings ("serial") public class ProtocolManager implements ApplicationManager, ActionListener {

    // ========================================
    private OwnFrame controller;
    private Protocol protocol;

    private JPanel protoPanel;
    private ButtonGroup group = new ButtonGroup ();
    private JComboBox<String> nsHostCB;
    private JComboBox<String> hostCB;
    private JComboBox<Integer> portCB;
    private JComboBox<String> uriCB;
    private JCheckBox proxyCheckBox;
    private JComboBox<String> proxyHostCB;
    private JComboBox<Integer> proxyPortCB;

    public Protocol getProtocol () { return protocol; }
    public void setProtocol (Protocol protocol) {
	if (this.protocol != null)
	    this.protocol.stop ();
	this.protocol = protocol;
	if (protocol != null)
	    protocol.start ();

	Thread.yield ();
	updateCommands ();
	updateProtocol ();
	broadcastStartProtocol ();
    }

    // ========================================
    public ProtocolManager (OwnFrame controller) {
	this.controller = controller;
	createGUI ();
	start ();
    }

    // ========================================
    protected Thread currentThread;

    public void stop () {
	currentThread = null;
    }

    public void start () {
	(new Thread () {
		public void run () {
		    for (currentThread = Thread.currentThread (); currentThread == Thread.currentThread (); ) {
			updateCommands ();
			Util.sleep (5);
		    }
		}
	    }).start ();
    }

    // ========================================
    public void createGUI () {
	final TreeSet<String> hosts = new TreeSet<String> ();
	hosts.add ("localhost");
	nsHostCB = new JComboBox<String> (new Vector<String> (hosts));
	nsHostCB.setSelectedItem ("localhost");
	

	(new Thread () {
		public void run () {
		    try {
			for (Enumeration<NetworkInterface> e = NetworkInterface.getNetworkInterfaces (); e.hasMoreElements (); ) {
			    NetworkInterface ni = e.nextElement ();
			    for (Enumeration<InetAddress> e2 = ni.getInetAddresses (); e2.hasMoreElements (); ) {
				InetAddress ia = e2.nextElement ();
				for (String host : Arrays.asList (ia.getHostAddress (), ia.getHostName ()))
				    if (!hosts.contains (host)) {
					nsHostCB.addItem (host);
					hosts.add (host);
				    }
			    }
			}
		    } catch (Exception e3) {
		    }
		}
	    }).start ();

	hostCB = new JComboBox<String> ();
	portCB = new JComboBox<Integer> ();
	uriCB = new JComboBox<String> ();
	proxyHostCB = new JComboBox<String> ();
	proxyPortCB = new JComboBox<Integer> ();

	Config.loadJComboBox ("hostProtocol", hostCB, "localhost");
	Config.loadJComboBoxInteger ("portProtocol", portCB, "8080");
	Config.loadJComboBox ("uriProtocol", uriCB, "/");
	Config.loadJComboBox ("hostProxy", proxyHostCB, "localhost");
	Config.loadJComboBoxInteger ("portProxy", proxyPortCB, "3128");

	hostCB.setEditable (true);
	portCB.setEditable (true);
	uriCB.setEditable (true);
	proxyHostCB.setEditable (true);
	proxyPortCB.setEditable (true);

	String protocolType = Config.getString (protocolTypeToken, setServerToken);
	protoPanel = Util.getGridBagPanel ();

	Util.addLabelFields (protoPanel, "AvailableNetworkCard", nsHostCB);
	Util.addLabel ("ConnectionType", SwingConstants.CENTER, protoPanel, GBCNL);

	Util.addRadioButton (setServerToken, this, group, protocolType, protoPanel, GBC);
	Util.addComponent (hostCB, protoPanel, GBC);
	Util.addComponent (new JLabel (":"), protoPanel, GBC);
	Util.addComponent (portCB, protoPanel, GBCNL);

	Util.addRadioButton (setClientToken, this, group, protocolType, protoPanel, GBC);
	Util.addComponent (uriCB, protoPanel, GBCNL);

	proxyCheckBox = Util.addCheckButtonConfig (setProxyToken, this, false, protoPanel, GBC);
	Util.addComponent (proxyHostCB, protoPanel, GBC);
	Util.addComponent (new JLabel (":"), protoPanel, GBC);
	Util.addComponent (proxyPortCB, protoPanel, GBCNL);


	Util.addComponent (new JLabel (), protoPanel, GBCNL);

	Util.addRadioButton (unsetProtocolToken, this, group, protocolType, protoPanel, GBC);
	Util.addComponent (new JLabel (), protoPanel, GBCNL);

	if (unsetProtocolToken.equals (protocolType))
	    actionUnsetProtocol ();
	else if (setClientToken.equals (protocolType))
	    actionSetClient ();
	else
	    actionSetServer ();
    }

    // ========================================
    static public final List<String> actionsNames = Arrays.asList ("LinkType");
    static public final String protocolTypeToken = "ProtocolType";
    static public final String setServerToken = "SetServer";
    static public final String setClientToken = "SetClient";
    static public final String unsetProtocolToken = "UnsetProtocol";
    static public final List<String> typeActionsNames =
	Arrays.asList (setClientToken, setServerToken, unsetProtocolToken);
    static public final String setProxyToken = "SetProxy";
    @SuppressWarnings("unchecked")
    static public final Hashtable<String, Method> actionsMethod =
	Util.collectMethod (ProtocolManager.class,
			    Util.merge (actionsNames, typeActionsNames,
					Arrays.asList (setProxyToken)));

    public void  actionPerformed (ActionEvent e) {
	Util.actionPerformed (actionsMethod, e, this);
    }

    // ========================================
    public synchronized void setIPEnable (boolean enable) {
	hostCB.setEnabled (enable);
	portCB.setEnabled (enable);
	uriCB.setEnabled (enable);
	proxyCheckBox.setEnabled (enable);
    }
    public synchronized void setProxyEnable (boolean enable) {
	proxyHostCB.setEnabled (enable);
	proxyPortCB.setEnabled (enable);
    }

    // ========================================
    public synchronized void actionSetServer () {
	setIPEnable (false);
	portCB.setEnabled (true);
	setProxyEnable (false);
    }

    public synchronized void actionSetClient () {
	setIPEnable (true);
	actionSetProxy ();
    }

    public synchronized void actionSetProxy () {
	boolean selected = proxyCheckBox.isSelected ();
	Config.setBoolean (setProxyToken+Config.checkedPostfix, selected);
	setProxyEnable (selected);
    }

    public synchronized void actionUnsetProtocol () {
	setIPEnable (false);
	setProxyEnable (false);
    }

    // ========================================
    public synchronized void actionLinkType () {
	if (JOptionPane.OK_OPTION !=
	    JOptionPane.showConfirmDialog (controller.getJFrame (), protoPanel,
					   Bundle.getTitle ("LinkType"),
					   JOptionPane.OK_CANCEL_OPTION))
	    return;
	String protocolType = group.getSelection ().getActionCommand ();
	Config.setString (protocolTypeToken, protocolType);
	Config.saveJComboBox ("hostProtocol", hostCB);
	Config.saveJComboBoxInteger ("portProtocol", portCB);
	Config.saveJComboBox ("uriProtocol", uriCB);
	Config.saveJComboBox ("hostProxy", proxyHostCB);
	Config.saveJComboBoxInteger ("portProxy", proxyPortCB);

	Protocol protocol = null;
	if (unsetProtocolToken.equals (protocolType))
	    ;
	else if (setClientToken.equals (protocolType)) {
	    if (proxyCheckBox.isSelected ())
		protocol = new Client (hostCB.getItemAt (hostCB.getSelectedIndex ()),
				       portCB.getItemAt (portCB.getSelectedIndex ()),
				       uriCB.getItemAt (uriCB.getSelectedIndex ()),
				       proxyHostCB.getItemAt (proxyHostCB.getSelectedIndex ()),
				       proxyPortCB.getItemAt (proxyPortCB.getSelectedIndex ()));
	    else
		protocol = new Client (hostCB.getItemAt (hostCB.getSelectedIndex ()),
				       portCB.getItemAt (portCB.getSelectedIndex ()),
				       uriCB.getItemAt (uriCB.getSelectedIndex ()));
	} else if (setServerToken.equals (protocolType))
	    protocol = new Server (portCB.getItemAt (portCB.getSelectedIndex ()));
	setProtocol (protocol);
    }

    // ========================================
    private Vector<AbstractButton> noNetworkCommands = new Vector<AbstractButton> ();
    public synchronized void addNoNetworkCommand (AbstractButton noNetworkCommand) {
	if (noNetworkCommand == null)
	    return;
	boolean isAlive = protocol != null && protocol.isAlive ();
	noNetworkCommands.add (noNetworkCommand);
	noNetworkCommand.setEnabled (!isAlive);
    }
    public synchronized void updateCommands () {
	boolean isAlive = protocol != null && protocol.isAlive ();
	for (AbstractButton noNetworkCommand : noNetworkCommands)
	    noNetworkCommand.setEnabled (!isAlive);
    }

    // ========================================
    private Vector<Waiter> waiters = new Vector<Waiter> ();
    public synchronized void addWaiter (Waiter waiter) {
	waiters.add (waiter);
    }

    public synchronized void updateProtocol () {
	for (Waiter waiter : waiters)
	    waiter.setProtocol (protocol);
    }

    // ========================================
    Vector<ProtocolObserver> protocolObservers = new Vector<ProtocolObserver> ();
    public void addProtocolObserver (ProtocolObserver protocolObserver) {
	protocolObservers.add (protocolObserver);
    }
    public void removeProtocolObserver (ProtocolObserver protocolObserver) {
	protocolObservers.remove (protocolObserver);
    }

    // ========================================
    public void broadcastStartProtocol () {
	if (protocol != null)
	    for (ProtocolObserver protocolObserver : protocolObservers)
		protocolObserver.startProtocol (protocol);
    }

    // ========================================
    public void addMenuItem (JMenu... jMenu) {
	Util.addMenuItem (actionsNames, this, jMenu[0]);
    }

    // ========================================
    public void addIconButtons (Container... container) {
	Util.addIconButton (actionsNames, this, container[0]);
    }

    // ========================================
    public void addActiveButtons (Hashtable<String, AbstractButton> buttons) {
	// XXX ??? fournir la sous-liste
    }

    // ========================================
}
