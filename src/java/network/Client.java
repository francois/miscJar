package network;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.net.URLConnection;

import org.w3c.dom.Element;

import misc.Log;
import misc.XML;

public class Client extends Protocol {

    // ========================================
    static public final boolean trace = false;
    static public final boolean socketNotURL = false; 

    // ========================================
    private String host;
    private int port;
    private String uri = "/";
    private Proxy proxy;

    // ========================================
    public Client (String host, int port) {
	super (ClientProtocolType);
	this.host = host;
	this.port = port;
    }

    public Client (String host, int port, String uri) {
	this (host, port);
	this.uri = uri;
    }

    public Client (String host, int port, String uri, String proxyHost, int proxyPort) {
	this (host, port, uri);
	proxy = new Proxy (Proxy.Type.HTTP, new InetSocketAddress (proxyHost, proxyPort));
	if (trace)
	    System.err.println (type+": proxy: "+proxy.address ());
    }

    public Client (String host, int port, String proxyHost, int proxyPort) {
	this (host, port, "/", proxyHost, proxyPort);
    }

    // ========================================
    public void start () {
	(new Thread () {
		public void run () {
		    Log.writeLog ("Protocol", "Client started pulling to "+host+":"+port+"("+proxy+")");
		    for (currentThread = Thread.currentThread (); currentThread == Thread.currentThread (); ) {
			long startTime = System.currentTimeMillis ();
			try {

			    // XXX prendre la date

			    Log.writeLog ("Protocol", "client connected to "+host+":"+port+"("+proxy+")");
			    URLConnection call = XMLConnection.getXMLConnection (new URL ("http", host, port, uri), proxy);
			    exchangeXML (call, true);
			} catch (IOException e) {
			    // include : ConnectException, MalformedURLException, UnknownHostException
			    long stopTime = System.currentTimeMillis ();
			    if (stopTime - startTime < 1000) {
				Log.keepLastException ("Client::start", e);
				break;
			    }
			    // XXX si date trop courte sortir de la boucle
			    System.err.println ("coucou:"+e);
			}
		    }
		    Log.writeLog ("Protocol", "Client stopped pulling to "+host+":"+port+"("+proxy+")");
		}
	    }).start ();
    }

    // ========================================
    public synchronized void send (String applicationName, Element applicationArgument) {
	super.send (applicationName, applicationArgument);
	if (trace)
	    System.err.println (type+":send: immediat send "+applicationName);
	// puis force l'envoie immediat
	(new Thread () {
		public void run () {
		    try {
			Log.writeLog ("Protocol", "client connected to "+host+":"+port+"("+proxy+")");
			URLConnection call = XMLConnection.getXMLConnection (new URL ("http", host, port, uri), proxy);
			exchangeXML (call, false);
		    } catch (IOException e) {
			//} catch (UnknownHostException e) {
			Log.keepLastException ("Client::send", e);
		    }
		}
	    }).start ();
    }



    // ========================================
    private void exchangeXML (URLConnection urlConnection, boolean askWaiting)
	throws IOException {
	OutputStream out = urlConnection.getOutputStream ();
	PendingRequest pendingRequest = getPendingRequest ();
	if (askWaiting)
	    pendingRequest.request.getDocumentElement ().setAttribute (waitAnswerToken, "yes");
	XML.writeDocument (pendingRequest.request, out);
	out.flush ();
	out.close ();
	InputStream in = urlConnection.getInputStream ();
	Element bundle = getBundle (in);
	in.close ();
	broadcastApplications (bundle);
    }

    // ========================================
    private PendingRequest pendingRequest = new PendingRequest ();
 
    // ========================================
    private synchronized PendingRequest getPendingRequest () {
	if (trace)
	    System.err.println (type+":getPendingRequest:");
	PendingRequest old = pendingRequest;
	pendingRequest = new PendingRequest ();
	return old;
    }

    // ========================================
    protected void addPendingApplication (String applicationName, Element applicationArgument) {
	Element applicationNode = pendingRequest.request.createElement (applicationToken);
	applicationNode.setAttribute (nameToken, applicationName);
	if (applicationArgument != null)
	    applicationNode.appendChild (pendingRequest.request.importNode (applicationArgument, true));
	pendingRequest.localPaquet.appendChild (applicationNode);
	pendingRequest.readyToSend = true;
	if (trace) {
	    System.err.println (type+":addPendingApplication: ");
	    XML.writeDocument (pendingRequest.request, System.err);
	    System.err.flush ();
	}
    }

    // ========================================
}
