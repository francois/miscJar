package network;

import java.io.IOException;
import java.io.InputStream;
import java.io.FilterInputStream;
import java.util.Hashtable;

public class HTTPInputStream extends FilterInputStream {

    // ========================================
    public String command;
    public Hashtable<String, String> headers = new Hashtable<String, String> ();
    private String lastHeader;

    // ========================================
    public HTTPInputStream (InputStream in)
	throws IOException {
	super (in);
	parseHeaders ();
    }

    // ========================================
    private void parseHeaders ()
	throws IOException {
	for (;;) {
	    String line = readLine ();
	    if (line.equals ("")) {
		break;
	    }
	    int pos = line.indexOf (":");
	    if (pos < 0) {
		if (lastHeader == null)
		    command = line;
		else
		    throw new IllegalArgumentException (line+" is not a mime line");
	    } else {
		lastHeader = line.substring (0, pos).trim ();
		headers.put (lastHeader, line.substring (pos+1).trim ());
	    }
	}
    }

    // ========================================
    public String readLine ()
	throws IOException {
	StringBuffer result = new StringBuffer ();
	for (;;) {
	    int c = super.read ();
	    if (c < 0)
		break;
	    if (c == '\r')
		continue;
	    if (c == '\n')
		break;
	    result.append ((char) c);
	}
	return result.toString ();
    }

    // ========================================
}
