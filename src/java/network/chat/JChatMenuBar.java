package network.chat;

import java.util.Hashtable;
import javax.swing.AbstractButton;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JMenu;
import javax.swing.JMenuBar;

import network.ProtocolManager;
import network.login.LoginManager;

import misc.ApplicationManager;
import misc.Log;
import misc.ToolBarManager;
import misc.Util;

/**
   La barre de menu.
*/
@SuppressWarnings ("serial") public class JChatMenuBar extends JMenuBar {

    // ========================================
    public JChatMenuBar (ApplicationManager controllerManager,
			 ProtocolManager protocolManager, LoginManager loginManager, ChatManager chatManager,
			 ApplicationManager helpManager, ToolBarManager toolBarManager) {
	setLayout (new BoxLayout (this, BoxLayout.X_AXIS));
	JMenu fileMenu = Util.addJMenu (this, "File");
	JMenu networkMenu = Util.addJMenu (this, "Network");
	add (Box.createHorizontalGlue ());
	JMenu helpMenu = Util.addJMenu (this, "Help");

	Util.addMenuItem (ChatManager.loginActionsNames, chatManager, fileMenu);
	Util.addMenuItem (ChatManager.saveActionsNames, chatManager, fileMenu);
	controllerManager.addMenuItem (fileMenu);
	protocolManager.addMenuItem (networkMenu);
	loginManager.addMenuItem (networkMenu);
	helpManager.addMenuItem (helpMenu);
	if (toolBarManager != null)
	    toolBarManager.addMenuItem (helpMenu);

	Hashtable<String, AbstractButton> buttons = new Hashtable<String, AbstractButton> ();
	Util.collectButtons (buttons, fileMenu);
	Util.collectButtons (buttons, networkMenu);
	Util.collectButtons (buttons, helpMenu);

	controllerManager.addActiveButtons (buttons);
	helpManager.addActiveButtons (buttons);
	chatManager.addSaveCommand (buttons.get (ChatManager.actionClear));
	if (toolBarManager != null)
	    toolBarManager.addActiveButtons (buttons);
    }

    // ========================================
}
