package network.chat;

import java.util.Comparator;
import java.util.Date;

/**
   Modificaion de modèle qui peuvent être observé.
*/
public class ChatQuote implements Comparator<ChatQuote> {

    int sequence;
    Date date;
    String speaker;
    String sentence;

    // ========================================
    public ChatQuote (int sequence, Date date, String speaker, String sentence) {
	this.sequence = sequence;
	this.date = date;
	this.speaker = speaker;
	this.sentence = sentence;
    }

    // ========================================
    public int compare (ChatQuote o1, ChatQuote o2) {
	if (o1.sequence != o2.sequence)
 	    return o1.sequence - o2.sequence;
	int diff = o1.date.compareTo (o2.date);
	if (diff != 0)
	    return diff;
	diff = o1.speaker.compareTo (o2.speaker);
	if (diff != 0)
	    return diff;
	return o1.sentence.compareTo (o2.sentence);
    }

    // ========================================
    public boolean equals (ChatQuote obj) {
	return
	    sentence == obj.sentence && date == obj.date &&
	    speaker.equals (obj.speaker) && sentence.equals (obj.sentence);
    }

    // ========================================
}
