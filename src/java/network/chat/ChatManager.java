package network.chat;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;
import java.util.Vector;
import javax.swing.AbstractButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JMenu;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

import misc.ApplicationManager;
import misc.Bundle;
import misc.Config;
import misc.OwnFrame;
import misc.Util;

/**
   comportement déclanché par des actionneurs graphiques (menu ou bouton).
*/
@SuppressWarnings ("serial") public class ChatManager implements ApplicationManager, ActionListener, ChatObserver {

    // ========================================
    private OwnFrame controller;

    static public final String actionPseudo = "Pseudo";
    static public final String extention = "chat";
    static public final String actionClear = "Clear";
    static public final List<String> loginActionsNames = Arrays.asList (actionPseudo);
    static public final List<String> saveActionsNames = Arrays.asList (actionClear, "SaveAs");
    @SuppressWarnings("unchecked")
				static public final List<String> actionsNames =
				Util.merge (loginActionsNames, saveActionsNames, Arrays.asList ("ManageExtention"));
    @SuppressWarnings("unchecked")
				static public final Hashtable<String, Method> actionsMethod =
				Util.collectMethod (ChatManager.class, actionsNames);

    public void  actionPerformed (ActionEvent e) {
				Util.actionPerformed (actionsMethod, e, this);
    }

    // ========================================
    private Chat chat;

    public Chat getChat () { return chat; }

    // ========================================
    public ChatManager (OwnFrame controller, Chat chat) {
				this.controller = controller;
				this.chat = chat;
				chat.addChatObserver (this);
				jFileChooser.setFileFilter (new FileNameExtensionFilter (Bundle.getLabel ("ChatFilter"), extention));
				jFileChooser.setAccessory (manageExtensionCheckBox =
																	 Util.newCheckButtonConfig ("ManageExtention", this, true));
    }

    // ========================================
    public void actionManageExtention () {
				// rien a faire
    }

    public void actionPseudo () {
				String newName =
						(String) JOptionPane.showInputDialog (controller.getJFrame (), Bundle.getLabel ("NewPseudo"),
																									Bundle.getTitle ("ChangePseudo"), JOptionPane.INFORMATION_MESSAGE,
																									null, null, Chat.getRandomPseudo ());
				if (newName == null)
						return;
				chat.setPseudo (newName);
    }

    public synchronized void actionClear () {
				if (chat.getModified () &&
						JOptionPane.YES_OPTION !=
						JOptionPane.showConfirmDialog (controller.getJFrame (), Bundle.getMessage ("RealyClearChat"),
																					 Bundle.getTitle ("ChatNotSaved"), JOptionPane.WARNING_MESSAGE))
						return;
				chat.clear ();
    }

    final JFileChooser jFileChooser = new JFileChooser (Config.getString ("ChatDir", "data/chat"));
    final JCheckBox manageExtensionCheckBox;

    public synchronized void actionSaveAs () {
				jFileChooser.setFileSelectionMode (JFileChooser.FILES_ONLY);
				if (jFileChooser.showSaveDialog (controller.getJFrame ()) != JFileChooser.APPROVE_OPTION)
						return;
				File file = jFileChooser.getSelectedFile ();
				Config.setString ("ChatDir", file.getParent ());
				Config.save ("Chat");
				try {
						if (manageExtensionCheckBox.isSelected () && !file.getName ().endsWith ("."+extention))
								file = new File (file.getParent (), file.getName () + "."+extention);
						chat.save (file);
				} catch (IOException e) {
						JOptionPane.showMessageDialog (controller.getJFrame (), e.getMessage (), "alert", JOptionPane.WARNING_MESSAGE);
				}
    }

    // ========================================
    private Vector<AbstractButton> saveCommands = new Vector<AbstractButton> ();
    public void addSaveCommand (AbstractButton saveCommand) {
				saveCommands.add (saveCommand);
				saveCommand.setEnabled (chat.getModified ());
    }

    // ========================================
    public void talk (ChatQuote quote) {}
    public void renameSpeaker (String speaker) {}
    public void chatModifiedChange (boolean modified) {
				for (AbstractButton saveCommand : saveCommands)
						saveCommand.setEnabled (modified);
    }
    public void clearChat () {}

    // ========================================
    public void addMenuItem (JMenu... jMenu) {
				Util.addMenuItem (loginActionsNames, this, jMenu[0]);
				Util.addMenuItem (saveActionsNames, this, jMenu[0]);
    }

    // ========================================
    public void addIconButtons (Container... containers) {
				Util.addIconButton (loginActionsNames, this, containers[0]);
				Util.addIconButton (saveActionsNames, this, containers[0]);
    }

    // ========================================
    public void addActiveButtons (Hashtable<String, AbstractButton> buttons) {
				addSaveCommand (buttons.get (actionClear));
    }

    // ========================================
}
