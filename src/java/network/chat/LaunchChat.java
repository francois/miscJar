package network.chat;

import javax.swing.SwingUtilities;

import misc.Bundle;
import misc.Config;

/**
   Lance le controleur.
*/
public class LaunchChat {

    // ========================================
    static public void main (String[] args) {
	Config.setPWD (LaunchChat.class);
	Config.load ("Chat");
	Bundle.load ("Help");
	Bundle.load ("ToolBar");
	Bundle.load ("Controller");
	Bundle.load ("Protocol");
	Bundle.load ("Login");
	Bundle.load ("Chat");

        SwingUtilities.invokeLater (new Runnable () {
		public void run () {
		    new ChatController ();
		}
	    });
    }

    // ========================================
}
