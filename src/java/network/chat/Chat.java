package network.chat;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.Random;
import java.util.Vector;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

import misc.Bundle;
import misc.Config;
import misc.Log;
import misc.XML;
import network.Protocol;
import network.Waiter;

/**
 * Modèle.

 * <?xml version="1.0" encoding="UTF-8"?>
 * <bundle>
 *   <paquet pearId="AA6C1B8851EBBAC2">
 *     <application name="chat">
 *       <argument sequence="1" date="08:30:50" speudo="inconnu_561C">azertyuiop</argument>
 *     </application>
 *   </paquet>
 * </bundle>   
 */

public class Chat implements Waiter {

    // ========================================
    static public final String applicationName = "chat";
    static public final String quoteToken      = "quote";
    static public final String sequenceToken   = "sequence";
    static public final String dateToken       = "date";
    static public final String speakerToken    = "speaker";
    static public final SimpleDateFormat dateFormat = new SimpleDateFormat ("HH:mm:ss");

    private Protocol protocol;
    private String pseudo;

    private int lastSequence;
    private Vector<ChatQuote> dialog = new Vector<ChatQuote> ();
    private boolean modified = false;

    public synchronized void setProtocol (Protocol protocol) {
	if (this.protocol != null)
	    this.protocol.removeWaiter (applicationName, this);
	this.protocol = protocol;
	if (protocol != null)
	    protocol.addWaiter (applicationName, this);
    }

    static public String getRandomPseudo () {
	return
	    Bundle.getString ("anonymous", null)+"_"+Integer.toHexString ((new Random ()).nextInt ()).toUpperCase ().substring (0, 4);
    }
    public String getPseudo () { return pseudo; }
    public void setPseudo (String pseudo) {
	this.pseudo = pseudo;
	broadcastRenameSpeaker ();
	Config.setString ("chatLogin", pseudo);
    }

    public boolean getModified () { return modified; }

    // ========================================
    public Chat (String pseudo) {
	this.pseudo = pseudo;
	dateFormat.setLenient (false);
    }

    public Chat (String pseudo, Protocol protocol) {
	this (pseudo);
	setProtocol (protocol);
    }

    // ========================================
    public void clear () {
	dialog = new Vector<ChatQuote> ();
	modified = false;
	broadcastClearChat ();
    }

    // ========================================
    public void save (File file)
	throws IOException {
	try {
	    file.setExecutable (false);

	    Collections.sort (dialog, new ChatQuote (0, new Date (), "", ""));

	    DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance ().newDocumentBuilder ();
	    Document document = documentBuilder.newDocument ();
	    document.setXmlStandalone (true);

	    Element applicationNode = document.createElement (applicationName);
	    document.appendChild (applicationNode);
	    for (ChatQuote quote : dialog) {
		Element quoteElement = document.createElement (quoteToken);
		applicationNode.appendChild (quoteElement);
		quoteElement.setAttribute (sequenceToken, ""+quote.sequence);
		quoteElement.setAttribute (dateToken, dateFormat.format (quote.date));
		quoteElement.setAttribute (speakerToken, quote.speaker);
		quoteElement.appendChild (document.createTextNode (quote.sentence));
	    }
	    FileOutputStream out = new FileOutputStream (file);
	    XML.writeDocument (document, out);
	    out.close ();
	    modified = false;
	    broadcastChatModifiedChange ();
	} catch (ParserConfigurationException e) {
	    Log.keepLastException ("Chat::save", e);
	}
    }

    // ========================================
    public synchronized void say (String sentence) {
	if (sentence == null)
	    sentence = "";
	ChatQuote quote = new ChatQuote (++lastSequence, new Date (), pseudo, sentence);
	broadcastTalk (quote);
	if (protocol == null)
	    return;
	Element argument = Protocol.createArgument ();
	argument.setAttribute (sequenceToken, ""+quote.sequence);
	argument.setAttribute (dateToken, dateFormat.format (quote.date));
	argument.setAttribute (speakerToken, quote.speaker);
	argument.appendChild (argument.getOwnerDocument ().createTextNode (quote.sentence));
	protocol.send (applicationName, argument);
    }

    // ========================================
    public synchronized void receive (Element argument) {
	String speaker = argument.getAttribute (speakerToken);
	Date date = new Date ();
	try {
	    dateFormat.parse (argument.getAttribute (dateToken));
	} catch (ParseException e) {
	    Log.keepLastException ("Chat::receive", e);
	}
	int sequence = Integer.parseInt (argument.getAttribute (sequenceToken));
	lastSequence = Math.max (lastSequence, sequence);
	String sentence = "";
	NodeList nodeList = argument.getChildNodes ();
	if (nodeList.getLength () > 0)
	    sentence = ((Text) nodeList.item (0)).getWholeText ();
	broadcastTalk (new ChatQuote (sequence, date, speaker, sentence));
    }

    // ========================================
    Vector<ChatObserver> chatObservers = new Vector<ChatObserver> ();
    public void addChatObserver (ChatObserver chatObserver) { chatObservers.add (chatObserver); }
    public void removeChatObserver (ChatObserver chatObserver) { chatObservers.remove (chatObserver); }

    public void broadcastTalk (ChatQuote quote) {
	dialog.add (quote);
	modified = true;
	for (ChatObserver chatObserver : chatObservers)
	    chatObserver.talk (quote);
	broadcastChatModifiedChange ();
    }

    public void broadcastRenameSpeaker () {
	for (ChatObserver chatObserver : chatObservers)
	    chatObserver.renameSpeaker (pseudo);
    }

    public void broadcastChatModifiedChange () {
	for (ChatObserver chatObserver : chatObservers)
	    chatObserver.chatModifiedChange (modified);
    }

    public void broadcastClearChat () {
	for (ChatObserver chatObserver : chatObservers)
	    chatObserver.clearChat ();
	broadcastChatModifiedChange ();
    }

    // ========================================
}
