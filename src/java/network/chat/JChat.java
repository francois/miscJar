package network.chat;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collections;
import java.util.Date;
import java.util.Hashtable;
import java.util.Vector;
import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.plaf.basic.BasicBorders;
import javax.swing.text.BadLocationException;

import misc.Config;
import misc.Log;
import misc.Util;
import network.ProtocolManager;
import network.login.Login;
import network.login.LoginManager;
import network.login.LoginObserver;

@SuppressWarnings ("serial") public class JChat extends JPanel implements ChatObserver, LoginObserver, ActionListener {

    // ========================================
    private Chat chat;

    public JTextArea forum;
    public Vector<ChatQuote> dialog;
    public JScrollPane jScrollPane;
    public JLabel pseudo;
    public JTextField message;

    public Chat getChat () { return chat; }

    // ========================================
    public JChat (ProtocolManager protocolManager, LoginManager loginManager, ChatManager chatManager) {
	super (new BorderLayout ());
	chat = chatManager.getChat ();
	chat.addChatObserver (this);
	loginManager.getLogin ().addLoginObserver (this);

	forum = new JTextArea (Integer.parseInt (Config.getString ("ChatRows", "18")),
			       Integer.parseInt (Config.getString ("ChatColumns", "64")));
	dialog = new Vector <ChatQuote> ();
	jScrollPane = new JScrollPane (forum); 
	jScrollPane.setVerticalScrollBarPolicy (ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
	forum.setEditable (false);
	message = new JTextField ();
	message.setEnabled (true);
	message.addActionListener (this);

	pseudo = new JLabel (chat.getPseudo ()+" : ");
	pseudo.setEnabled (true);
	add (jScrollPane, BorderLayout.CENTER);
	JPanel buttonsPanel = new JPanel (null);
	buttonsPanel.setLayout (new BoxLayout (buttonsPanel, BoxLayout.X_AXIS));
	Util.addIconButton (ChatManager.loginActionsNames, chatManager, buttonsPanel);
	Util.addIconButton (ProtocolManager.actionsNames, protocolManager, buttonsPanel);
	Util.addIconButton (ChatManager.saveActionsNames, chatManager, buttonsPanel);
	Hashtable<String, AbstractButton> buttons = new Hashtable<String, AbstractButton> ();
	Util.collectButtons (buttons, buttonsPanel);

	Border buttonBorder =
	    BorderFactory.createCompoundBorder (new EtchedBorder (),
						BorderFactory.createCompoundBorder (BasicBorders.getMenuBarBorder (),
										    new EmptyBorder (3, 3, 3, 3)));
	for (Component component : buttonsPanel.getComponents ()) {
	    try {
		AbstractButton button = (AbstractButton) component;
		button.setBorder (buttonBorder);
	    } catch (Exception e) {
	    }
	}
	chatManager.addSaveCommand (buttons.get (ChatManager.actionClear));
	JPanel footer = new JPanel (new BorderLayout ());
	footer.add (pseudo, BorderLayout.WEST);
	footer.add (message, BorderLayout.CENTER);
	footer.add (buttonsPanel, BorderLayout.EAST);
	add (footer, BorderLayout.SOUTH);
    }

    // ========================================
    public void  actionPerformed (ActionEvent e) {
	actionSay ();
    }

    public void actionSay () {
	chat.say (message.getText ());
	message.setText ("");
    }

    // ========================================
    private ChatQuote control = new ChatQuote (0, new Date (), "", "");

    public synchronized void talk (ChatQuote quote) {
	dialog.add (quote);
	Collections.sort (dialog, control);
	String newLine = Chat.dateFormat.format (quote.date)+" "+quote.speaker+" > "+quote.sentence+"\n";
	int quoteIndex = dialog.indexOf (quote);
	try {
	    int lineOffset = forum.getLineStartOffset (quoteIndex);
	    forum.insert (newLine, lineOffset);
	    forum.setCaretPosition (lineOffset);
	} catch (BadLocationException e) {
	    try {
		forum.append (newLine);
		forum.setCaretPosition (forum.getLineEndOffset (forum.getLineCount ()-1));
	    } catch (BadLocationException e2) {
		Log.keepLastException ("JChat::talk", e2);
	    }
	}
    }

    public void renameSpeaker (String speaker) {
	pseudo.setText (speaker+" : ");
    }

    public void chatModifiedChange (boolean modified) { }

    public void clearChat () {
	forum.setText ("");
	dialog = new Vector<ChatQuote> ();
	message.setText ("");
    }

    // ========================================
    public void updateLogin (String login) {}
    public void updateGroup (String groupName) {}
    public void updateGroups (String[] groupsName) {}
    public void infoGroup (Login.Group group) {}
    public void loginState (String state) {
	boolean connected = !Login.unknownToken.equals (state);
	pseudo.setEnabled (connected);
	message.setEnabled (connected);
    }

    // ========================================
}
