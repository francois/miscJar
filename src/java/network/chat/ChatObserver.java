package network.chat;

import java.util.Date;

/**
   Modificaion de modèle qui peuvent être observé.
*/
public interface ChatObserver {

    // ========================================
    public void talk (ChatQuote quote);
    public void renameSpeaker (String speaker);
    public void chatModifiedChange (boolean modified);
    public void clearChat ();

    // ========================================
}
