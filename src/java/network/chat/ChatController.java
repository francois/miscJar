package network.chat;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Image;
import java.text.MessageFormat;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import network.login.Login;
import network.login.LoginManager;
import network.ProtocolManager;
import misc.Controller;
import misc.Bundle;
import misc.Config;
import misc.HelpManager;
import misc.MultiToolBarBorderLayout;
import misc.Util;

/**
   Créer et relie le modèle avec le graphisme.
*/
public class ChatController extends Controller<Chat> implements ChatObserver {

    // ========================================
    public ChatController () {
	super (new Chat (Config.getString ("chatLogin", Bundle.getString ("anonymous", null))));
    }

    // ========================================
    Chat chat;
    Login login;

    ProtocolManager protocolManager;
    ChatManager chatManager;
    LoginManager loginManager;
    HelpManager helpManager;

    JChat jChat;
    JChatMenuBar jChatMenuBar;

    // ========================================
    public String getTitle () { return MessageFormat.format (Bundle.getTitle ("Chat"), chat.getPseudo ()); }
    public Image getIcon () { return Util.loadImage (Config.getString ("ChatIcon", "data/images/chat/chat.png")); }

    // ========================================
    protected boolean tryClosingWindows () {
	Config.save ("Chat");
	if (chat.getModified ()) {
	    switch (JOptionPane.showConfirmDialog (jFrame, Bundle.getMessage ("SaveChat"),
						   Bundle.getTitle ("ChatNotSaved"),
						   JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.WARNING_MESSAGE)) {
	    case JOptionPane.YES_OPTION:
		chatManager.actionSaveAs ();
		return true;
	    case JOptionPane.NO_OPTION:
		return true;
	    case JOptionPane.CANCEL_OPTION:
	    case JOptionPane.CLOSED_OPTION:
		return false;
	    }
	}
	return true;
    }

    // ========================================
    protected void createModel (Chat chat) {
	this.chat = chat;
	login = new Login (Config.getString ("pseudo", Login.getRandomLogin ()));
	chat.addChatObserver (this);
    }

    // ========================================
    protected Component createGUI () {
	JPanel contentPane = new JPanel (new MultiToolBarBorderLayout ());

	protocolManager = new ProtocolManager (this);
	protocolManager.addWaiter (login);
	protocolManager.addWaiter (chat);
	protocolManager.start ();

	chatManager = new ChatManager (this, chat);
	loginManager = new LoginManager (this, login);

	helpManager = new HelpManager (this, "Chat");

	jChat = new JChat (protocolManager, loginManager, chatManager);

	contentPane.add (jChat, BorderLayout.CENTER);
	return contentPane;
    }

    // ========================================
    protected JMenuBar createMenuBar () {
	jChatMenuBar = new JChatMenuBar (this, protocolManager, loginManager, chatManager, helpManager, null);
	return jChatMenuBar;
    }

    // ========================================
    public void talk (ChatQuote quote) {}
    public void renameSpeaker (String speaker) { jFrame.setTitle (getTitle ()); }
    public void chatModifiedChange (boolean modified) {}
    public void clearChat () {}

    // ========================================
}
