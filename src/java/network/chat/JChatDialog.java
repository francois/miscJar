package network.chat;

import java.awt.Frame;
import java.text.MessageFormat;

import misc.Bundle;
import misc.TitledDialog;

/**
   fenêtre d'habillage pour inclusion.
*/
@SuppressWarnings ("serial") public class JChatDialog extends TitledDialog implements ChatObserver {

    // ========================================
    public String pseudo;

    // ========================================
    public JChatDialog (Frame frame, JChat jChat) {
	super (frame, "Chat");
	pseudo = jChat.getChat ().getPseudo ();
	jChat.getChat ().addChatObserver (this);
	add (jChat);
	updateBundle ();
	Bundle.addBundleObserver (this);
    }

    // ========================================
    public void updateBundle () {
	setTitle (getTitle ());
    }

    // ========================================
    public String getTitle () {
	return MessageFormat.format (Bundle.getTitle (titleId), pseudo);
    }

    // ========================================
    public void talk (ChatQuote quote) {}
    public void renameSpeaker (String speaker) { pseudo = speaker; updateBundle (); }
    public void chatModifiedChange (boolean modified) {}
    public void clearChat () {}

    // ========================================
}
