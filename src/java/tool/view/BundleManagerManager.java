package tool.view;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;
import java.util.Vector;
import java.util.regex.Pattern;
import javax.swing.AbstractButton;
import javax.swing.Box;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.filechooser.FileNameExtensionFilter;

import misc.ApplicationManager;
import misc.Bundle;
import misc.Config;
import misc.OwnFrame;
import misc.Util;
import tool.model.BundleManagerModel;
import tool.model.BundleManagerObserver;

@SuppressWarnings ("serial") public class BundleManagerManager
    implements ApplicationManager, ActionListener, BundleManagerObserver {

    // ========================================
    BundleManagerModel bundleManagerModel;

    OwnFrame controller;

    BundleManagerView bundleManagerView;
    JFileChooser jFileChooser = new JFileChooser (Config.getString ("BundleDir", "."));
    File bundleDir = null;
    String bundleName = null;

    public String getBundleName () { return bundleName; }

    // ========================================
    static public final List<String> fileActionsNames = Arrays.asList ("Empty", "Open", "Merge", "Save", "SaveAs");
    static public final List<String> modelActionsNames = Arrays.asList ("AddKey", "RemoveKey", "AddLocale", "RenameLocale", "RemoveLocale");
    @SuppressWarnings ("unchecked")
				static public final Hashtable<String, Method> actionsMethod =
				Util.collectMethod (BundleManagerManager.class, Util.merge (fileActionsNames, modelActionsNames));

    public void actionPerformed (ActionEvent e) {
				Util.actionPerformed (actionsMethod, e, this);
    }

    // ========================================
    public void updateBundle () {
				jFileChooser.setFileFilter (new FileNameExtensionFilter (Bundle.getLabel ("BundelPropertieFile"), "properties"));
    }

    // ========================================
    public BundleManagerManager (OwnFrame controller, BundleManagerModel bundleManagerModel, BundleManagerView bundleManagerView) {
				this.controller = controller;
				this.bundleManagerModel = bundleManagerModel;
				this.bundleManagerView = bundleManagerView;
				jFileChooser.setMultiSelectionEnabled (false);

				Bundle.addBundleObserver (this);
				bundleManagerModel.addBundleObserver (this);
				updateBundle ();
    }

    // ========================================
    public void actionEmpty () {
				if (bundleManagerModel.getModified () &&
						JOptionPane.showConfirmDialog (controller.getJFrame (), Bundle.getLabel ("RealyDiscardBundle"),
																					 Bundle.getTitle ("BundleNotSaved"),
																					 JOptionPane.YES_NO_OPTION,
																					 JOptionPane.WARNING_MESSAGE) != JOptionPane.YES_OPTION)
						return;
				bundleName = null;
				bundleManagerModel.empty ();
    }

    // ========================================
    private void actionOpen (boolean merge) {
				if (bundleManagerModel.getModified () &&
						JOptionPane.showConfirmDialog (controller.getJFrame (), Bundle.getLabel ("RealyDiscardBundle"),
																					 Bundle.getTitle ("BundleNotSaved"),
																					 JOptionPane.YES_NO_OPTION,
																					 JOptionPane.WARNING_MESSAGE) != JOptionPane.YES_OPTION)
						return;
				jFileChooser.setFileSelectionMode (JFileChooser.FILES_ONLY);
				if (jFileChooser.showOpenDialog (controller.getJFrame ()) != JFileChooser.APPROVE_OPTION)
						return;
				File file = jFileChooser.getSelectedFile ();
				setBundleDir (file.getParentFile ());
				setBundleName (file.getName ());
				bundleManagerModel.open (bundleDir, bundleName, merge);
				Config.save ("BundleManager");
    }

    // ========================================
    public void actionOpen () {
				actionOpen (false);
    }

    // ========================================
    public void actionMerge () {
				actionOpen (true);
    }

    // ========================================
    private void setBundleName (String fileName) {
				if (fileName.endsWith (BundleManagerModel.extention))
						fileName = fileName.substring (0, fileName.length () - BundleManagerModel.extention.length ());
				Pattern pattern = Pattern.compile (".*_[a-zA-Z]{2}");
				if (pattern.matcher (fileName).matches ())
						fileName = fileName.substring (0, fileName.length () - 3);
				if (pattern.matcher (fileName).matches ())
						fileName = fileName.substring (0, fileName.length () - 3);
				bundleName = fileName;
    }

    // ========================================
    public void actionSave () {
				if (bundleDir == null || bundleName == null) {
						actionSaveAs ();
						return;
				}
				bundleManagerModel.saveAs (bundleDir, bundleName);
				Config.save ("BundleManager");
    }

    // ========================================
    public void actionSaveAs () {
				jFileChooser.setFileSelectionMode ((bundleName == null) ?
																					 JFileChooser.FILES_ONLY : JFileChooser.FILES_AND_DIRECTORIES);
				if (jFileChooser.showSaveDialog (controller.getJFrame ()) != JFileChooser.APPROVE_OPTION)
						return;
				File file = jFileChooser.getSelectedFile ();
				if (file.isDirectory ())
						setBundleDir (file);
				else {
						setBundleDir (file.getParentFile ());
						setBundleName (file.getName ());
				}
				actionSave ();
    }

    // ========================================
    public void actionAddKey () {
				String newKey =
						JOptionPane.showInputDialog (controller.getJFrame (), Bundle.getLabel ("NewKey"),
																				 Bundle.getTitle ("AddKey"),
																				 JOptionPane.INFORMATION_MESSAGE);
				if (newKey == null)
						return;
				bundleManagerModel.addKey (newKey);
    }

    // ========================================
    public void actionRemoveKey () {
				if (bundleManagerView.bundleJTable.getSelectedRowCount () != 1) {
						JOptionPane.showMessageDialog (controller.getJFrame (), Bundle.getMessage ("NoKey"), Bundle.getTitle ("NoKey"),
																					 JOptionPane.WARNING_MESSAGE);
						return;
				}
				int selected = bundleManagerView.bundleJTable.getSelectedRow ();
				bundleManagerModel.removeKey (bundleManagerModel.getKeyByIndex (selected));
    }

    // ========================================
    public void actionAddLocale () {

				Locale [] locales = Bundle.getAllLocales ();
				JComboBox<String> localesList = Bundle.getJComboFlags (locales);
				localesList.setEditable (true);
				JPanel jPanel = new JPanel (new BorderLayout ());
				jPanel.add (new JLabel (Bundle.getMessage ("AddLocale")), BorderLayout.PAGE_START);
				jPanel.add (localesList, BorderLayout.CENTER);

				if (JOptionPane.OK_OPTION != JOptionPane.showConfirmDialog (controller.getJFrame (), jPanel, Bundle.getTitle ("AddLocale"),
																																		JOptionPane.OK_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE))
						return;
				bundleManagerModel.add ("_"+locales[localesList.getSelectedIndex ()].toString ());
    }

    // ========================================
    public void actionRenameLocale () {
				if (bundleManagerView.bundleJTable.getSelectedColumnCount () != 1) {
						JOptionPane.showMessageDialog (controller.getJFrame (), Bundle.getMessage ("NoLocale"),
																					 Bundle.getTitle ("NoLocale"),
																					 JOptionPane.WARNING_MESSAGE);
						return;
				}
				String newLocale =
						JOptionPane.showInputDialog (controller.getJFrame (), Bundle.getMessage ("RenameLocale"),
																				 Bundle.getTitle ("RenameLocale"),
																				 JOptionPane.INFORMATION_MESSAGE);
				if (newLocale == null)
						return;
				bundleManagerModel.renameLocale (bundleManagerView.bundleColumnNames.get (bundleManagerView.bundleJTable.getSelectedColumn ()), newLocale);
    }

    // ========================================
    public void actionRemoveLocale () {
				if (bundleManagerView.bundleJTable.getSelectedColumnCount () != 1) {
						JOptionPane.showMessageDialog (controller.getJFrame (), Bundle.getMessage ("NoLocale"),
																					 Bundle.getTitle ("NoLocale"),
																					 JOptionPane.WARNING_MESSAGE);
						return;
				}
				bundleManagerModel.removeLocale (bundleManagerView.bundleColumnNames.get (bundleManagerView.bundleJTable.getSelectedColumn ()));
    }

    // ========================================
    private void setBundleDir (File dir) {
				bundleDir = dir;
				try {
						Config.setString ("BundleDir", dir.getCanonicalPath ());
				} catch (Exception e) {
				}
    }

    // ========================================
    private Vector<AbstractButton> saveCommands = new Vector<AbstractButton> ();
    public void addSaveCommand (AbstractButton saveCommand) {
				if (saveCommand == null)
						return;
				saveCommands.add (saveCommand);
				saveCommand.setEnabled (bundleName != null && bundleManagerModel.getModified ());
    }

    // ========================================
    private Vector<AbstractButton> saveAsCommands = new Vector<AbstractButton> ();
    public void addSaveAsCommand (AbstractButton saveAsCommand) {
				if (saveAsCommand == null)
						return;
				saveAsCommands.add (saveAsCommand);
				saveAsCommand.setEnabled (bundleManagerModel.getModified ());
    }

    // ========================================
    public void broadcastSaveUpdate () {
				boolean enable = bundleName != null && bundleManagerModel.getModified ();
				for (AbstractButton saveCommand : saveCommands)
						saveCommand.setEnabled (enable);
    }

    // ========================================
    public void broadcastSaveAsUpdate () {
				boolean enable = bundleManagerModel.getModified ();
				for (AbstractButton saveAsCommand : saveAsCommands)
						saveAsCommand.setEnabled (enable);
    }

    // ========================================
    public void dataModifiedChange () {
				broadcastSaveUpdate ();
				broadcastSaveAsUpdate ();
    }
    public void localesUpdated () {}
    public void dataUpdated () {}

    // ========================================
    public void addMenuItem (JMenu... jMenu) {
				int idx = 0;
				Util.addMenuItem (fileActionsNames, this, jMenu[idx++]);
				Util.addMenuItem (modelActionsNames, this, jMenu[idx++]);
    }
    public void addIconButtons (Container... containers) {
				int idx = 0;
				Util.addIconButton (fileActionsNames, this, containers[idx++]);
				Util.addIconButton (modelActionsNames, this, containers[idx++]);
    }
    public void addActiveButtons (Hashtable<String, AbstractButton> buttons) {
				addSaveCommand (buttons.get ("Save"));
				addSaveAsCommand (buttons.get ("SaveAs"));
    }

    // ========================================
}
