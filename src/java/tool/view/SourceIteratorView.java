package tool.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

import misc.Bundle;
import misc.Config;
import misc.Util;
import tool.model.SourceIteratorModel;

@SuppressWarnings ("serial") public class SourceIteratorView extends JPanel implements ActionListener {

    // ========================================
    SourceIteratorModel sourceIteratorModel = null;

    // ========================================
    String[] columnLabels = { "FileName", "LineNumber", "Line" };
    DefaultTableModel sourceIteratorTableModel;
    JTable sourceIteratorJTable;

    // ========================================
    static public final List<String> actionsNames = Arrays.asList ("Init", "Next");
    @SuppressWarnings ("unchecked")
				static public final Hashtable<String, Method> actionsMethod =
				Util.collectMethod (SourceIteratorView.class, actionsNames);

    public void actionPerformed (ActionEvent e) {
				Util.actionPerformed (actionsMethod, e, this);
    }

    // ========================================
    public void updateBundle () {
				Util.setColumnLabels (sourceIteratorJTable, columnLabels);
    }

    // ========================================
    DefaultTableCellRenderer greenRendered = new DefaultTableCellRenderer ();
    DefaultTableCellRenderer whiteRendered = new DefaultTableCellRenderer ();

    // ========================================
    public SourceIteratorView () {
				super (new BorderLayout ());

				greenRendered.setBackground (Color.GREEN);
				whiteRendered.setBackground (Color.WHITE);

				int [] columnWidth = {
						200,
						5,
						1000
				};
				sourceIteratorTableModel = new DefaultTableModel (columnLabels, 1);
				sourceIteratorJTable = new JTable (sourceIteratorTableModel);
				updateBundle ();
				for (int i = 0; i < sourceIteratorTableModel.getColumnCount (); i++)
						sourceIteratorJTable.getColumnModel ().getColumn (i).setPreferredWidth (columnWidth [i]);
				sourceIteratorJTable.getColumnModel ().setColumnSelectionAllowed (true);
        sourceIteratorJTable.setSelectionMode (ListSelectionModel.SINGLE_SELECTION);
				add (sourceIteratorJTable.getTableHeader (), BorderLayout.PAGE_START);
				add (sourceIteratorJTable, BorderLayout.CENTER);

				JPanel buttonPanel = new JPanel ();
				Util.addButton (actionsNames, this, buttonPanel);
				add (buttonPanel, BorderLayout.PAGE_END);

				Bundle.addBundleObserver (this);
    }

    // ========================================
    JFileChooser jFileChooser = new JFileChooser (Config.getString ("SourceIteratorDir", "."));

    // ========================================
    public void actionInit () {
				jFileChooser.setFileSelectionMode (JFileChooser.DIRECTORIES_ONLY);
				if (jFileChooser.showOpenDialog (this) != JFileChooser.APPROVE_OPTION)
						return;
				File dir = jFileChooser.getSelectedFile ();
				sourceIteratorModel = new SourceIteratorModel (dir);
				actionNext ();
				try {
						Config.setString ("SourceIteratorDir", dir.getCanonicalPath ());
				} catch (Exception e) {
				}
    }


    // ========================================
    public void actionNext () {
				if (sourceIteratorModel == null) {
						JOptionPane.showMessageDialog (this, Bundle.getMessage ("NoSourceDir"), Bundle.getTitle ("NoSource"),
																					 JOptionPane.WARNING_MESSAGE);
						return;
				}
				if (! sourceIteratorModel.hasMoreString ()) {
						sourceIteratorTableModel.setRowCount (0);
						sourceIteratorTableModel.addRow (new String [] {"", "", ""});
						sourceIteratorModel = null;
						return;
				}
				String next = sourceIteratorModel.nextString ();
				sourceIteratorTableModel.setRowCount (0);
				sourceIteratorTableModel.addRow (new String [] {
								""+sourceIteratorModel.getCurrentFile (),
								""+sourceIteratorModel.getCurrentLine (),
								next
						});

				sourceIteratorJTable.getColumnModel ().getColumn (2).setCellRenderer
						((next.indexOf ("Bundle.") >= 0 || next.indexOf ("Config.") >= 0 ) ? greenRendered : whiteRendered);
    }

    // ========================================
}
