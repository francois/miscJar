package tool.view;

import java.awt.Component;
import java.util.Hashtable;
import javax.swing.AbstractButton;
import javax.swing.Box;
import javax.swing.JComponent;
import javax.swing.JToolBar;

import misc.ApplicationManager;
import misc.Bundle;
import misc.HelpManager;
import misc.Log;
import misc.ToolBarManager;
import misc.Util;

public class BundleManagerToolBar {

    // ========================================
    static public String defaultCardinalPoint = "North";

    public BundleManagerToolBar (ApplicationManager controllerManager, BundleManagerManager bundleManagerManager,
				 HelpManager helpManager, ToolBarManager toolBarManager) {
	JToolBar fileToolBar = toolBarManager.newJToolBar ("File", defaultCardinalPoint);
	JToolBar bundleToolBar = toolBarManager.newJToolBar ("Bundle", defaultCardinalPoint);
	JToolBar helpToolBar = toolBarManager.newJToolBar ("Help", defaultCardinalPoint);

	controllerManager.addIconButtons (fileToolBar);
	bundleManagerManager.addIconButtons (fileToolBar, bundleToolBar);
	helpManager.addIconButtons (helpToolBar);
	toolBarManager.addIconButtons (helpToolBar);

	Hashtable<String, AbstractButton> buttons = new Hashtable<String, AbstractButton> ();
	Util.collectButtons (buttons, fileToolBar);
	Util.collectButtons (buttons, bundleToolBar);
	Util.collectButtons (buttons, helpToolBar);

	controllerManager.addActiveButtons (buttons);
	bundleManagerManager.addActiveButtons (buttons);
	helpManager.addActiveButtons (buttons);
	toolBarManager.addActiveButtons (buttons);
    }

    // ========================================
}
