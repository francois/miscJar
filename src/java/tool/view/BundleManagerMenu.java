package tool.view;

import java.util.Hashtable;
import javax.swing.AbstractButton;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JMenu;
import javax.swing.JMenuBar;

import misc.ApplicationManager;
import misc.HelpManager;
import misc.Log;
import misc.ToolBarManager;
import misc.Util;

@SuppressWarnings ("serial") public class BundleManagerMenu extends JMenuBar {
    // ========================================
    public BundleManagerMenu (ApplicationManager controllerManager, ApplicationManager bundleManagerManager,
			      ApplicationManager helpManager, ToolBarManager toolBarManager) {
	setLayout (new BoxLayout (this, BoxLayout.X_AXIS));

	JMenu fileMenu = Util.addJMenu (this, "File");
	JMenu bundleMenu = Util.addJMenu (this, "Bundle");
	add (Box.createHorizontalGlue ());
	JMenu helpMenu = Util.addJMenu (this, "Help");

	controllerManager.addMenuItem (fileMenu);
	bundleManagerManager.addMenuItem (fileMenu, bundleMenu);
	helpManager.addMenuItem (helpMenu);
	toolBarManager.addMenuItem (helpMenu);

	Hashtable<String, AbstractButton> buttons = new Hashtable<String, AbstractButton> ();
	Util.collectButtons (buttons, fileMenu);
	Util.collectButtons (buttons, bundleMenu);
	Util.collectButtons (buttons, helpMenu);

	controllerManager.addActiveButtons (buttons);
	bundleManagerManager.addActiveButtons (buttons);
	helpManager.addActiveButtons (buttons);
	toolBarManager.addActiveButtons (buttons);
    }

    // ========================================
}
