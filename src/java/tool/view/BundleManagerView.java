package tool.view;

import java.awt.BorderLayout;
import java.util.Vector;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;

import misc.Bundle;
import tool.model.BundleManagerModel;
import tool.model.BundleManagerObserver;

@SuppressWarnings ("serial") public class BundleManagerView extends JPanel implements BundleManagerObserver {

    // ========================================
    BundleManagerModel bundleManagerModel;

    // ========================================
    Vector<String> bundleColumnNames;
    DefaultTableModel bundleTableModel;
    JTable bundleJTable;

    // ========================================
    public void updateBundle () {
	localesUpdated ();
    }

    // ========================================
    public BundleManagerView (final BundleManagerModel bundleManagerModel) {
 	super (new BorderLayout ());
	this.bundleManagerModel = bundleManagerModel;
	bundleJTable = new JTable ();
	bundleJTable.getColumnModel ().setColumnSelectionAllowed (true);
        bundleJTable.setSelectionMode (ListSelectionModel.SINGLE_SELECTION);
	JScrollPane scrollBundle = new JScrollPane (bundleJTable);
	bundleJTable.setFillsViewportHeight (true);
 	add (scrollBundle, BorderLayout.CENTER);
	bundleManagerModel.addBundleObserver (this);
	updateBundle ();
	Bundle.addBundleObserver (this);
    }

    // ========================================
    public void dataModifiedChange () {}

    // ========================================
    public void localesUpdated () {
	bundleColumnNames = new Vector<String> ();
	bundleColumnNames.add (Bundle.getLabel ("Keys"));
	bundleColumnNames.addAll (bundleManagerModel.getLocales ());
	bundleTableModel = new DefaultTableModel (bundleColumnNames, 0) {

		private static final long serialVersionUID = 1L;

		public String getColumnName(int col) {
		    return bundleColumnNames.get (col);
		}

		public int getColumnCount () {
		    return bundleColumnNames.size ();
		}

		public Object getValueAt (int row, int col) {
		    String key = bundleManagerModel.getKeyByIndex (row);
		    if (col == 0)
			return key;
		    String locale = bundleManagerModel.getLocaleByIndex (col-1);
		    return bundleManagerModel.get (locale, key);
		}

		public void setValueAt (Object value, int row, int col) {
		    String key = bundleManagerModel.getKeyByIndex (row);
		    if (col == 0) {
			String newKey = (String) value;
			if (newKey.equals (key))
			    return;
			bundleManagerModel.renameKey (key, newKey);
			return;
		    }
		    String locale = bundleManagerModel.getLocaleByIndex (col-1);
		    bundleManagerModel.set (locale, key, (String) value);
		    //fireTableCellUpdated (row, col);
		}
		public boolean isCellEditable (int row, int column) {
		    return true;
		}
	    };
	bundleJTable.setModel (bundleTableModel);
	dataUpdated ();
    }

    // ========================================
    public void dataUpdated () {
	bundleTableModel.setRowCount (0);
	for (String key : bundleManagerModel.getKeys ()) {
	    Vector<String> row = new Vector<String> ();
	    row.add (key);
	    for (String locale : bundleManagerModel.getLocales ())
		row.add (bundleManagerModel.get (locale, key));
	    bundleTableModel.addRow (row);
	}
	bundleTableModel.fireTableDataChanged ();
    }

    // ========================================
}
