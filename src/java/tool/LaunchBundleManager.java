package tool;

import javax.swing.SwingUtilities;

import misc.Bundle;
import misc.Config;

import tool.controller.BundleManagerController;

public class LaunchBundleManager {

    // ========================================
    static public void main (String [] arg) {
	Config.setPWD (LaunchBundleManager.class);
	Config.load ("BundleManager");
	Bundle.load ("Help");
	Bundle.load ("ToolBar");
	Bundle.load ("Controller");
	Bundle.load ("BundleManager");

	SwingUtilities.invokeLater (new Runnable() {
		public void run() {
		    new BundleManagerController ();
		}
	    });
    }

    // ========================================
}
