package tool.controller;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Image;
import java.text.MessageFormat;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSplitPane;

import misc.Bundle;
import misc.Config;
import misc.Controller;
import misc.HelpManager;
import misc.MultiToolBarBorderLayout;
import misc.ToolBarManager;
import misc.Util;
import tool.model.BundleManagerModel;
import tool.model.BundleManagerObserver;
import tool.view.BundleManagerManager;
import tool.view.BundleManagerMenu;
import tool.view.BundleManagerToolBar;
import tool.view.BundleManagerView;
import tool.view.SourceIteratorView;

public class BundleManagerController extends Controller<BundleManagerModel> implements BundleManagerObserver {

    // ========================================
    public BundleManagerController () {
	super (new BundleManagerModel ());
    }

    // ========================================
    BundleManagerModel bundleManagerModel;

    BundleManagerManager bundleManagerManager;
    HelpManager helpManager;

    BundleManagerView bundleManagerView;
    BundleManagerMenu bundleManagerMenu;

    BundleManagerToolBar bundleManagerToolBar;
    ToolBarManager toolBarManager;

    // ========================================
    public String getTitle () {
	String bundleName = bundleManagerManager.getBundleName ();
	return MessageFormat.format (Bundle.getTitle ("BundleEditor"),
				     (bundleName == null) ? Bundle.getAction ("Empty") : bundleName);
    }
    public Image getIcon () { return Util.loadImage (Config.getString ("BundleIcon", "data/images/bundle/bundle.png")); }

    // ========================================
    protected void createModel (BundleManagerModel bundleManagerModel) {
	this.bundleManagerModel = bundleManagerModel;
	bundleManagerModel.addBundleObserver (this);
    }

    // ========================================
    protected Component createGUI () {
	JPanel contentPane = new JPanel (new MultiToolBarBorderLayout ());
	toolBarManager = new ToolBarManager (getIcon (), contentPane);

	bundleManagerView = new BundleManagerView (bundleManagerModel);
	bundleManagerManager = new BundleManagerManager (this, bundleManagerModel, bundleManagerView);

	JSplitPane splitPane = new JSplitPane (JSplitPane.VERTICAL_SPLIT,
					       bundleManagerView, new SourceIteratorView ());
	splitPane.setOneTouchExpandable (true);

	helpManager = new HelpManager (this, "BundleManager");

	bundleManagerToolBar = new BundleManagerToolBar (this, bundleManagerManager, helpManager, toolBarManager);
	contentPane.add (splitPane, BorderLayout.CENTER);
	return contentPane;
    }

    // ========================================
    protected JMenuBar createMenuBar () {
	return bundleManagerMenu = new BundleManagerMenu (this, bundleManagerManager, helpManager, toolBarManager);
    }

    // ========================================
    protected boolean tryClosingWindows () {
	toolBarManager.saveLocation ();
	Config.save ("BundleManager");
	if (bundleManagerModel.getModified ()) {
	    switch (JOptionPane.showConfirmDialog (jFrame, Bundle.getLabel ("SaveBundle"), Bundle.getTitle ("BundleNotSaved"),
						   JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.WARNING_MESSAGE)) {
	    case JOptionPane.YES_OPTION:
		bundleManagerManager.actionSave ();
		return true;
	    case JOptionPane.NO_OPTION:
		return true;
	    case JOptionPane.CANCEL_OPTION:
	    case JOptionPane.CLOSED_OPTION:
		return false;
	    }
	}
	return true;
    }

    // ========================================
    public void dataModifiedChange () { jFrame.setTitle (getTitle ()); }
    public void localesUpdated () {}
    public void dataUpdated () {}

    // ========================================
}
