package tool.model;

public interface BundleManagerObserver {

    // ========================================
    public void dataModifiedChange ();
    public void localesUpdated ();
    public void dataUpdated ();

    // ========================================
}
