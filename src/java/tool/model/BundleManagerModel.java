package tool.model;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.lang.reflect.Method;
import java.text.MessageFormat;
import java.util.Hashtable;
import java.util.Properties;
import java.util.TreeSet;
import java.util.Vector;

import misc.Bundle;
import misc.Log;

public class BundleManagerModel {

    // ========================================
    static public final String extention = ".properties";

    private Hashtable<String, Properties> bundles;
    private TreeSet<String> keys;
    private TreeSet<String> locales;

    // ========================================
    public final TreeSet<String> getKeys () {
	return keys;
    }

    // ========================================
    public String getKeyByIndex (int idx) {
	return (String) keys.toArray () [idx];
    }

    // ========================================
    public final TreeSet<String> getLocales () {
	return locales;
    }

    // ========================================
    public String getLocaleByIndex (int idx) {
	return (String) locales.toArray () [idx];
    }

    // ========================================
    private boolean modified = false;
    public boolean getModified () {
	return modified;
    }

    // ========================================
    public BundleManagerModel () {
	empty ();
    }

    // ========================================
    public void empty () {
	bundles = new Hashtable<String, Properties> ();
	keys = new TreeSet<String> ();
	locales = new TreeSet<String> ();
	modified = false;
	broadcastMethode ("localesUpdated");
	broadcastMethode ("dataUpdated");
	broadcastMethode ("dataModifiedChange");
    }

    // ========================================
    public void open (File bundleDir, final String bundleName, boolean merge) {
	File [] bundleFile = bundleDir.listFiles (new FilenameFilter () {
		public boolean accept (File dir, String name) {
		    return name.startsWith (bundleName) && name.endsWith (extention);
		}
	    });
	if (!merge)
	    empty ();
	int bundleNameLength = bundleName.length ();
	int extentionLength = extention.length ();
	for (File file : bundleFile) {
	    String fileName = file.getName ();
	    locales.add (fileName.substring (bundleNameLength, fileName.length ()-extentionLength));
	}
	for (String locale : locales) {
	    try {
		Properties properties = new Properties ();
		FileInputStream fileInputStream = new FileInputStream (new File (bundleDir, bundleName+locale+extention));
		properties.load (fileInputStream);
		fileInputStream.close ();
		Properties old = bundles.get (locale);
		if (old != null && merge) {
		    for (Object key : old.keySet ())
			properties.put (key, old.get (key));
		}
		bundles.put (locale, properties);
		for (Object key : properties.keySet ())
		    keys.add ((String) key);
	    } catch (IOException e) {
		System.err.println (MessageFormat.format (Bundle.getException ("CantLoad"), locale));
	    }
	}
	modified = merge;
	broadcastMethode ("localesUpdated");
	broadcastMethode ("dataUpdated");
	broadcastMethode ("dataModifiedChange");
    }

    // ========================================
    public void saveAs (File bundleDir, final String bundleName) {
	// XXX en cas de réécriture dans le meme repertoire supprimer les "locale" supprimes
	bundleDir.mkdirs ();
	boolean trouble = false;
	for (String locale : locales) {
	    try {
		FileOutputStream fileOutputStream = new FileOutputStream (new File (bundleDir, bundleName+locale+extention));
		bundles.get (locale).store (fileOutputStream, Bundle.getString ("HeaderBundleFile", null));
		fileOutputStream.close ();
	    } catch (IOException e) {
		System.err.println (MessageFormat.format (Bundle.getException ("CantSave"), locale));
		trouble = true;
	    }
	}
	modified = trouble;
	broadcastMethode ("dataModifiedChange");
    }

    // ========================================
    public void addKey (String newKey) {
	if (keys.contains (newKey))
	    throw new IllegalArgumentException (MessageFormat.format (Bundle.getException ("DuplicatedKey"), newKey));
	keys.add (newKey);
	broadcastMethode ("dataUpdated");
    }

    // ========================================
    public void renameKey (String key, String newKey) {
	if (keys.contains (newKey))
	    throw new IllegalArgumentException (MessageFormat.format (Bundle.getException ("DuplicatedKey"), newKey));
	for (String locale : locales) {
	    try {
		Properties properties = bundles.get (locale);
		String val = properties.getProperty (key);
		properties.remove (key);
		properties.setProperty (newKey, val);
		modified = true;
	    } catch (Exception e) {
		// key not defined on this locale
	    }
	}
	keys.remove (key);
	keys.add (newKey);
	broadcastMethode ("dataUpdated");
	broadcastMethode ("dataModifiedChange");
    }

    // ========================================
    public void removeKey (String key) {
	if (!keys.contains (key))
	    return;
	for (String locale : locales) {
	    try {
		Properties properties = bundles.get (locale);
		properties.remove (key);
		modified = true;
	    } catch (Exception e) {
		// key not defined on this locale
	    }
	}
	keys.remove (key);
	broadcastMethode ("dataUpdated");
	broadcastMethode ("dataModifiedChange");
    }

    // ========================================
    public void add (String locale) {
	if (locales.contains (locale))
	    return;
	locales.add (locale);
	bundles.put (locale, new Properties ());
	modified = true;
	broadcastMethode ("localesUpdated");
	broadcastMethode ("dataModifiedChange");
    }

    // ========================================
    public void renameLocale (String locale, String newLocale) {
	if (!locales.contains (locale))
	    return;
	if (locales.contains (newLocale))
	    throw new IllegalArgumentException (MessageFormat.format (Bundle.getException ("DuplicatedLocale"),
								      newLocale));
	locales.remove (locale);
	bundles.put (newLocale, bundles.remove (locale));
	locales.add (newLocale);
	modified = true;
	broadcastMethode ("localesUpdated");
	broadcastMethode ("dataModifiedChange");
    }

    // ========================================
    public void removeLocale (String locale) {
	if (!locales.contains (locale))
	    return;
	locales.remove (locale);
	bundles.remove (locale);
	modified = true;
	broadcastMethode ("localesUpdated");
	broadcastMethode ("dataModifiedChange");
    }

    // ========================================
    public String get (String locale, String key) {
	try {
	    return bundles.get (locale).getProperty (key);
	} catch (Exception e) {
	    return null;
	}
    }

    // ========================================
    public void set (String locale, String key, String val) {
	String oldVal = get (locale, key);
	if (val == oldVal || val.equals (oldVal))
	    return;
	bundles.get (locale).setProperty (key, val);
	modified = true;
	broadcastMethode ("dataUpdated");
	broadcastMethode ("dataModifiedChange");
    }

    // ========================================
    Vector<BundleManagerObserver> bundleObservers = new Vector<BundleManagerObserver> ();
    public void addBundleObserver    (BundleManagerObserver bundleObserver) { bundleObservers.add    (bundleObserver); }
    public void removeBundleObserver (BundleManagerObserver bundleObserver) { bundleObservers.remove (bundleObserver); }

    // ========================================
    public void broadcastMethode (String methodeName) {
	try {
	    Method methode = BundleManagerObserver.class.getMethod (methodeName);
	    for (BundleManagerObserver bundleObserver : bundleObservers)
		methode.invoke (bundleObserver);
	} catch (Exception e) {
	    Log.keepLastException ("BundleManagerModel::broadcastMethode", e);
	}
    }

    // ========================================
}
