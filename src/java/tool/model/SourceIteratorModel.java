package tool.model;

import java.io.*;
import java.util.*;

public class SourceIteratorModel {

    // ========================================
    Vector<File> files = new Vector<File> ();
    Enumeration<File> javaEnumeration;

    // ========================================
    public SourceIteratorModel (File dir) {
	add (dir);
	javaEnumeration = files.elements ();
    }

    // ========================================
    private void add (File dir) {
	for (File file :
		 dir.listFiles (new FilenameFilter () {
			 public boolean accept (File dir, String name) {
			     return name.endsWith (".java");
			 }
		     }))
	    files.add (file);
	for (File subdir :
		 dir.listFiles (new FileFilter () {
			 public boolean accept (File child) {
			     return child.isDirectory ();
			 }
		     }))
	    add (subdir);
    }

    // ========================================
    public boolean hasMoreJava () {
	return javaEnumeration.hasMoreElements ();
    }

    // ========================================
    public File nextJava () {
	return javaEnumeration.nextElement ();
    }

    // ========================================
    String nextString = null;
    int currentLine = 0;
    File currentFile;
    BufferedReader currentBuffered = null;

    // ========================================
    public int getCurrentLine () {
	return currentLine;
    }

    // ========================================
    public File getCurrentFile () {
	return currentFile;
    }

    // ========================================
    public boolean hasMoreString () {
	for (;;) {
	    if (nextString != null)
		return true;
	    if (currentBuffered == null) {
		if (! hasMoreJava ())
		    return false;
		try {
		    currentLine = -1;
		    currentFile = nextJava ();
		    currentBuffered = new BufferedReader (new FileReader (currentFile));
		} catch (FileNotFoundException e) {
		    System.err.println ("skip not found file ("+e+")");
		    continue;
		}
	    }
	    try {
		String line = currentBuffered.readLine ();
		currentLine++;
		if (line == null) {
		    currentBuffered = null;
		    continue;
		}
		if (line.indexOf ("\"") >= 0)
		    nextString = line;
	    } catch (IOException e) {
		System.err.println ("skip premature end of file ("+e+")");
	    }
	}
    }

    // ========================================
    public String nextString () {
	if (! hasMoreString ())
	    return null;
	String result = nextString;
	nextString = null;
	return result;
    }

    // ========================================
}
