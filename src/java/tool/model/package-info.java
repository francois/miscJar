/**
 * Ordre de lecture des paquetages et des classes<ul>
 *  <li>BundleManagerObserver</li>
 *  <li>BundleManagerModel</li>
 *  <li>SourceIteratorModel</li>
 * </ul>
 * @author F. Merciol
 */
package tool.model;
