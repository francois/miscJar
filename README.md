# miscJar

Misc est un atelier pour le développement d'application Java pouvant servir à l'illustration pédagogique.



Objets de pratiques

  * Log : fonction de trace
  * Config : gestion de valeurs initiales
  * Bundle : internationalisation
  * UpdateSender : alerte sur modification de valeur
  * ColorCheckedLine : gestion des couleurs d'un terminal pour testes unitaires en mode texte

Fonctions récurrentes

  * Util : ensemble de fonction (souvent graphique) pour factoriser du code

Éléments graphiques

  * SpinnerSlider : couplage d'un curseur (slider) et d'un champ numérique (spinner)
  * DatePanel : saisie de date
  * HourPanel : saisie d'heure
  * ImagePreview : accessoire de visualisation d'image pour dialogue de choix de fichier.
  * TitledDialog : fenêtre avec gestion d'icône de l'application
  * HtmlDialog : affichage d'une page HTML dans une application
  * Guide : outil d’auto-documentation pour surlignant l'action à réaliser dans un scénario d'utilisation

Modèle MVC

  * ApplicationManager : associe la gestion de bouton dans des menus déroulant ou des boîte d'icône
  * HelpManager : menu d'aide standard avec gestion de langue
  * ToolBarManager : boîte d'icône détachable
  * Controler : contrôleur du modèle MVC
  * ProgressState : gestion de modification du modèle pour connexion d'une barre de progression

Contournement

  * XML : contournement d'un bug d'une version de Java

Réseau

  * CommandLineServer : fonctions générique d'un serveur socket en mode texte
  * CommandLineWaiter : classe générique à spécialiser en fonction du serveur souhaité

